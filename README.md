# Retrospective motion correction

Short descriptions of the scripts in the repo:

-----------------------------------------------------------------------
## changeFeatDesignFile.sh
Change the design file for FEAT (and Melodic) according to the options provided

Note: There is a similar script distributed as part of FSL. It is located at fsldir/src/feat5/setFEAT.
Use both scripts to change different settings in the design file. 

-----------------------------------------------------------------------
## convertBahramsOutlierDataFile2FslStyle.sh

-----------------------------------------------------------------------
## convertSliceTimingFile2FeatStyle.sh
Converst sliceTiming.txt to the FSL-FEAT compliant style

-----------------------------------------------------------------------
## createSquareWaveRegressor4Fsl.sh
Creates a custom square wave regressor (Explanatory Variable, EV) for FSL

-----------------------------------------------------------------------
## defineEnvironmentVariables.sh
Define environment variables that may be needed in other scripts

-----------------------------------------------------------------------
## flash.sh
FEAT Limitedly Automated Shell Script

-----------------------------------------------------------------------
## mlash.sh
MELODIC Limitedly Automated Shell Script

-----------------------------------------------------------------------
## getFslMotionOutliers.sh

-----------------------------------------------------------------------
## mergeVolumesReorient2Std.sh
Merges nifti format fmri volumes in a given directory and reorients the resulting 4D image to FSL standard space

-----------------------------------------------------------------------
## samd.sh
Run FSL-FEAT on multiple directories with the same design file

-----------------------------------------------------------------------

-----------------------------------------------------------------------
## seedBasedConnectivityAnalsisUsingFslTools.sh
Seed based connectivity analysis of rsfMRI data using FSL tools and nipype

-----------------------------------------------------------------------
