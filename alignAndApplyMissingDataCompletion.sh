#! /bin/sh
# Aligns data using FSL mcflirt and then applies dynamic missing data completion (DMC)
#
# Author: Seyhmus Guler
#
#

if [ $# -lt 1 ] ; then
    echo "Usage: $0 <fmri_data>" 
    exit 1 
fi

func=$1
func_filename=$(basename $(basename $func .gz) .nii)

n_input_data_volumes=`${FSLDIR}/bin/fslinfo $func | grep dim4 | head -n 1 | awk {'print $2'}`
ref_vol_idx=`echo "$n_input_data_volumes / 2 " | bc`

${FSLDIR}/bin/fslroi $func ref_vol $ref_vol_idx 1 

${FSLDIR}/bin/mcflirt -in $func -out ${func_filename}_mcf -mats -plots -reffile ref_vol -rmsrel -rmsabs -spline_final
func_mcf=${func_filename}_mcf.nii.gz

${FSLDIR}/bin/fsl_motion_outliers -i $func -o ${func_filename}_motion_outliers_fd_thr_point5 --fd --thresh=0.5 -v | tee ${func_filename}_confounders.txt

outlier_idx=`cat ${func_filename}_confounders.txt | grep "Found spikes at" | sed -n -e 's/^.*at//p' | sed 's/^[ \t]*//'`
remove_volume_idx=`echo ${outlier_idx} | sed -e 's/\s\+/\n/g' | while read in; do echo $in; echo $(($in-1)); echo $(($in+1)); echo $(($in+2)); done | sort -n | uniq | while read in; do if [ $in -ge 0 ] && [ $in -lt $n_input_data_volumes ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`

/common/motion/code/fMRIMissingDataCompletion/fMRIMissingDataCompletion $func_mcf ${func_filename}_mcf_dmc.nii.gz $remove_volume_idx
