#! /bin/sh
# scrubs data based on a logical vector of which volumes to remove
#
# author : seyhmus guler
# date : 3/10/2017


if [ $# -ne 2 ] ; then echo "Usage : $0 <fmri4D_data> <remove_volumes_list>" ; exit 1 ; fi

random_text=2138213d98321

fmri4D_data=$1
remove_list=$2
datap=`dirname $fmri4D_data`
filename=`basename $fmri4D_data .gz`
filename=`basename $filename .nii`

trSec=`fslinfo $fmri4D_data | grep pixdim4 | awk {'print $2'}`
fslsplit $fmri4D_data tempvolumes_${randomtext}_

idx=0
num_delete=0

while read line ; do
    if [ $line -eq 1 ] ; then
        num_delete=`echo " $num_delete + 1 " | bc`
        rm -f tempvolumes_${randomtext}_000${idx}.nii.gz
        rm -f tempvolumes_${randomtext}_00${idx}.nii.gz
        rm -f tempvolumes_${randomtext}_0${idx}.nii.gz
        rm -f tempvolumes_${randomtext}_${idx}.nii.gz
    fi
    idx=`echo " $idx + 1 " | bc`
done < $remove_list

fslmerge -tr ${filename}_scrubbed tempvolumes_${randomtext}_* $trSec

rm -f tempvolumes_${randomtext}_*

# check 
final_size_along_time=`fslinfo ${filename}_scrubbed | grep dim4 | head -n 1 | awk {'print $2 '}`
deleted_plus_remaining=`echo " $final_size_along_time + $num_delete " | bc`
original_size=`fslinfo ${filename} | grep dim4 | head -n 1 | awk {'print $2'}`
if [ $deleted_plus_remaining -ne $original_size ] ; then
    echo -e "\e[31mERROR: Size mismatch after scrubbing the data, check the code\e[0m"
    rm -f ${filename}_scrubbed.nii.gz
    exit 1
else
    echo "Removed $num_delete volumes as part of scrubbing"
fi
