
# coding: utf-8

# In[69]:


import networkx as nx
from nilearn import plotting
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from networkx.readwrite import json_graph
import json

mapping = {1: 'bilateral_third_ventricle', 2: 'bilateral_fourth_ventricle', 3: 'right_accumbens_area', 4: 'left_accumbens_area', 5: 'right_amygdala', 6: 'left_amygdala', 7: 'bilateral_brainstem', 8: 'right_caudate', 9: 'left_caudate', 10: 'right_cerebellum_exterior', 11: 'left_cerebellum_exterior', 12: 'right_cerebellum_white_matter', 13: 'left_cerebellum_white_matter', 14: 'right_cerebral_white_matter', 15: 'left_cerebral_white_matter', 16: 'bilateral_cerebrospinal_fluid', 17: 'right_hippocampus', 18: 'left_hippocampus', 19: 'right_inferior_lateral_ventricle', 20: 'left_inferior_lateral_ventricle', 21: 'right_lateral_ventricle', 22: 'left_lateral_ventricle', 23: 'right_pallidum', 24: 'left_pallidum', 25: 'right_putamen', 26: 'left_putamen', 27: 'right_thalamus', 28: 'left_thalamus', 29: 'right_ventral_diencephalon', 30: 'left_ventral_diencephalon', 31: 'bilateral_optic_chiasm', 32: 'bilateral_cerebellar_vermal_lobules_I-V', 33: 'bilateral_cerebellar_vermal_lobules_VI-VII', 34: 'bilateral_cerebellar_vermal_lobules_VIII-X', 35: 'left_basal_forebrain', 36: 'right_basal_forebrain', 37: 'right_anterior_cingulate_gyrus', 38: 'left_anterior_cingulate_gyrus', 39: 'right_anterior_insula', 40: 'left_anterior_insula', 41: 'right_anterior_orbital_gyrus', 42: 'left_anterior_orbital_gyrus', 43: 'right_angular_gyrus', 44: 'left_angular_gyrus', 45: 'right_calcarine_cortex', 46: 'left_calcarine_cortex', 47: 'right_central_operculum', 48: 'left_central_operculum', 49: 'right_cuneus', 50: 'left_cuneus', 51: 'right_entorhinal_area', 52: 'left_entorhinal_area', 53: 'right_frontal_operculum', 54: 'left_frontal_operculum', 55: 'right_frontal_pole', 56: 'left_frontal_pole', 57: 'right_fusiform_gyrus', 58: 'left_fusiform_gyrus', 59: 'right_gyrus_rectus', 60: 'left_gyrus_rectus', 61: 'right_inferior_occipital_gyrus', 62: 'left_inferior_occipital_gyrus', 63: 'right_inferior_temporal_gyrus', 64: 'left_inferior_temporal_gyrus', 65: 'right_lingual_gyrus', 66: 'left_lingual_gyrus', 67: 'right_lateral_orbital_gyrus', 68: 'left_lateral_orbital_gyrus', 69: 'right_middle_cingulate_gyrus', 70: 'left_middle_cingulate_gyrus', 71: 'right_medial_frontal_cortex', 72: 'left_medial_frontal_cortex', 73: 'right_middle_frontal_gyrus', 74: 'left_middle_frontal_gyrus', 75: 'right_middle_occipital_gyrus', 76: 'left_middle_occipital_gyrus', 77: 'right_medial_orbital_gyrus', 78: 'left_medial_orbital_gyrus', 79: 'right_postcentral_gyrus_medial_segment', 80: 'left_postcentral_gyrus_medial_segment', 81: 'right_precentral_gyrus_medial_segment', 82: 'left_precentral_gyrus_medial_segment', 83: 'right_superior_frontal_gyrus_medial_segment', 84: 'left_superior_frontal_gyrus_medial_segment', 85: 'right_middle_temporal_gyrus', 86: 'left_middle_temporal_gyrus', 87: 'right_occipital_pole', 88: 'left_occipital_pole', 89: 'right_occipital_fusiform_gyrus', 90: 'left_occipital_fusiform_gyrus', 91: 'right_opercular_part_of_the_inferior_frontal_gyrus', 92: 'left_opercular_part_of_the_inferior_frontal_gyrus', 93: 'right_orbital_part_of_the_inferior_frontal_gyrus', 94: 'left_orbital_part_of_the_inferior_frontal_gyrus', 95: 'right_posterior_cingulate_gyrus', 96: 'left_posterior_cingulate_gyrus', 97: 'right_precuneus', 98: 'left_precuneus', 99: 'right_parahippocampal_gyrus', 100: 'left_parahippocampal_gyrus', 101: 'right_posterior_insula', 102: 'left_posterior_insula', 103: 'right_parietal_operculum', 104: 'left_parietal_operculum', 105: 'right_postcentral_gyrus', 106: 'left_postcentral_gyrus', 107: 'right_posterior_orbital_gyrus', 108: 'left_posterior_orbital_gyrus', 109: 'right_planum_polare', 110: 'left_planum_polare', 111: 'right_precentral_gyrus', 112: 'left_precentral_gyrus', 113: 'right_planum_temporale', 114: 'left_planum_temporale', 115: 'right_subcallosal_area', 116: 'left_subcallosal_area', 117: 'right_superior_frontal_gyrus', 118: 'left_superior_frontal_gyrus', 119: 'right_supplementary_motor_cortex', 120: 'left_supplementary_motor_cortex', 121: 'right_supramarginal_gyrus', 122: 'left_supramarginal_gyrus', 123: 'right_superior_occipital_gyrus', 124: 'left_superior_occipital_gyrus', 125: 'right_superior_parietal_lobule', 126: 'left_superior_parietal_lobule', 127: 'right_superior_temporal_gyrus', 128: 'left_superior_temporal_gyrus', 129: 'right_temporal_pole', 130: 'left_temporal_pole', 131: 'right_triangular_part_of_the_inferior_frontal_gyrus', 132: 'left_triangular_part_of_the_inferior_frontal_gyrus', 133: 'right_transverse_temporal_gyrus', 134: 'left_transverse_temporal_gyrus', 135: 'bilateral_extracerebral_cerebrospinal_fluid'}
threshold, upper, lower = 0.4, 1, 0

cc2=np.loadtxt('/local/data/tsc/bids/derivatives/fmriprep2/sub-a001/ses-01/corrcoef/sub-a001_ses-01_task-rest_bold_t1_res_bandpassed_scrubbed_ts_corrcoef.txt')
node_centers=np.loadtxt('/local/data/tsc/temp_parc/parcel_centers_mni_mm.txt')
node_sizes = np.loadtxt('/local/data/tsc/bids/derivatives/fmriprep_final/sub-c013/ses-02/corrcoef_noGS/sub-c013_ses-02_task-rest_bold_t1_res_noGS_bandpassed_scrubbed_reho.nii.gz_ROI_reho.vals')

#print(np.size(cc2))
mat_deneme = scipy.io.loadmat('/local/data/tsc/bids/derivatives/fmriprep2/aut_ave.mat')
aut_ave=mat_deneme['aut_ave']
#print(np.size(aut_ave,0))
mat_deneme = scipy.io.loadmat('/local/data/tsc/bids/derivatives/fmriprep2/con_ave.mat')
con_ave=mat_deneme['con_ave']

mat_deneme = scipy.io.loadmat('/local/data/tsc/bids/derivatives/fmriprep2/tsc_ave.mat')
tsc_ave=mat_deneme['tsc_ave']

con_minus_aut=scipy.io.loadmat('/local/data/tsc/bids/derivatives/fmriprep2/con_aut_both.mat')
conaut=con_minus_aut['conaut']

aut_minus_con=scipy.io.loadmat('/local/data/tsc/bids/derivatives/fmriprep2/autcon.mat')
autcon=aut_minus_con['autcon']

con_minus_tsc=scipy.io.loadmat('/local/data/tsc/bids/derivatives/fmriprep2/con_tsc.mat')
contsc=con_minus_tsc['contsc']
aa= np.absolute(aut_ave)
#plt.imshow(aa,extent=[0, 1, 0, 1])
#plt.show(aa)
#print(np.size(node_sizes))
G = nx.from_numpy_matrix(np.where(aa > threshold, upper, lower))
G = nx.relabel_nodes(G, mapping)
#data = json_graph.adjacency_data(G)
#with open('data2.txt', 'w') as outfile: 
#    json.dumps(data)
#pos = nx.circular_layout(G)
nx.draw_circular(G,node_size=node_sizes*120,node_color=node_sizes*100,with_labels=True)
cluster_coefficient = nx.average_clustering(G)
#local_efficiency = nx.local_efficiency(G)
#global_efficiency = nx.global_efficiency(G)
#shortest_path = nx.average_shortest_path_length(G)
print(cluster_coefficient)
#print(local_efficiency)
#print(global_efficiency)
#print(shortest_path)


# In[53]:


import pandas as pd
from bokeh.charts import output_file, Chord
from bokeh.io import show
from bokeh.sampledata.les_mis import data
 
nodes = data['nodes']
links = data['links']
 
nodes_df = pd.DataFrame(nodes)
links_df = pd.DataFrame(links)
 
source_data = links_df.merge(nodes_df, how='left', left_on='source', right_index=True)
source_data = source_data.merge(nodes_df, how='left', left_on='target', right_index=True)
source_data = source_data[source_data["value"] > 5]
source_data
 
chord_from_df = Chord(source_data, source="name_x", target="name_y", value="value")
output_file('chord-diagram-bokeh.html', mode="inline")
show(chord_from_df)


# In[51]:


###################
# chord diagram
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

import numpy as np

LW = 0.3

def polar2xy(r, theta):
    return np.array([r*np.cos(theta), r*np.sin(theta)])

def hex2rgb(c):
    return tuple(int(c[i:i+2], 16)/256.0 for i in (1, 3 ,5))

def IdeogramArc(start=0, end=60, radius=1.0, width=0.2, ax=None, color=(1,0,0)):
    # start, end should be in [0, 360)
    if start > end:
        start, end = end, start
    start *= np.pi/180.
    end *= np.pi/180.
    # optimal distance to the control points
    # https://stackoverflow.com/questions/1734745/how-to-create-circle-with-b%C3%A9zier-curves
    opt = 4./3. * np.tan((end-start)/ 4.) * radius
    inner = radius*(1-width)
    verts = [
        polar2xy(radius, start),
        polar2xy(radius, start) + polar2xy(opt, start+0.5*np.pi),
        polar2xy(radius, end) + polar2xy(opt, end-0.5*np.pi),
        polar2xy(radius, end),
        polar2xy(inner, end),
        polar2xy(inner, end) + polar2xy(opt*(1-width), end-0.5*np.pi),
        polar2xy(inner, start) + polar2xy(opt*(1-width), start+0.5*np.pi),
        polar2xy(inner, start),
        polar2xy(radius, start),
        ]

    codes = [Path.MOVETO,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.LINETO,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CLOSEPOLY,
             ]

    if ax == None:
        return verts, codes
    else:
        path = Path(verts, codes)
        patch = patches.PathPatch(path, facecolor=color+(0.5,), edgecolor=color+(0.4,), lw=LW)
        ax.add_patch(patch)


def ChordArc(start1=0, end1=60, start2=180, end2=240, radius=1.0, chordwidth=0.7, ax=None, color=(1,0,0)):
    # start, end should be in [0, 360)
    if start1 > end1:
        start1, end1 = end1, start1
    if start2 > end2:
        start2, end2 = end2, start2
    start1 *= np.pi/180.
    end1 *= np.pi/180.
    start2 *= np.pi/180.
    end2 *= np.pi/180.
    opt1 = 4./3. * np.tan((end1-start1)/ 4.) * radius
    opt2 = 4./3. * np.tan((end2-start2)/ 4.) * radius
    rchord = radius * (1-chordwidth)
    verts = [
        polar2xy(radius, start1),
        polar2xy(radius, start1) + polar2xy(opt1, start1+0.5*np.pi),
        polar2xy(radius, end1) + polar2xy(opt1, end1-0.5*np.pi),
        polar2xy(radius, end1),
        polar2xy(rchord, end1),
        polar2xy(rchord, start2),
        polar2xy(radius, start2),
        polar2xy(radius, start2) + polar2xy(opt2, start2+0.5*np.pi),
        polar2xy(radius, end2) + polar2xy(opt2, end2-0.5*np.pi),
        polar2xy(radius, end2),
        polar2xy(rchord, end2),
        polar2xy(rchord, start1),
        polar2xy(radius, start1),
        ]

    codes = [Path.MOVETO,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             ]

    if ax == None:
        return verts, codes
    else:
        path = Path(verts, codes)
        patch = patches.PathPatch(path, facecolor=color+(0.5,), edgecolor=color+(0.4,), lw=LW)
        ax.add_patch(patch)

def selfChordArc(start=0, end=60, radius=1.0, chordwidth=0.7, ax=None, color=(1,0,0)):
    # start, end should be in [0, 360)
    if start > end:
        start, end = end, start
    start *= np.pi/180.
    end *= np.pi/180.
    opt = 4./3. * np.tan((end-start)/ 4.) * radius
    rchord = radius * (1-chordwidth)
    verts = [
        polar2xy(radius, start),
        polar2xy(radius, start) + polar2xy(opt, start+0.5*np.pi),
        polar2xy(radius, end) + polar2xy(opt, end-0.5*np.pi),
        polar2xy(radius, end),
        polar2xy(rchord, end),
        polar2xy(rchord, start),
        polar2xy(radius, start),
        ]

    codes = [Path.MOVETO,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             Path.CURVE4,
             ]

    if ax == None:
        return verts, codes
    else:
        path = Path(verts, codes)
        patch = patches.PathPatch(path, facecolor=color+(0.5,), edgecolor=color+(0.4,), lw=LW)
        ax.add_patch(patch)

def chordDiagram(X, ax, colors=None, width=0.1, pad=2, chordwidth=0.7):
    """Plot a chord diagram
    Parameters
    ----------
    X :
        flux data, X[i, j] is the flux from i to j
    ax :
        matplotlib `axes` to show the plot
    colors : optional
        user defined colors in rgb format. Use function hex2rgb() to convert hex color to rgb color. Default: d3.js category10
    width : optional
        width/thickness of the ideogram arc
    pad : optional
        gap pad between two neighboring ideogram arcs, unit: degree, default: 2 degree
    chordwidth : optional
        position of the control points for the chords, controlling the shape of the chords
    """
    # X[i, j]:  i -> j
    x = X.sum(axis = 1) # sum over rows
    ax.set_xlim(-1.1, 1.1)
    ax.set_ylim(-1.1, 1.1)

    if colors is None:
    # use d3.js category10 https://github.com/d3/d3-3.x-api-reference/blob/master/Ordinal-Scales.md#category10
        colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd',
                  '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        if len(x) > 10:
            print('x is too large! Use x smaller than 10')
        colors = [hex2rgb(colors[i]) for i in range(len(x))]

    # find position for each start and end
    y = x/np.sum(x).astype(float) * (360 - pad*len(x))

    pos = {}
    arc = []
    nodePos = []
    start = 0
    for i in range(len(x)):
        end = start + y[i]
        arc.append((start, end))
        angle = 0.5*(start+end)
        #print(start, end, angle)
        if -30 <= angle <= 210:
            angle -= 90
        else:
            angle -= 270
        nodePos.append(tuple(polar2xy(1.1, 0.5*(start+end)*np.pi/180.)) + (angle,))
        z = (X[i, :]/x[i].astype(float)) * (end - start)
        ids = np.argsort(z)
        z0 = start
        for j in ids:
            pos[(i, j)] = (z0, z0+z[j])
            z0 += z[j]
        start = end + pad

    for i in range(len(x)):
        start, end = arc[i]
        IdeogramArc(start=start, end=end, radius=1.0, ax=ax, color=colors[i], width=width)
        start, end = pos[(i,i)]
        selfChordArc(start, end, radius=1.-width, color=colors[i], chordwidth=chordwidth*0.7, ax=ax)
        for j in range(i):
            color = colors[i]
            if X[i, j] > X[j, i]:
                color = colors[j]
            start1, end1 = pos[(i,j)]
            start2, end2 = pos[(j,i)]
            ChordArc(start1, end1, start2, end2,
                     radius=1.-width, color=colors[i], chordwidth=chordwidth, ax=ax)

    #print(nodePos)
    return nodePos

##################################
if __name__ == "__main__":
    fig = plt.figure(figsize=(6,6))
    flux = np.array([[11975,  5871, 8916, 2868],
      [ 1951, 10048, 2060, 6171],
      [ 8010, 16145, 8090, 8045],
      [ 1013,   990,  940, 6907]
    ])

    ax = plt.axes([0,0,1,1])

    #nodePos = chordDiagram(flux, ax, colors=[hex2rgb(x) for x in ['#666666', '#66ff66', '#ff6666', '#6666ff']])
    nodePos = chordDiagram(flux, ax)
    ax.axis('off')
    prop = dict(fontsize=16*0.8, ha='center', va='center')
    nodes = ['non-crystal', 'FCC', 'HCP', 'BCC']
    for i in range(4):
        ax.text(nodePos[i][0], nodePos[i][1], nodes[i], rotation=nodePos[i][2], **prop)

    plt.savefig("example.png", dpi=600,
            transparent=True,
            bbox_inches='tight', pad_inches=0.02)

