#! /bin/sh
# functional connectivity analysis of rsfMRI data using FSL tools
#
# author : seyhmus guler
#
#

#======================================================================
# usage 
#----------------------------------------------------------------------

print_usage () {
    echo "
    Functional connectivity analysis of rsfMRI data using FSL tools
    
    Usage: 
    `basename $0` -in <input_data> -out <output_dir> [options]

    Compulsory arguments (You MUST set all of):
        -in <input_data>        -   input data  (4D image file)
        -out <output_dir>       -   output directory
    
    Optional arguments (You may optionally set one or more of):
        
        ___seed region selection___
        -seed_mask <seed_mask>      -   seed mask (3D or 4D image file)
                                        if 4D, each volume is treated as a separate seed region
        -anat <anatomical>          -   anatomical data (3D image file)
        -parcel <parcellation>      -   parcellation of the anatomical data (3D image file)
        -seed_label <value>         -   parcel label for the seed region  
                                        default: parcels 95 and 96, which correspond to PCC in NVM parcellation 
        -seed_coor <x> <y> <z>      -   seed voxel coordinate
                                        example: -coor 47 41 23 corresponds to voxel x=47, y=41, z=23
        -kmeans  <num_clusters>     -   run k-means clustering on time series data of the seed region,
                                        new seed region is the largest cluster
        
        ___preprocessing___
        -smoothing <fwhm>           -   FWHM for spatial smoothing of correlation maps
        -scrub_list <scrub>         -   list of remove volume indices for scrubbing (a text file)
                                        indexing starts from 0
        -design_file <design>       -   FSL-FEAT design file for preprocessing (a .fsf file)
        -brain_mask <brain_mask>    -   brain mask (3D image file) for registration or global signal regression
        -wm_mask <wm_mask>          -   white matter mask for regressing out mean wm signal (3D image file)
        -csf_mask <csf_mask>        -   csf mask for regressing out mean csf signal (3D image file)
        -motion_parameters <mot>    -   motion parameters for regressing out motion parameters
                                        default: mcflirt will be run to get these parameters
        -detrend_clq                -   turn on constant, linear and quadratic detrending
        -nuisance_regression_yn <x> -   nuisance signal regression (1: yes, 0: no, default: yes)
        -affine_matrix <aff_mat>    -   affine transformation matrix (in .mat format) from example_func to anat
                                        requires two other files 'example_func', 'highres' in the same directory 
        -slice_timing <st>          -   slice timing for slice timing correction (a text file)
                                        if not set, slice timing correction is NOT performed
        -bbr                        -   turn on boundary based registration (BBR) from functional to anatomical
        -warpres <warp_res>         -   warp resolution for nonlinear registration from anatomical to standard
                                        if not set, nonlinear registration is NOT performed
        
        ___miscellaneous___
        -prefix <prefix>            -   filename prefix for the output correlation maps, default: 'corr_map'
        -reorient2std               -   reorient all the images to FSL standard orientation a-priori
        -use_our_preprocessing      -   use our preprocessing script (preprocess.sh) instead of feat preprocessing
        -help                       -   display this help text
        

    Examples:
        `basename $0` -in <input> -out <out_dir> -seed_mask <seed> -anat <anat> -design_file <design> -use_our_preprocessing 
        `basename $0` -in <input> -out <out_dir> -parcellation <parcellationNVM> -seed_label 95 -anat <anat>
        `basename $0` -in <input> -out <out_dir> -seed_coor 47 41 23
        `basename $0` -help

    Notes:
        1. Image files should be either in nifti (.nii/.nii.gz) or in nrrd (.nrrd) formats
        2. Slice timing should be normalized by the TR. Use exportSliceTimingFromJsonFile.py script
           to extract slice timing from a json file."
    }

rule () {
    printf -v _hr "%*s" $(tput cols) && echo -en ${_hr// /${2--}} && echo -e "\r\033[2C$1"

}

if [ $# -eq 0 ] ; then print_usage ; exit 1 ; fi 
#======================================================================

# default settings
outfile=corr_map
regress_out_nuisance_yn=1
code_dir=/common/motion/seyhmus/code

#======================================================================
rule "[ Options ]"
#----------------------------------------------------------------------

while [[ $1 != '' ]] ; do

    if [[ $1 == '-in' ]] 
    then
        func=$2
        func=$(realpath $(dirname $func))/$(basename $func)
        echo "Functional data:  $func"
        if [ ! -f $func ] ; then echo -e "\e[31mERROR: Functional data $func not found\e[0m" ; exit 1 ; fi
        shift; shift

    elif [[ $1 == '-out' ]]
    then
        output_dir=$2
        output_dir=$(realpath $(dirname ${output_dir}))/$(basename ${output_dir})
        echo "Output directory: ${output_dir}"
        if [ -d $output_dir ] ; then
            echo -e "\e[33mWARNING: Output directory $output_dir already exists\e[0m"
        fi
        shift; shift
        
    elif [[ $1 == '-seed_mask' ]]
    then
        seed_mask=$2
        seed_mask=$(realpath $(dirname $seed_mask))/$(basename $seed_mask)
        echo "Seed mask: $seed_mask"
        if [ ! -f $seed_mask ] ; then echo -e "\e[31mERROR: Seed mask $seed_mask not found\e[0m" ; exit 1; fi 
        shift; shift
    
    elif [[ $1 == '-parcellation' ]]
    then
        parcellation=$2
        parcellation=$(realpath $(dirname $parcellation))/$(basename $parcellation)
        echo "Parcellation: $parcellation"
        if [ ! -f $parcellation ] ; then 
            echo -e "\e[31mERROR: Parcellation $parcellation not found\e[0m" ; exit 1
        fi
        shift; shift

    elif [[ $1 == '-seed_label' ]]
    then
        seed_parcel_label=$2
        echo "Parcel label for the seed ROI: $seed_parcel_label"
        shift; shift
        
    elif [[ $1 == '-seed_coor' ]]
    then
        x_coor=$2 ; y_coor=$3 ; z_coor=$4
        seed_coordinate="$x_coor $y_coor $z_coor"
        echo "Seed coordinate:  $seed_coordinate"
        shift; shift; shift; shift

    elif [[ $1 == '-anat' ]]
    then
        anat=$2
        anat=$(realpath $(dirname $anat))/$(basename $anat)
        echo "Anatomical data:  $anat"
        if [ ! -f $anat ] ; then echo -e "\e[31mERROR: Anatomical data $anat not found\e[0m" ; exit 1 ; fi
        shift; shift

    elif [[ $1 == '-design_file' ]] 
    then
        design=$2
        echo "Design file: `realpath $design`"
        if [[ $design != *.fsf ]] ; then 
            echo -e "\e[31mERROR: Design file should have .fsf extension\e[0m" ; exit 1 
        fi
        shift; shift

    elif [[ $1 == '-brain_mask' ]]
    then
        brain_mask=$2
        brain_mask=$(realpath $(dirname $brain_mask))/$(basename $brain_mask)
        echo "Brain mask: $brain_mask"
        if [ ! -f $brain_mask ] ; then echo -e "\e[31mERROR: Brain mask $brain_mask not found\e[0m" ; exit 1 ; fi
        shift; shift

    elif [[ $1 == '-motion_parameters' ]] 
    then
        motion_parameters=$2
        motion_parameters=$(realpath $(dirname $motion_parameters))/$(basename $motion_parameters)
        echo "Motion parameters: $motion_parameters"
        if [ ! -f $motion_parameters ] ; then 
            echo -e "\e[31mERROR: Motion parameters file $motion_parameters not found\e[0m" ; exit 1 
        fi
        shift; shift

    elif [[ $1 == '-kmeans' ]] 
    then
        echo "Number of clusters for kmeans : $2"
        kmeans_clustering_yn=1
        num_clusters=$2
        renum='^[0-9]+$'
        if ! [[ $num_clusters =~ $renum ]] ; then
            echo -e "\e[31mERROR: Number of clusters $num_clusters not accepted\e[0m" ; exit 1
        fi
        shift; shift

    elif [[ $1 == '-prefix' ]] 
    then
        outfile=$2
        outfile=$(basename $(basename $outfile .gz) .nii)
        echo "Output correlation map filename prefix: $2"
        shift; shift

    elif [[ $1 == '-smoothing' ]]
    then
        echo "fwhm : $2"
        if [[ $2 -gt 0 ]] && [[ $2 -lt 50 ]] ; then
            smoothing_yn=1
            smoothing_fwhm=$2
        else
            echo -e "\e[33mWARNING: Fwhm outside range [0,50], smoothing turned off.\e[0m"
        fi
        shift; shift

    elif [[ $1 == '-scrub_list' ]] 
    then
        echo "Remove volume list for scrubbing and dmc: $2"
        remove_volume_indices_file=$2
        if [ ! -f $remove_volume_indices_file ] ; then 
            echo -e "\e[31mERROR: Remove volume indices file $remove_volume_indices_file not found \e[0m"
            exit 1 
        fi
        shift; shift

    elif [[ $1 == '-detrend_clq' ]] 
    then
        echo "Constant, linear and quadratic signal detrending: YES"
        detrend_clq_yn=1
        shift
    
    elif [[ $1 == '-nuisance_regression_yn' ]] 
    then
        if [[ $2 == '0' ]]
        then
            echo "Nuisance signal regression: NO" 
            regress_out_nuisance_yn=0
        fi
        shift; shift

    elif [[ $1 == '-affine_matrix' ]]
    then
        transfer_matrix_func2anat=`realpath $2`
        transfer_dir=`dirname $transfer_matrix_func2anat`
        transfer_matrix_anat2func=$transfer_dir/highres2example_func.mat
        example_func=$transfer_dir/example_func.nii.gz
        highres=$transfer_dir/highres.nii.gz
        echo "Transformation matrix from func to anat: $transfer_matrix_func2anat"
        if [ ! -f $transfer_matrix_func2anat ] || [[ $transfer_matrix_func2anat != *.mat ]] ; then
            echo -e "\e[31mERROR: Invalid transformation matrix $transfer_matrix_func2anat\e[0m"
            exit 1
        fi
        if [ ! -f $transfer_dir/example_func.nii.gz ] || [ ! -f $transfer_dir/highres.nii.gz ] ; then
            echo -e "\e[31mERROR: Either example_func.nii.gz or highres.nii.gz is missing from transformation matrix directory\e[0m"
            exit 1
        fi
        if [[ $transfer_matrix_anat2func != *.mat ]] ; then
            echo -e "\e[31mERROR: Transfer matrix from anat to func $transfer_matrix_anat2func not found\e[0m"
            exit 1
        fi
        shift; shift

    elif [[ $1 == '-warpres' ]]
    then
        echo "Non-rigid registration from anatomical to standard: YES"
        echo "Warp resolution: $2"
        non_rigid_reg_yn=1
        non_rigid_reg_warpres=$2
        shift; shift
        
    elif [[ $1 == '-wm_mask' ]]
    then
        wm_mask=$2
        wm_mask=$(realpath $(dirname $wm_mask))/$(basename $wm_mask)
        echo "White matter mask: $wm_mask"
        if [ ! -f $wm_mask ] ; then 
            echo -e "\e[31mERROR: White matter mask $brain_mask not found\e[0m"
            exit 1 
        fi
        shift; shift
    
    elif [[ $1 == '-csf_mask' ]]
    then
        csf_mask=$2
        csf_mask=$(realpath $(dirname $csf_mask))/$(basename $csf_mask)
        echo "CSF mask: $csf_mask"
        if [ ! -f $csf_mask ] ; then echo -e "\e[31mERROR: CSF mask $brain_mask not found\e[0m" ; exit 1 ; fi
        shift; shift

    elif [[ $1 == '-slice_timing' ]] 
    then
        stfile=`realpath $2`
        echo "Slice timing file (normalized by TR): $stfile"
        if [ ! -f $stfile ] ; then echo -e "\e[31mERROR: Slice timing file $stfile not found\e[0m" ; exit 1 ; fi
        shift; shift

    elif [[ $1 == '-bbr' ]]
    then
        echo "Boundary based registration (BBR) from functional to anatomical : YES"
        bbr_yn=1
        shift

    elif [[ $1 == '-reorient2std' ]]
    then
        echo "Reorient to FSL standard space a-priori: YES"
        reorient2std_yn=1
        shift
    
    elif [[ $1 == '-use_our_preprocessing' ]]
    then
        echo "Use our script for preprocessing: YES" 
        use_our_script_for_preprocessing_yn=1
        shift
        
    elif [[ $1 == '-normalize_after_nui_regression' ]]
    then
        echo "Normalize after nuisance signal regression: YES"
        normalize_after_nuisance_regression_yn=1
        shift

    elif [[ $1 == '-help' ]]
    then
        print_usage
        exit 1
    else
        echo -e "\e[31mERROR: Unrecognized option: $1 \e[0m"
        exit 1
    fi
done

# check required variables
if [ -z $func ] || [ -z $output_dir ] ; then
    print_usage
    echo " $func"
    echo -e "\e[31mERROR: At least one compulsory argument is missing\e[0m"
    exit 1 
fi

# define variable FSLDIR in case you would like to use another FSL installation
if [ -z "$FSLDIR" ] ; then 
    FSLDIR=`which fsl`
    FSLDIR=`realpath $FSLDIR`
    FSLDIR=`dirname $FSLDIR`
    FSLDIR=`dirname $FSLDIR`
    if [ -z "$FSLDIR" ] ; then
        echo -e "\e[31mERROR: Couldn't find FSL directory\e[0m"
        exit 1
    fi
fi

mkdir -p $output_dir
#======================================================================


#======================================================================
rule "[ File formating and reorientation to standard space ]"
#----------------------------------------------------------------------

# convert nrrd to nifti
if [[ $func == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $func -out ${output_dir}/func.nii.gz
    func=${output_dir}/func.nii.gz
fi

if [[ $anat == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $anat -out ${output_dir}/anat.nii.gz
    anat=${output_dir}/anat.nii.gz
fi

if [[ $parcellation == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $parcellation -out ${output_dir}/parcellation.nii.gz
    parcellation=${output_dir}/parcellation.nii.gz
fi

if [[ $seed_mask == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $seed_mask -out ${output_dir}/seed_mask.nii.gz
    seed_mask=${output_dir}/seed_mask.nii.gz
fi

if [[ $brain_mask == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $brain_mask -out ${output_dir}/brain_mask.nii.gz
    brain_mask=${output_dir}/brain_mask.nii.gz
fi

if [[ $wm_mask == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $wm_mask -out ${output_dir}/wm_mask.nii.gz
    wm_mask=${output_dir}/wm_mask.nii.gz
fi

if [[ $csf_mask == *.nrrd ]] ; then
    crlConvertBetweenFileFormats -in $csf_mask -out ${output_dir}/csf_mask.nii.gz
    csf_mask=${output_dir}/csf_mask.nii.gz
fi

# if orientation to FSL standard space is turned on 
if [[ $reorient2std_yn -eq 1 ]] ; then
    echo "Reorienting the images to FSL standard orientation space" 
        ${FSLDIR}/bin/fslreorient2std $func ${output_dir}/func.nii.gz
        func=${output_dir}/func.nii.gz
    if [ ! -z $anat ] ; then
        ${FSLDIR}/bin/fslreorient2std $anat ${output_dir}/anat.nii.gz
        anat=${output_dir}/anat.nii.gz
    fi
    if [ ! -z $parcellation ] ; then
        ${FSLDIR}/bin/fslreorient2std $parcellation ${output_dir}/parcellation.nii.gz
        parcellation=${output_dir}/parcellation.nii.gz
    fi
    if [ ! -z $seed_mask ] ; then
        ${FSLDIR}/bin/fslreorient2std $seed_mask ${output_dir}/seed_mask.nii.gz
        seed_mask=${output_dir}/seed_mask.nii.gz
    fi
    if [ ! -z $brain_mask ] ; then
        ${FSLDIR}/bin/fslreorient2std $brain_mask ${output_dir}/brain_mask.nii.gz
        brain_mask=${output_dir}/brain_mask.nii.gz
    fi
    if [ ! -z $wm_mask ] ; then
        ${FSLDIR}/bin/fslreorient2std $wm_mask ${output_dir}/wm_mask.nii.gz
        wm_mask=${output_dir}/wm_mask.nii.gz
    fi
    if [ ! -z $csf_mask ] ; then
        ${FSLDIR}/bin/fslreorient2std $csf_mask ${output_dir}/csf_mask.nii.gz
        csf_mask=${output_dir}/csf_mask.nii.gz
    fi

fi

#======================================================================



#======================================================================
rule "[ Brain, seed, wm, csf masks ]"
#----------------------------------------------------------------------

dims_func=`fslinfo $func | grep dim | head -n 3 | awk {'print $2'}`
if [ ! -z $anat ] ; then 
    dims_anat=`fslinfo $anat | grep dim | head -n 3 | awk {'print $2'}`
fi

if [ ! -z $design ] ; then
    reg_standard_yn=`cat $design | grep "set fmri(regstandard_yn)" | awk {'print $3'}`
    if [[ $reg_standard_yn -eq 1 ]] ; then
        standard=`cat $design | grep "set fmri(regstandard)" | awk {'print $3'} | tr -d '"'`
        if [ ! -f ${standard}.nii.gz ] ; then
            echo -e "\e[31mERROR: Standard image $standard not found \e[0m"
            exit 1
        else
            ${FSLDIR}/bin/fslmaths ${standard}.nii.gz ${output_dir}/standard.nii.gz
            standard=${output_dir}/standard.nii.gz
            ${FSLDIR}/bin/fslmaths ${standard} -bin ${output_dir}/brain_mask_stan.nii.gz
            brain_mask_stan=${output_dir}/brain_mask_stan.nii.gz
            dims_standard=`fslinfo $standard | grep dim | head -n 3 | awk {'print $2'}`
        fi
    fi
fi


#-------------------[ brain mask ]-------------------------

if [ ! -z $brain_mask ] ; then
    dims_brain_mask=`fslinfo $brain_mask | grep dim | head -n 3 | awk {'print $2'}`
    if [[ $dims_brain_mask == $dims_func ]] ; then
        ${FSLDIR}/bin/fslmaths $brain_mask ${output_dir}/brain_mask_func.nii.gz
        brain_mask_func=${output_dir}/brain_mask_func.nii.gz
    elif [[ $dims_brain_mask == $dims_anat ]] ; then
        ${FSLDIR}/bin/fslmaths $brain_mask ${output_dir}/brain_mask_anat.nii.gz
        ${FSLDIR}/bin/fslmaths $anat -mas ${output_dir}/brain_mask_anat.nii.gz ${output_dir}/anat_brain.nii.gz
        brain_mask_anat=${output_dir}/brain_mask_anat.nii.gz
    elif [[ $dims_brain_mask == $dims_standard ]] ; then
        ${FSLDIR}/bin/fslmaths $brain_mask ${output_dir}/brain_mask_stan.nii.gz
        brain_mask_stan=${output_dir}/brain_mask_stan.nii.gz
    else
        echo -e "\e[31mERROR: Mismatch between dimensions of brain mask and functional/anatomical/standard images\e[0m"
        exit 1
    fi

else
    if [ ! -z $parcellation ] ; then
        ${FSLDIR}/bin/fslmaths $parcellation -bin ${output_dir}/brain_mask_anat.nii.gz
        if [ ! -z $anat ] ; then
            ${FSLDIR}/bin/fslmaths $anat -mas ${output_dir}/brain_mask_anat.nii.gz ${output_dir}/anat_brain.nii.gz
            brain_mask_anat=${output_dir}/brain_mask_anat.nii.gz
        else
            echo -e "\e[31mERROR: Anatomical image not found for parcellation $parcellation\e[0m"
            exit 1
        fi

    elif [ ! -z $anat ] ; then
        # using BET for brain extraction (if no brain mask was found)
        # if you want to use another tool, place the code below. 
        echo -e "\e[33mWARNING: Using BET for brain extraction\e[0m"
        ${FSLDIR}/bin/bet $anat ${output_dir}/anat_brain -m -R
        mv ${output_dir}/anat_brain_mask.nii.gz ${output_dir}/brain_mask_anat.nii.gz
        brain_mask_anat=${output_dir}/brain_mask_anat.nii.gz

    else
        echo -e "\e[33mWARNING: No brain mask is defined\e[0m"
        ${FSLDIR}/bin/bet $func ${output_dir}/func_brain -f 0.3 -m -R
        rm ${output_dir}/func_brain.nii.gz
        mv ${output_dir}/func_brain_mask.nii.gz ${output_dir}/brain_mask_func.nii.gz
        brain_mask_func=${output_dir}/brain_mask_func.nii.gz
    fi
fi


# -----------------[ seed mask ]---------------------

if [ ! -z $seed_mask ] ; then
    dims_seed_mask=`fslinfo $seed_mask | grep dim | head -n 3 | awk {'print $2'}`
    if [[ $dims_seed_mask == $dims_func ]] ; then
        ${FSLDIR}/bin/fslmaths $seed_mask ${output_dir}/seed_mask_func.nii.gz
        seed_mask_func=${output_dir}/seed_mask_func.nii.gz
    elif [[ $dims_seed_mask == $dims_anat ]] ; then
        ${FSLDIR}/bin/fslmaths $seed_mask ${output_dir}/seed_mask_anat.nii.gz
        seed_mask_anat=${output_dir}/seed_mask_anat.nii.gz
    elif [[ $dims_seed_mask == $dims_standard ]] ; then
        ${FSLDIR}/bin/fslmaths $seed_mask ${output_dir}/seed_mask_stan.nii.gz
        seed_mask_stan=${output_dir}/seed_mask_stan.nii.gz
    else
        echo -e "\e[31mERROR: Mismatch between dimensions of seed mask and functional/anatomical/standard images\e[0m"
        exit 1
    fi

elif [ ! -z $x_coor ] ; then
    ${FSLDIR}/bin/fslmaths $func -roi $x_coor 1 $y_coor 1 $z_coor 1 0 1 -bin ${output_dir}/seed_mask_func.nii.gz
    seed_mask_func=${output_dir}/seed_mask_func.nii.gz

elif [ ! -z $parcellation ] ; then
    if [ ! -z $anat ] ; then
        if [ ! -z $seed_parcel_label ] ; then
            $FSLDIR/bin/fslmaths $parcellation -thr $seed_parcel_label -uthr $seed_parcel_label -bin ${output_dir}/seed_mask_anat.nii.gz
            seed_mask_anat=${output_dir}/seed_mask_anat.nii.gz
        else
            echo -e "\e[33mWARNING: Selecting parcels 95 and 96 (PCC) as seed region\e[0m"
            $FSLDIR/bin/fslmaths $parcellation -thr 95 -uthr 96 -bin ${output_dir}/seed_mask_anat.nii.gz
            seed_mask_anat=${output_dir}/seed_mask_anat.nii.gz
        fi
    else
        echo -e "\e[31mERROR: Anatomical image not found for parcellation $parcellation\e[0m"
        exit 1
    fi
else
    echo -e "\e[33mWARNING: No seed region is defined. Whole brain will be selected as seed\e[0m"
    if [ ! -z $brain_mask_func ] ; then
        seed_mask_func=$brain_mask_func
    else
        echo -e "\e[31mERROR: No seed region and brain mask is defined. Exiting...\e[0m"
        exit 1
    fi
fi


# --------------------[ csf mask ]-----------------------

if [ ! -z $csf_mask ] ; then
    dims_csf_mask=`fslinfo $csf_mask | grep dim | head -n 3 | awk {'print $2'}`
    if [[ $dims_csf_mask == $dims_func ]] ; then
        ${FSLDIR}/bin/fslmaths $csf_mask ${output_dir}/csf_mask_func.nii.gz
        csf_mask_func=${output_dir}/csf_mask_func.nii.gz
    elif [[ $dims_csf_mask == $dims_anat ]] ; then
        ${FSLDIR}/bin/fslmaths $csf_mask ${output_dir}/csf_mask_anat.nii.gz
        csf_mask_anat=${output_dir}/csf_mask_anat.nii.gz
    elif [[ $dims_csf_mask == $dims_standard ]] ; then
        ${FSLDIR}/bin/fslmaths $csf_mask ${output_dir}/csf_mask_stan.nii.gz
        csf_mask_stan=${output_dir}/csf_mask_stan.nii.gz
    else
        echo -e "\e[31mERROR: Mismatch between dimensions of csf mask and functional/anatomical/standard images\e[0m"
        exit 1
    fi
fi

# -------------------[ wm mask ]------------------------
if [ ! -z $wm_mask ] ; then
    dims_wm_mask=`fslinfo $wm_mask | grep dim | head -n 3 | awk {'print $2'}`
    if [[ $dims_wm_mask == $dims_func ]] ; then
        ${FSLDIR}/bin/fslmaths $wm_mask ${output_dir}/wm_mask_func.nii.gz
        wm_mask_func=${output_dir}/wm_mask_func.nii.gz
    elif [[ $dims_wm_mask == $dims_anat ]] ; then
        ${FSLDIR}/bin/fslmaths $wm_mask ${output_dir}/wm_mask_anat.nii.gz
        wm_mask_anat=${output_dir}/wm_mask_anat.nii.gz
    elif [[ $dims_wm_mask == $dims_standard ]] ; then
        ${FSLDIR}/bin/fslmaths $wm_mask ${output_dir}/wm_mask_stan.nii.gz
        wm_mask_stan=${output_dir}/wm_mask_stan.nii.gz
    else
        echo -e "\e[31mERROR: Mismatch between dimensions of wm mask and functional/anatomical/standard images\e[0m"
        exit 1
    fi
fi

#======================================================================



#======================================================================
rule "[ Preprocessing ]"
#----------------------------------------------------------------------

# change template design file
if [ ! -z $design ] ; then
    cp $design ${output_dir}/design.fsf
    feat_output_dir=${output_dir}/preprocessing

    #input functional data, output directory
    ${code_dir}/changeFeatDesignFile.sh -i $func -o $feat_output_dir ${output_dir}/design.fsf

    #motion correction
    if [[ -z $motion_parameters ]] ; then
        ${code_dir}/changeFeatDesignFile.sh -m 1 ${output_dir}/design.fsf
    else
        ${code_dir}/changeFeatDesignFile.sh -m 0 ${output_dir}/design.fsf
    fi
    
    # anatomical image
    if [ ! -z $anat ] ; then
        if [ ! -f ${output_dir}/anat_brain.nii.gz ] ; then
            if [ -z $brain_mask_anat ] ; then
                ${FSLDIR}/bin/bet $anat ${output_dir}/anat_brain -R
            else
                ${FSLDIR}/bin/fslmaths $anat -mas $brain_mask_anat ${output_dir}/anat_brain.nii.gz
            fi
        fi
        ${code_dir}/changeFeatDesignFile.sh -h ${output_dir}/anat_brain.nii.gz ${output_dir}/design.fsf
    fi

    #slice timing correction
    if [ ! -z $stfile ] ; then
        ${code_dir}/changeFeatDesignFile.sh -s 4 -t $stfile ${output_dir}/design.fsf
    else
        ${code_dir}/changeFeatDesignFile.sh -s 0 ${output_dir}/design.fsf
    fi

    #run only preprocessing
    ${FSLDIR}/bin/setFEAT -i ${output_dir}/design.fsf -analysis 1 

    #registration options
    if [[ $non_rigid_reg_yn -eq 1 ]] ; then
        ${FSLDIR}/bin/setFEAT -i ${output_dir}/design.fsf -FNIRT 1 -FNIRTwarpres $non_rigid_reg_warpres 
    fi
    if [[ $bbr_yn -eq 1 ]] ; then
        ${FSLDIR}/bin/setFEAT -i ${output_dir}/design.fsf -highresdof BBR
    fi
    
    # additional preprocessing options

    # number of delete volumes
    ${code_dir}/changeFeatDesignFile.sh -d 0 ${output_dir}/design.fsf

    #spatial smoothing fwhm (0 : no smoothing)
    ${FSLDIR}/bin/setFEAT -i ${output_dir}/design.fsf -smooth 0

    #temporal filtering (turned off, will be applied after nuisance signal regression)
    ${FSLDIR}/bin/setFEAT -i ${output_dir}/design.fsf -hpfilteron 0

    if [[ $use_our_script_for_preprocessing_yn -eq 1 ]] ; then
        # run preprocessing using our script 
        ${code_dir}/preprocess.sh ${output_dir}/design.fsf | tee ${output_dir}/preprocessing_log.txt
    else
        # run preprocessing using FEAT
        ${FSLDIR}/bin/feat ${output_dir}/design.fsf | tee ${output_dir}/preprocessing_log.txt
    fi

    feat_output_dir=${output_dir}/preprocessing.feat
    mv ${output_dir}/preprocessing_log.txt ${feat_output_dir}/
    transfer_matrix_anat2func=${feat_output_dir}/reg/highres2example_func.mat
    transfer_matrix_func2anat=${feat_output_dir}/reg/example_func2highres.mat
    transfer_matrix_stan2func=${feat_output_dir}/reg/standard2example_func.mat
    transfer_matrix_func2stan=${feat_output_dir}/reg/example_func2standard.mat
    example_func=${feat_output_dir}/reg/example_func.nii.gz
    highres=${feat_output_dir}/reg/highres.nii.gz
    func=${feat_output_dir}/filtered_func_data.nii.gz

    if [[ $non_rigid_reg_yn -eq 1 ]] ; then
        warp_func2stan=${feat_output_dir}/reg/example_func2standard_warp.nii.gz
        warp_anat2stan=${feat_output_dir}/reg/highres2standard_warp.nii.gz
        ${FSLDIR}/bin/invwarp -w $warp_func2stan -o ${feat_output_dir}/reg/standard2example_func_warp.nii.gz -r $example_func
        warp_stan2func=${feat_output_dir}/reg/standard2example_func_warp.nii.gz
        ${FSLDIR}/bin/invwarp -w $warp_anat2stan -o ${feat_output_dir}/reg/standard2highres_warp.nii.gz -r $highres
        warp_stan2anat=${feat_output_dir}/reg/standard2highres_warp.nii.gz
    fi
    
    if [ -z $motion_parameters ] ; then
        ${FSLDIR}/bin/mp_diffpow.sh ${feat_output_dir}/mc/motion_parameters_original_6.par ${feat_output_dir}/mc/motion_parameters_extended_18
        paste -d ' ' ${feat_output_dir}/mc/motion_parameters_original_6.par ${feat_output_dir}/mc/motion_parameters_extended_18.dat > ${feat_output_dir}/mc/motion_parameters_all_24.par
        motion_parameters=${feat_output_dir}/mc/motion_parameters_all_24.par
    fi
    cp $motion_parameters ${output_dir}/motion_parameters.par
else
    echo "There is no design file for preprocessing. Nothing to do."
    if [[ ! -z $seed_mask ]] && [[ -z $seed_mask_stan ]] ; then 
        seed_mask_stan=$seed_mask
    fi
    if [[ ! -z $brain_mask ]] && [[ -z $brain_mask_stan ]] ; then 
        brain_mask_stan=$brain_mask
    fi
fi

#======================================================================



#======================================================================
rule "[ Transform masks to functional space ]"
#----------------------------------------------------------------------

binarize_transformed_masks_yn=0
if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
    cut_off_percent=25
fi

# ---------[ brain mask ]--------------
if [ -z $brain_mask_func ] ; then
    if [ ! -z $brain_mask_stan ] ; then
        $FSLDIR/bin/flirt -in $brain_mask_stan -ref ${example_func} -applyxfm -init ${transfer_matrix_stan2func} -out ${output_dir}/brain_mask_func.nii.gz
        if [ ! -z $warp_stan2anat ] ; then
            $FSLDIR/bin/applywarp -i $brain_mask_stan -o ${output_dir}/brain_mask_func.nii.gz -r $example_func -w $warp_stan2func
        fi
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/brain_mask_func -thrp ${cut_off_percent} -bin ${output_dir}/brain_mask_func.nii.gz
        fi
        brain_mask_func=${output_dir}/brain_mask_func.nii.gz
    elif [ ! -z $brain_mask_anat ] ; then
        ${FSLDIR}/bin/flirt -in $brain_mask_anat -ref ${example_func} -applyxfm -init ${transfer_matrix_anat2func} -out ${output_dir}/brain_mask_func.nii.gz
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/brain_mask_func -thrp ${cut_off_percent} -bin ${output_dir}/brain_mask_func.nii.gz
        fi
        brain_mask_func=${output_dir}/brain_mask_func.nii.gz
    else
        echo -e "\e[33mWARNING: No brain mask is defined\e[0m"
        ${FSLDIR}/bin/fslmaths $func -bin ${output_dir}/brain_mask_func
        brain_mask_func=${output_dir}/brain_mask_func.nii.gz
    fi
fi

# ----------[ seed mask ]----------------
if [ -z $seed_mask_func ] ; then
    if [ ! -z $seed_mask_anat ] ; then
        ${FSLDIR}/bin/flirt -in $seed_mask_anat -ref ${example_func} -applyxfm -init ${transfer_matrix_anat2func} -out ${output_dir}/seed_mask_func.nii.gz
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/seed_mask_func.nii.gz -thrp ${cut_off_percent} -bin ${output_dir}/seed_mask_func.nii.gz
        fi
        seed_mask_func=${output_dir}/seed_mask_func.nii.gz
    elif [ ! -z $seed_mask_stan ] ; then
        ${FSLDIR}/bin/flirt -in $seed_mask_stan -ref ${example_func} -applyxfm -init ${transfer_matrix_stan2func} -out ${output_dir}/seed_mask_func.nii.gz
        if [ ! -z $warp_stan2anat ] ; then 
            ${FSLDIR}/bin/applywarp -i $seed_mask_stan -o ${output_dir}/seed_mask_func.nii.gz -r $example_func -w $warp_stan2func
        fi
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/seed_mask_func.nii.gz -thrp ${cut_off_percent} -bin ${output_dir}/seed_mask_func.nii.gz
        fi
        seed_mask_func=${output_dir}/seed_mask_func.nii.gz
    else
        seed_mask_func=$brain_mask_func
    fi
fi

# -----------[ csf mask ]-----------------
if [ -z $csf_mask_func ] ; then
    if [ ! -z $csf_mask_anat ] ; then
        ${FSLDIR}/bin/flirt -in $csf_mask_anat -ref ${example_func} -applyxfm -init ${transfer_matrix_anat2func} -out ${output_dir}/csf_mask_func.nii.gz
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/csf_mask_func.nii.gz -thrp ${cut_off_percent} -bin ${output_dir}/csf_mask_func.nii.gz
        fi
        csf_mask_func=${output_dir}/csf_mask_func.nii.gz
    elif [ ! -z $csf_mask_stan ] ; then
        ${FSLDIR}/bin/flirt -in $csf_mask_stan -ref ${example_func} -applyxfm -init ${transfer_matrix_stan2func} -out ${output_dir}/csf_mask_func.nii.gz
        if [ ! -z $warp_stan2anat ] ; then 
            ${FSLDIR}/bin/applywarp -i $csf_mask_stan -o ${output_dir}/csf_mask_func.nii.gz -r $example_func -w $warp_stan2func
        fi
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/csf_mask_func.nii.gz -thrp ${cut_off_percent} -bin ${output_dir}/csf_mask_func.nii.gz
        fi
        csf_mask_func=${output_dir}/csf_mask_func.nii.gz
    else
        echo -e "\e[33mWARNING: No CSF mask is defined. CSF signal will not be regressed out\e[0m"
    fi
fi

# -----------[ wm mask ]----------------
if [ -z $wm_mask_func ] ; then
    if [ ! -z $wm_mask_anat ] ; then
        ${FSLDIR}/bin/flirt -in $wm_mask_anat -ref ${example_func} -applyxfm -init ${transfer_matrix_anat2func} -out ${output_dir}/wm_mask_func.nii.gz
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/wm_mask_func.nii.gz -thrp ${cut_off_percent} -bin ${output_dir}/wm_mask_func.nii.gz
        fi
        wm_mask_func=${output_dir}/wm_mask_func.nii.gz
    elif [ ! -z $wm_mask_stan ] ; then
        ${FSLDIR}/bin/flirt -in $wm_mask_stan -ref ${example_func} -applyxfm -init ${transfer_matrix_stan2func} -out ${output_dir}/wm_mask_func.nii.gz
        if [ ! -z $warp_stan2anat ] ; then 
            ${FSLDIR}/bin/applywarp -i $wm_mask_stan -o ${output_dir}/wm_mask_func.nii.gz -r $example_func -w $warp_stan2func
        fi
        if [[ $binarize_transformed_masks_yn -eq 1 ]] ; then
            ${FSLDIR}/bin/fslmaths ${output_dir}/wm_mask_func.nii.gz -thrp ${cut_off_percent} -bin ${output_dir}/wm_mask_func.nii.gz
        fi
        wm_mask_func=${output_dir}/wm_mask_func.nii.gz
    else
        echo -e "\e[33mWARNING: No WM mask is defined. WM signal will not be regressed out\e[0m"
    fi
fi

#======================================================================



#======================================================================
rule "[ Nuisance signal removal ]"
#----------------------------------------------------------------------

tr_in_seconds=`fslval $func pixdim4`
func_temp_name=$(basename $(basename $func .gz) .nii)
save_results_to=${output_dir}/preprocessing.feat/nui
gs_removal_outfile=$save_results_to/${func_temp_name}_nui

nui_options=
if [ ! -z $brain_mask_func ] ; then
    nui_options="$nui_options -g 1 -b $brain_mask_func"
fi
if [ ! -z $wm_mask_func ] ; then
    nui_options="$nui_options -w 1 -m $wm_mask_func"
fi
if [ ! -z $csf_mask_func ] ; then
    nui_options="$nui_options -c 1 -f $csf_mask_func"
fi
if [ ! -z $motion_parameters ] ; then
    nui_options="$nui_options -d $motion_parameters"
fi

if [[ $detrend_clq_yn -eq 1 ]] ; then
    nui_options="$nui_options -t -l -q"
fi

if [[ $normalize_after_nuisance_regression_yn -eq 1 ]] ; then
    nui_options="$nui_options -n"
fi

if [[ $regress_out_nuisance_yn -eq 1 ]] ; then
    mkdir -p $save_results_to
    pushd $save_results_to
    echo "${code_dir}/regressOutNuisanceSignal.sh -i $func -o $gs_removal_outfile $nui_options"
    ${code_dir}/regressOutNuisanceSignal.sh -i $func -o $gs_removal_outfile $nui_options
    func=${gs_removal_outfile}_res.nii.gz
    popd
fi

#======================================================================



#======================================================================
rule "[ Temporal filtering ]"
#----------------------------------------------------------------------
func_basename=$(basename $(basename $func .gz) .nii)
python ${code_dir}/bandpass_nii.py $func $tr_in_seconds ${output_dir}/${func_basename}_tempfilt_python_script.nii.gz
func=${output_dir}/${func_basename}_tempfilt_python_script.nii.gz
#======================================================================



#======================================================================
rule "[ Scrubbing ]"
#----------------------------------------------------------------------

if [ ! -z $remove_volume_indices_file ] ; then
    func_basename=$(basename $(basename $func .gz) .nii)
    cp $remove_volume_indices_file ${output_dir}/remove_volumes_list.txt
    echo " Remove volume list for scrubbing : " 
    cat ${output_dir}/remove_volumes_list.txt
    ${code_dir}/scrubBasedOnRemoveVolumeList.sh $func ${output_dir}/remove_volumes_list.txt
    func=${output_dir}/${func_basename}_scrubbed.nii.gz
fi

#======================================================================



#======================================================================
rule "[ Functional to MNI space ]" 
#----------------------------------------------------------------------
if [ ! -z $design ] ; then
    func_basename=$(basename $(basename $func .gz) .nii)
    if [[ $non_rigid_reg_yn -eq 1 ]] ; then
        ${FSLDIR}/bin/applywarp -i $func -r $standard -o ${output_dir}/${func_basename}_warped_to_MNI -w $warp_func2stan
        func=${output_dir}/${func_basename}_warped_to_MNI.nii.gz
    else
        ${FSLDIR}/bin/flirt -in $func -ref $standard -applyxfm -init ${transfer_matrix_func2stan} -out ${output_dir}/${func_basename}_MNI
        func=${output_dir}/${func_basename}_MNI.nii.gz
    fi
fi

#======================================================================



#======================================================================
rule "[ Kmeans clustering ]"
#----------------------------------------------------------------------

if [ ! -z $num_clusters ] && [ $kmeans_clustering_yn -eq 1 ] ; then
    
    mkdir -p ${output_dir}/seed_clusters
    
    echo "Running kmeans clustering on time series data of the seed region."
    echo "Check ${output_dir}/seed_clusters for the clustering results."
    
    echo "cd ${output_dir}/seed_clusters/;" > matlab_commands.m
    echo "addpath('$code_dir');" >> matlab_commands.m
    echo "addpath('$output_dir/seed_clusters/');" >> matlab_commands.m
    echo "addpath('~/Documents/MATLAB/NiftiTools');" >> matlab_commands.m
    echo "func_data='$func';" >> matlab_commands.m
    echo "roi_mask='$seed_mask_func';" >> matlab_commands.m
    echo "k = $num_clusters;" >> matlab_commands.m
    echo "cd ${output_dir}/seed_clusters/;" >> matlab_commands.m
    echo "clusterROIBasedOnTimeSeries(func_data,roi_mask,k);" >> matlab_commands.m
    echo "exit;" >> matlab_commands.m
    matlab -nosplash -nodesktop -r "matlab_commands"
    mv matlab_commands.m ${output_dir}/seed_clusters/matlab_commands.m

        clustered_seed=${outputdir}/seed_clusters/seed_mask_func_clustered
        $FSLDIR/bin/fslstats -K ${clustered_seed} ${clustered_seed} > ${output_dir}/seed_clusters/cluster_volumes.txt
    cluster_voxel_volumes=`awk -v ncl=$((num_clusters*2)) {' for (i=1;i<=ncl;i+=2) printf "%d ", $i '} ${output_dir}/seed_clusters/temp_cluster_volumes.txt`
    echo "Cluster volumes (in number of voxels) : $cluster_voxel_volumes" 
    echo $cluster_voxel_volumes > ${output_dir}/seed_clusters/cluster_volumes_in_voxels.txt


    # choose the cluster with highest volume as new seed region
    count=1
    new_seed_idx=1
    seed_vol=0
    while [ $count -le $num_clusters ] ; do
        ${FSLDIR}/bin/fslmaths ${clustered_seed} -thr $count -uthr $count -bin ${output_dir}/seed_clusters/cluster_$count
        if [ `echo $cluster_voxel_volumes | awk -v cnt=$count {'print $cnt'}` -gt $seed_vol ] ; then
            new_seed_idx=$count
            seed_vol=`echo $cluster_voxel_volumes | awk -v cnt=$count {'print $cnt'}`
        fi
        count=`echo " $count + 1 " | bc`
    done

    echo "Cluster $new_seed_idx is selected as the new seed region after kmeans clustering."
    $FSLDIR/bin/fslmaths $output_dir/seed_clusters/cluster_${new_seed_idx} ${output_dir}/seed_mask_func_kmeans.nii.gz
    seed_mask_func=$output_dir/seed_mask_func_kmeans.nii.gz

fi
#======================================================================



#======================================================================
rule "[ Seed-based functional connectivity ]"
#----------------------------------------------------------------------

num_of_seed_ROIs=`fslval $seed_mask_stan dim4`
echo "Number of seed regions: $num_of_seed_ROIs"
seed_roi_idx=0
mkdir -p ${output_dir}/seed_rois
${FSLDIR}/bin/fslsplit $seed_mask_stan ${output_dir}/seed_rois/seed_roi_
for seed_roi in ${output_dir}/seed_rois/seed_roi_* ; do
    ${code_dir}/computePearsonsCorrelationCoefficient.sh $func $seed_roi
    seed_roi_basename=$(basename $(basename $seed_roi .gz) .nii)
    mv ${seed_roi_basename}_corr.nii.gz ${output_dir}/${outfile}_${seed_roi_basename}.nii.gz
    mv ${seed_roi_basename}_mts.txt ${output_dir}/${outfile}_${seed_roi_basename}_mts.txt
done

# merge correlation maps
${FSLDIR}/bin/fslmerge -t ${output_dir}/${outfile}.nii.gz ${output_dir}/${outfile}_*.nii.gz
paste ${output_dir}/${outfile}_*_mts.txt > ${output_dir}/${outfile}.txt
rm -f ${output_dir}/${outfile}_*

# mask correlation maps
if [[ ! -z $brain_mask_stan ]] ; then
    ${FSLDIR}/bin/fslmaths ${output_dir}/${outfile}.nii.gz -mas ${output_dir}/brain_mask_stan.nii.gz ${output_dir}/${outfile}.nii.gz
fi


# fisher z transform
${code_dir}/applyFisherTransformUsingFslTools.sh ${output_dir}/${outfile}.nii.gz ${output_dir}/${outfile}_fisherz.nii.gz

# smoothing
if [[ ${smoothing_yn} -eq 1 ]] ; then
    smoothing_sigma=`echo " $smoothing_fwhm / 2.355 " | bc -l`
    ${FSLDIR}/bin/fslmaths ${output_dir}/${outfile}_fisherz.nii.gz -s $smoothing_sigma ${output_dir}/${outfile}_fisherz_fwhm-${smoothing_fwhm}.nii.gz
fi
#======================================================================
rule "[ DONE! ]"
