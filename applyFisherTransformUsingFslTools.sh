#! /bin/sh
# Applies Fisher Z-transformation to a correlation map image using FSL tools.
#
# author : seyhmus guler
# date   : May 9, 2017
#
#
#======================================================================
# check the inputs
#----------------------------------------------------------------------

if [ $# -ne 2 ] ; then 
    echo "Usage: $0 <input> <output>"
    exit 1 
fi

data=$1

minmax=`${FSLDIR}/bin/fslstats $data -R`
min_intensity=`echo $minmax | awk {'print $1'}`
max_intensity=`echo $minmax | awk {'print $2'}`

${FSLDIR}/bin/fslmaths $data -nan withoutNan
data=withoutNan.nii.gz
if [ `echo " $min_intensity >= -1 " | bc -l` -eq 1 ] && [ `echo " $max_intensity <= 1 " | bc -l` -eq 1 ] ; then
    ${FSLDIR}/bin/fslmaths $data -add 1 corr_plus_1
    corr_plus_one=corr_plus_1.nii.gz
    ${FSLDIR}/bin/fslmaths $data -mul -1 -add 1 minus_corr_plus_1
    minus_corr_plus_one=minus_corr_plus_1.nii.gz
    ${FSLDIR}/bin/fslmaths $corr_plus_one -div $minus_corr_plus_one -log -mul 0.5 $2 -odt float
else
    echo -e "\e[31mERROR: The input image $data is not a correlation map (there exists values outside [-1,1] range)\e[0m"
fi

rm -f withoutNan.nii.gz corr_plus_1.nii.gz minus_corr_plus_1.nii.gz 
