#! /bin/sh

if [ ! $# -eq 2 ] ; then echo "Usage: $0 <fsl_motion_outliers> <outliers_one_column_format>. "; exit 1; fi

awk '{sum=0
for(i=1;i<=NF;i++)
    {sum+=$i}
    print sum
}' $1 > $2



#    if(sum==0) 
#        sum+=1; 
#    else
#        sum-=1; 
