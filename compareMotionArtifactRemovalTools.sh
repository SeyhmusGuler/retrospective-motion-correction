#! /bin/sh
# compares fix, aroma, scrubbing for denoising and motion artifact removal in rsfMRI -incomplete
#
# author : seyhmus guler
# date : 3/16/2017
#
#
#
# Notes:
#   1.  BIDS input data structure is assumed, 
#       Check http://bids.neuroimaging.io/ for details of BIDS file structure


#======================================================================
# checking the inputs
#----------------------------------------------------------------------

if [ $# -lt 2 ] ; then
    echo "Usage: `basename $0` <bids_dir> <output_dir>"
    echo "Usage: `basename $0` <bids_dir> <output_dir> --participant_label <participant_IDs>"  
    exit 1 

elif [ $# -gt 2 ] ; then
    
    bids_dir=$1
    output_dir=$2
    
    if [ $3 == "--participant_label" ] ; then
        num_subjects=$(($#-3))
        subject_id_array=${*: -$num_subjects:$num_subjects} 
        echo "Number of subjects : $num_subjects"
        echo "Subject IDs : $subject_id_array"
    else
        echo "Usage: `basename $0` <bids_cir> <output_dir> --participant_label <participant_IDs>"
        echo "Note that 
        exit 1
    fi

elif [ $# -eq 2 ] ; then
bids_dir=$1
output_dir=$2
    ~/bids-validate/bin/bids-validator $bids_dir --verbose --ignoreWarnings > bids_validator_log
    log_first_line=`head -n 1 bids_validator_log`
    if [ "$log_first_line" == "This dataset appears to be BIDS compatible." ] ; then
        echo $log_first_line
        echo "Subjects in the dataset : "
    else
        echo "BIDS validator ERROR: $log_first_line"
        exit 1
    fi
fi

mkdir -p $output_dir
mv bids_validator_log ${output_dir}/
#======================================================================

#======================================================================
# running the analysis
#----------------------------------------------------------------------
