
# coding: utf-8

# In[1]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
from nipype.interfaces import c3
import nipype.pipeline.engine as pe
import nipype.interfaces.afni as afni
from nipype.utils.filemanip import fname_presuffix
import nibabel as nb
import matplotlib.pyplot as plt
import nilearn as nil
import numpy as np
import nipype.interfaces.io as nio
import os
import pandas as pd
from textwrap3 import indent
import nipype.algorithms.confounds as nac
from nilearn.masking import compute_epi_mask
from nilearn.image import concat_imgs
from nilearn import image as nli
from nilearn.image import index_img
import tempfile
from __future__ import print_function
from bids import BIDSLayout
import re
from copy import deepcopy
from mimetypes import guess_type
import shutil


import nibabel as nb
import os.path as op
import numpy as np
import scipy
from scipy import signal
import sys
from pathlib2 import Path

import warnings as _warnings
import os as _os
from nipype.utils.filemanip import load_json, save_json, split_filename, fname_presuffix
from os.path import join as opj
from os.path import abspath
from IPython.display import Image
from nipype.interfaces.base import (
    traits, TraitedSpec, isdefined, CommandLineInputSpec, CommandLine, BaseInterfaceInputSpec, 
    File, InputMultiPath, Str, Directory, OutputMultiPath, SimpleInterface, BaseInterface)


# In[2]:


LOCAL_DEFAULT_NUMBER_OF_THREADS = 1
PREFERED_ITKv4_THREAD_LIMIT_VARIABLE = 'NSLOTS'
ALT_ITKv4_THREAD_LIMIT_VARIABLE = 'ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS'


__all__ = ['BIDS_NAME']

BIDS_NAME = re.compile(
    r'^(.*\/)?'
    '(?P<subject_id>sub-[a-zA-Z0-9]+)'
    '(_(?P<session_id>ses-[a-zA-Z0-9]+))?'
    '(_(?P<task_id>task-[a-zA-Z0-9]+))?'
    '(_(?P<acq_id>acq-[a-zA-Z0-9]+))?'
    '(_(?P<rec_id>rec-[a-zA-Z0-9]+))?'
    '(_(?P<run_id>run-[a-zA-Z0-9]+))?')


class BIDSError(ValueError):
    def __init__(self, message, bids_root):
        indent = 10
        header = '{sep} BIDS root folder: "{bids_root}" {sep}'.format(
            bids_root=bids_root, sep=''.join(['-'] * indent))
        self.msg = '\n{header}\n{indent}{message}\n{footer}'.format(
            header=header, indent=''.join([' '] * (indent + 1)),
            message=message, footer=''.join(['-'] * len(header))
        )
        super(BIDSError, self).__init__(self.msg)
        self.bids_root = bids_root


class BIDSWarning(RuntimeWarning):
    pass


# additional function and class definitions (needed to wrap some commandline tools)
class MCFLIRT2ITKInputSpec(BaseInterfaceInputSpec):
    in_files = InputMultiPath(File(exists=True), mandatory=True,
                              desc='list of MAT files from MCFLIRT')
    in_reference = File(exists=True, mandatory=True,
                        desc='input image for spatial reference')
    in_source = File(exists=True, mandatory=True,
                     desc='input image for spatial source')
    num_threads = traits.Int(1, usedefault=True, nohash=True,
                             desc='number of parallel processes')


class MCFLIRT2ITKOutputSpec(TraitedSpec):
    out_file = File(desc='the output ITKTransform file')

class MCFLIRT2ITK(SimpleInterface):

    input_spec = MCFLIRT2ITKInputSpec
    output_spec = MCFLIRT2ITKOutputSpec

    def _run_interface(self, runtime):
        num_threads = self.inputs.num_threads
        if num_threads < 1:
            num_threads = None
        with TemporaryDirectory(prefix='tmp-', dir=runtime.cwd) as tmp_folder:
            # Inputs are ready to run in parallel
            if num_threads is None or num_threads > 1:
                from concurrent.futures import ThreadPoolExecutor
                with ThreadPoolExecutor(max_workers=num_threads) as pool:
                    itk_outs = list(pool.map(_mat2itk, [
                        (in_mat, self.inputs.in_reference, self.inputs.in_source, i, tmp_folder)
                        for i, in_mat in enumerate(self.inputs.in_files)]
                    ))
            else:
                itk_outs = [_mat2itk((
                    in_mat, self.inputs.in_reference, self.inputs.in_source, i, tmp_folder))
                    for i, in_mat in enumerate(self.inputs.in_files)
                ]

        # Compose the collated ITK transform file and write
        tfms = '#Insight Transform File V1.0\n' + ''.join(
            [el[1] for el in sorted(itk_outs)])

        self._results['out_file'] = os.path.join(runtime.cwd, 'mat2itk.txt')
        with open(self._results['out_file'], 'w') as f:
            f.write(tfms)
        return runtime
    
def _mat2itk(args):
    from nipype.interfaces.c3 import C3dAffineTool
    from nipype.utils.filemanip import fname_presuffix

    in_file, in_ref, in_src, index, newpath = args
    # Generate a temporal file name
    out_file = fname_presuffix(in_file, suffix='_itk-%05d.txt' % index,
                               newpath=newpath)

    # Run c3d_affine_tool
    C3dAffineTool(transform_file=in_file, reference_file=in_ref, source_file=in_src,
                  fsl2ras=True, itk_transform=out_file, resource_monitor=False).run()
    transform = '#Transform %d\n' % index
    with open(out_file) as itkfh:
        transform += ''.join(itkfh.readlines()[2:])

    return (index, transform)


    
class EstimateReferenceImageInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc="4D EPI file")
    sbref_file = File(exists=True, desc="Single band reference image")
    mc_method = traits.Enum(
        "AFNI", "FSL", usedefault=True,
        desc="Which software to use to perform motion correction")


class EstimateReferenceImageOutputSpec(TraitedSpec):
    ref_image = File(exists=True, desc="3D reference image")
    n_volumes_to_discard = traits.Int(desc="Number of detected non-steady "
                                           "state volumes in the beginning of "
                                           "the input file")


class EstimateReferenceImage(SimpleInterface):
    """
    Given an 4D EPI file estimate an optimal reference image that could be later
    used for motion estimation and coregistration purposes. If detected uses
    T1 saturated volumes (non-steady state) otherwise a median of
    of a subset of motion corrected volumes is used.
    """
    input_spec = EstimateReferenceImageInputSpec
    output_spec = EstimateReferenceImageOutputSpec

    def _run_interface(self, runtime):
        if isdefined(self.inputs.sbref_file):
            self._results['ref_image'] = self.inputs.sbref_file
            return

        in_nii = nb.load(self.inputs.in_file)
        data_slice = in_nii.dataobj[:, :, :, :50]

        # Slicing may induce inconsistencies with shape-dependent values in extensions.
        # For now, remove all. If this turns out to be a mistake, we can select extensions
        # that don't break pipeline stages.
        # in_nii.header.extensions.clear()

        n_volumes_to_discard = _get_vols_to_discard(in_nii)

        out_ref_fname = os.path.abspath("ref_image.nii.gz")

        if n_volumes_to_discard == 0:
            if in_nii.shape[-1] > 40:
                slice_fname = os.path.abspath("slice.nii.gz")
                nb.Nifti1Image(data_slice[:, :, :, 20:40], in_nii.affine,
                               in_nii.header).to_filename(slice_fname)
            else:
                slice_fname = self.inputs.in_file

            if self.inputs.mc_method == "AFNI":
                res = afni.Volreg(in_file=slice_fname, args='-Fourier -twopass',
                                  zpad=4, outputtype='NIFTI_GZ').run()
            elif self.inputs.mc_method == "FSL":
                res = fsl.MCFLIRT(in_file=slice_fname,
                                  ref_vol=0, interpolation='sinc').run()
            mc_slice_nii = nb.load(res.outputs.out_file)

            median_image_data = np.median(mc_slice_nii.get_data(), axis=3)
        else:
            median_image_data = np.median(
                data_slice[:, :, :, :n_volumes_to_discard], axis=3)

        nb.Nifti1Image(median_image_data, in_nii.affine,
                       in_nii.header).to_filename(out_ref_fname)

        self._results["ref_image"] = out_ref_fname
        self._results["n_volumes_to_discard"] = n_volumes_to_discard

        return runtime
    
def _remove_volumes(bold_file, skip_vols):
    """remove skip_vols from bold_file"""
    import nibabel as nb
    from nipype.utils.filemanip import fname_presuffix
    if skip_vols == 0:
        return bold_file

    out = fname_presuffix(bold_file, suffix='_cut')
    bold_img = nb.load(bold_file)
    bold_img.__class__(bold_img.dataobj[..., skip_vols:],
                       bold_img.affine, bold_img.header).to_filename(out)
    
    return out


def _add_volumes(bold_file, bold_cut_file, skip_vols):
    """prepend skip_vols from bold_file onto bold_cut_file"""

    if skip_vols == 0:
        return bold_cut_file

    bold_img = nb.load(bold_file)
    bold_cut_img = nb.load(bold_cut_file)

    bold_data = np.concatenate((bold_img.dataobj[..., :skip_vols],
                                bold_cut_img.dataobj), axis=3)

    out = fname_presuffix(bold_cut_file, suffix='_addnonsteady')
    bold_img.__class__(bold_data, bold_img.affine, bold_img.header).to_filename(out)

    return out 

def _get_vols_to_discard(img):
    from nipype.algorithms.confounds import is_outlier
    data_slice = img.dataobj[:, :, :, :50]
    global_signal = data_slice.mean(axis=0).mean(axis=0).mean(axis=0)
    return is_outlier(global_signal)

class TemporaryDirectory(object):
    """Create and return a temporary directory.  This has the same
    behavior as mkdtemp but can be used as a context manager.  For
    example:

        with TemporaryDirectory() as tmpdir:
            ...

    Upon exiting the context, the directory and everything contained
    in it are removed.
    """

    def __init__(self, suffix="", prefix="tmp", dir=None):
        self._closed = False
        self.name = None # Handle mkdtemp raising an exception
        self.name = tempfile.mkdtemp(suffix, prefix, dir)

    def __repr__(self):
        return "<{} {!r}>".format(self.__class__.__name__, self.name)

    def __enter__(self):
        return self.name

    def cleanup(self, _warn=False):
        if self.name and not self._closed:
            try:
                self._rmtree(self.name)
            except (TypeError, AttributeError) as ex:
                # Issue #10188: Emit a warning on stderr
                # if the directory could not be cleaned
                # up due to missing globals
                if "None" not in str(ex):
                    raise
                print("ERROR: {!r} while cleaning up {!r}".format(ex, self,),
                      file=_sys.stderr)
                return
            self._closed = True
            if _warn:
                self._warn("Implicitly cleaning up {!r}".format(self),
                           ResourceWarning)

    def __exit__(self, exc, value, tb):
        self.cleanup()

    def __del__(self):
        # Issue a ResourceWarning if implicit cleanup needed
        self.cleanup(_warn=True)

    # XXX (ncoghlan): The following code attempts to make
    # this class tolerant of the module nulling out process
    # that happens during CPython interpreter shutdown
    # Alas, it doesn't actually manage it. See issue #10188
    _listdir = staticmethod(_os.listdir)
    _path_join = staticmethod(_os.path.join)
    _isdir = staticmethod(_os.path.isdir)
    _islink = staticmethod(_os.path.islink)
    _remove = staticmethod(_os.remove)
    _rmdir = staticmethod(_os.rmdir)
    _warn = _warnings.warn

    def _rmtree(self, path):
        # Essentially a stripped down version of shutil.rmtree.  We can't
        # use globals because they may be None'ed out at shutdown.
        for name in self._listdir(path):
            fullname = self._path_join(path, name)
            try:
                isdir = self._isdir(fullname) and not self._islink(fullname)
            except OSError:
                isdir = False
            if isdir:
                self._rmtree(fullname)
            else:
                try:
                    self._remove(fullname)
                except OSError:
                    pass
        try:
            self._rmdir(path)
        except OSError:
            pass
        
class calcMotionParamDiffInputSpec(CommandLineInputSpec):
    in_file = File(desc = "motion parameters file", exists = True, mandatory = True, argstr="-in %s")
    out_name = traits.Str(desc = "out file name", mandatory = True, argstr = "-out %s")
    
class calcMotionParamDiffOutputSpec(TraitedSpec):
    out_file = File(desc = "friston 24 motion parameters file", exists = True)

class calcMotionParamDiff(CommandLine):
    _cmd = '/fileserver/motion/seyhmus/code/mp_diffpow24.sh'
    input_spec = calcMotionParamDiffInputSpec
    output_spec = calcMotionParamDiffOutputSpec
      
    def _list_outputs(self):
        outputs = self.output_spec().get()
        fname = self.inputs.in_file
        outname = self.inputs.out_name
        dirname, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(dirname+ '/' + outname + '.dat')
        return outputs
        
class removeVolumeListInputSpec(CommandLineInputSpec):
    in_file = File(desc = "metric file", exists = True, mandatory = True, argstr="-in %s")
    thr = traits.Float(desc = "threshold value", mandatory = True, argstr='-thr %f')
    
class removeVolumeListOutputSpec(TraitedSpec):
    out_file = File(desc = "remove_volume_list_file", exists = True)

class removeVolumeList(CommandLine):
    _cmd = '/fileserver/motion/seyhmus/code/removeVolumeList.sh'
    input_spec = removeVolumeListInputSpec
    output_spec = removeVolumeListOutputSpec
      
    def _list_outputs(self):
        outputs = self.output_spec().get()
        fname = self.inputs.in_file
        dirname, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(dirname+ '/' + base + '_remove_volume_list.txt')
        return outputs
    
class fslvalInputSpec(FSLCommandInputSpec):
    in_file = File(desc = "input data", exists = True, mandatory = True, argstr="%s",
                  position=1)
    keyword = traits.Str(desc = "keyword", mandatory = True, argstr="%s", position=2)
    
class fslvalOutputSpec(TraitedSpec):
    out_stat = traits.Any(desc = "fslval output")
    
class fslValCommand(FSLCommand):
    input_spec = fslvalInputSpec
    output_spec = fslvalOutputSpec
    _cmd = 'fslval'

    def aggregate_outputs(self,runtime=None):
        outputs = self._outputs()
        outfile = os.path.join(os.getcwd(), 'stat_result.json')
        if runtime is None:
            try: 
                out_stat = load_json(outfile)['stat']
            except IOError:
                return self.run().outputs
        else:
            out_stat = []
            for line in runtime.stdout.split('\n'):
                if line:
                    values = line.split()
                    if len(values)>1:
                        out_stat.append([str(val) for val in values])
                    else:
                        out_stat.extend([str(val) for val in values])
            if len(out_stat)==1:
                out_stat = out_stat[0]
            save_json(outfile, dict(stat=out_stat))
        outputs.out_stat = out_stat
        return outputs 
    
class dmcInputSpec(CommandLineInputSpec):
    in_file = File(desc = "input fmri data", exists = True, mandatory = True, argstr="-in %s")
    remove_volumes = File(desc = "remove volume list", exists = True, mandatory = True,
                          argstr="-remove_list %s")
    
class dmcOutputSpec(TraitedSpec):
    out_file = File(desc = "completed fmri data", exists = True)

class dynamicMissingdataCompletion(CommandLine):
    _cmd = '/fileserver/motion/seyhmus/code/dmc.sh'
    input_spec = dmcInputSpec
    output_spec = dmcOutputSpec
      
    def _list_outputs(self):
        outputs = self.output_spec().get()
        fname = self.inputs.in_file
        dirname, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(dirname+ '/' + base + '_dmc.nii.gz')
        return outputs
    
class scrubInputSpec(CommandLineInputSpec):
    in_file = File(desc = "input fmri data", exists = True, mandatory = True, argstr="-in %s")
    remove_list = File(desc = "remove volume list", exists = True, mandatory = True,
                          argstr="-rm %s")
    
class scrubOutputSpec(TraitedSpec):
    out_file = File(desc = "scrubbed fmri data", exists = True)

class scrubBasedOnRemoveVolumeList(CommandLine):
    _cmd = '/fileserver/motion/seyhmus/code/scrubBasedOnRemoveVolumeList.sh'
    input_spec = scrubInputSpec
    output_spec = scrubOutputSpec
      
    def _list_outputs(self):
        outputs = self.output_spec().get()
        fname = self.inputs.in_file
        dirname, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(dirname+ '/' + base + '_scrubbed.nii.gz')
        return outputs

    
class updateConfoudsFileInputSpec(CommandLineInputSpec):
    in_file = File(desc = "input confounds tsv file", exists = True, mandatory = True, argstr="-in %s")
    skip_vols = traits.Int(0, usedefault=True, 
                           desc = "number of skip volumes",
                          argstr="-skip %d")

class updateConfoudsFileOutputSpec(TraitedSpec):
    out_file = File(desc = "updated confounds txt file", exists = True)

class updateConfoundsFile(CommandLine):
    _cmd = '/fileserver/motion/seyhmus/code/updateConfoundsTSVfile.sh'
    input_spec = updateConfoudsFileInputSpec
    output_spec = updateConfoudsFileOutputSpec
      
    def _list_outputs(self):
        outputs = self.output_spec().get()
        fname = self.inputs.in_file
        dirname, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(dirname+ '/' + base + '.txt')
        return outputs

class pearCorrCoefInputSpec(BaseInterfaceInputSpec):
    in_file=File(exists =True, desc = 'Mean time series', mandatory=True)
    
class pearCorrCoefOutputSpec(TraitedSpec):
    out_file=File(exists=True, desc = "Correlation coefficient matrix")
    
class pearCorrCoef(BaseInterface):
    input_spec = pearCorrCoefInputSpec
    output_spec = pearCorrCoefOutputSpec
    
    def _run_interface(self, runtime):
        fname=self.inputs.in_file
        data=np.loadtxt(fname)
        ccmat=np.corrcoef(data,rowvar=False)
        dirname, base, _ = split_filename(fname)
        np.savetxt(dirname + '/' + base + '_corrcoef.txt',ccmat)
        return runtime
    def _list_outputs(self):
        outputs = self.output_spec().get()
        fname = self.inputs.in_file
        dirname, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(dirname + '/' + base + '_corrcoef.txt')
        return outputs
    
def _read_cc_txt(txtfile):
    cc=np.loadtxt(txtfile)
    return cc
    
class bandpassInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists = True, desc = 'data to be bandpassed', mandatory=True)
    TR = traits.Float(desc='repetition time', mandatory = True)
    lpThr = traits.Float(desc='low pass cut-off frequency', mandatory=True)
    hpThr = traits.Float(desc='high pass cut-off frequency', mandatory=True)
    
class bandpassOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc="bandpassed data")
    
class bandpassFMRI(BaseInterface):
    input_spec = bandpassInputSpec
    output_spec = bandpassOutputSpec
    
    def _run_interface(self, runtime):
        fname = self.inputs.in_file
        samplingperiod = self.inputs.TR
        lp_cutoff = self.inputs.lpThr
        hp_cutoff = self.inputs.hpThr
        
        passHz = np.array([hp_cutoff, lp_cutoff])
        stopHz = np.array([hp_cutoff*0.8, lp_cutoff*1.1])
        
        gpass = 1
        gstop = 40
        
        img = nb.load(fname)
        
        samplingfreq = 1.0/samplingperiod
        wp = passHz/(samplingfreq/2)
        ws = stopHz/(samplingfreq/2)
        
        sos = scipy.signal.iirdesign(wp, ws, gpass, gstop, output="sos")
        filtered_data = scipy.signal.sosfiltfilt(sos, img.get_data(), axis=3, padtype="even", padlen=int(0.5*img.shape[3]))
        new_img = nb.Nifti1Image(filtered_data, img.affine, img.header)
        _, base, _ = split_filename(fname)
        nb.save(new_img, base + '_bandpassed.nii.gz')
        return runtime
    
    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_bandpassed.nii.gz')
        return outputs


class MaskEPIInputSpec(BaseInterfaceInputSpec):
    in_files = InputMultiPath(File(exists=True), mandatory=True,
                              desc='input EPI or list of files')
    lower_cutoff = traits.Float(0.2, usedefault=True)
    upper_cutoff = traits.Float(0.85, usedefault=True)
    connected = traits.Bool(True, usedefault=True)
    opening = traits.Int(2, usedefault=True)
    exclude_zeros = traits.Bool(False, usedefault=True)
    ensure_finite = traits.Bool(True, usedefault=True)
    target_affine = traits.Either(None, traits.File(exists=True),
                                  default=None, usedefault=True)
    target_shape = traits.Either(None, traits.File(exists=True),
                                 default=None, usedefault=True)
    no_sanitize = traits.Bool(False, usedefault=True)


class MaskEPIOutputSpec(TraitedSpec):
    out_mask = File(exists=True, desc='output mask')


class MaskEPI(SimpleInterface):
    input_spec = MaskEPIInputSpec
    output_spec = MaskEPIOutputSpec

    def _run_interface(self, runtime):
        masknii = compute_epi_mask(
            self.inputs.in_files,
            lower_cutoff=self.inputs.lower_cutoff,
            upper_cutoff=self.inputs.upper_cutoff,
            connected=self.inputs.connected,
            opening=self.inputs.opening,
            exclude_zeros=self.inputs.exclude_zeros,
            ensure_finite=self.inputs.ensure_finite,
            target_affine=self.inputs.target_affine,
            target_shape=self.inputs.target_shape
        )

        if self.inputs.no_sanitize:
            in_file = self.inputs.in_files
            if isinstance(in_file, list):
                in_file = in_file[0]
            nii = nb.load(in_file)
            qform, code = nii.get_qform(coded=True)
            masknii.set_qform(qform, int(code))
            sform, code = nii.get_sform(coded=True)
            masknii.set_sform(sform, int(code))

        self._results['out_mask'] = fname_presuffix(
            self.inputs.in_files[0], suffix='_mask', newpath=runtime.cwd)
        masknii.to_filename(self._results['out_mask'])
        return runtime
    
class CopyXFormInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='the file we get the data from')
    hdr_file = File(exists=True, mandatory=True, desc='the file we get the header from')


class CopyXFormOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='written file path')


class CopyXForm(SimpleInterface):
    """
    Copy the x-form matrices from `hdr_file` to `out_file`.
    """
    input_spec = CopyXFormInputSpec
    output_spec = CopyXFormOutputSpec

    def _run_interface(self, runtime):
        __version__ = '0.3.5'
        out_name = fname_presuffix(self.inputs.in_file,
                                   suffix='_xform',
                                   newpath=runtime.cwd)
        # Copy and replace header
        shutil.copy(self.inputs.in_file, out_name)
        _copyxform(self.inputs.hdr_file, out_name
                   #message='CopyXForm (niworkflows v%s)' % __version__)
                  )
        self._results['out_file'] = out_name
        return runtime
    
def _copyxform(ref_image, out_image, message=None):
    # Read in reference and output
    resampled = nb.load(out_image)
    orig = nb.load(ref_image)
    
    # Copy xform infos
    qform, qform_code = orig.header.get_qform(coded=True)
    sform, sform_code = orig.header.get_sform(coded=True)
    header = resampled.header.copy()
    header.set_qform(qform, int(qform_code))
    header.set_sform(sform, int(sform_code))
    header['descrip'] = 'xform matrices modified by %s.' % (message or '(unknown)')

    newimg = resampled.__class__(resampled.get_data(), orig.affine, header)
    newimg.to_filename(out_image)
    
def collect_data(dataset, participant_label, task=None, echo=None):
    
    layout = BIDSLayout(dataset, exclude=['derivatives', 'sourcedata'])
    queries = {
        'fmap': {'subject': participant_label, 'modality': 'fmap',
                 'extensions': ['nii', 'nii.gz']},
        'bold': {'subject': participant_label, 'modality': 'func', 'type': 'bold',
                 'extensions': ['nii', 'nii.gz']},
        'sbref': {'subject': participant_label, 'modality': 'func', 'type': 'sbref',
                  'extensions': ['nii', 'nii.gz']},
        'flair': {'subject': participant_label, 'modality': 'anat', 'type': 'FLAIR',
                  'extensions': ['nii', 'nii.gz']},
        't2w': {'subject': participant_label, 'modality': 'anat', 'type': 'T2w',
                'extensions': ['nii', 'nii.gz']},
        't1w': {'subject': participant_label, 'modality': 'anat', 'type': 'T1w',
                'extensions': ['nii', 'nii.gz']},
        'roi': {'subject': participant_label, 'modality': 'anat', 'type': 'roi',
                'extensions': ['nii', 'nii.gz']},
    }

    if task:
        queries['bold']['task'] = task

    if echo:
        queries['bold']['echo'] = echo

    subj_data = {modality: [x.filename for x in layout.get(**query)]
                 for modality, query in queries.items()}

    def _grp_echos(x):
        if '_echo-' not in x:
            return x
        echo = re.search("_echo-\\d*", x).group(0)
        return x.replace(echo, "_echo-?")

    if subj_data["bold"]:
        bold_sess = subj_data["bold"]

        if any(['_echo-' in bold for bold in bold_sess]):
            ses_uids = [list(bold) for _, bold in groupby(bold_sess, key=_grp_echos)]
            ses_uids = [x[0] if len(x) == 1 else x for x in ses_uids]
        else:
            ses_uids = bold_sess

    subj_data.update({"bold": ses_uids})
    #subj_data.update({"bold":bold_sess}) # added by Seyhmus

    return subj_data, layout

class ValidateImageInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='input image')


class ValidateImageOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='validated image')
    out_report = File(exists=True, desc='HTML segment containing warning')


class ValidateImage(SimpleInterface):
    """
    Check the correctness of x-form headers (matrix and code)
    This interface implements the `following logic
    <https://github.com/poldracklab/fmriprep/issues/873#issuecomment-349394544>`_:
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    | valid quaternions | `qform_code > 0` | `sform_code > 0` | `qform == sform` \
| actions                                        |
    +===================+==================+==================+==================\
+================================================+
    | True              | True             | True             | True             \
| None                                           |
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    | True              | True             | False            | *                \
| sform, scode <- qform, qcode                   |
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    | *                 | *                | True             | False            \
| qform, qcode <- sform, scode                   |
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    | *                 | False            | True             | *                \
| qform, qcode <- sform, scode                   |
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    | *                 | False            | False            | *                \
| sform, qform <- best affine; scode, qcode <- 1 |
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    | False             | *                | False            | *                \
| sform, qform <- best affine; scode, qcode <- 1 |
    +-------------------+------------------+------------------+------------------\
+------------------------------------------------+
    """
    input_spec = ValidateImageInputSpec
    output_spec = ValidateImageOutputSpec

    def _run_interface(self, runtime):
        img = nb.load(self.inputs.in_file)
        out_report = os.path.join(runtime.cwd, 'report.html')

        # Retrieve xform codes
        sform_code = int(img.header._structarr['sform_code'])
        qform_code = int(img.header._structarr['qform_code'])

        # Check qform is valid
        valid_qform = False
        try:
            qform = img.get_qform()
            valid_qform = True
        except ValueError:
            pass

        sform = img.get_sform()
        if np.linalg.det(sform) == 0:
            valid_sform = False
        else:
            RZS = sform[:3, :3]
            zooms = np.sqrt(np.sum(RZS * RZS, axis=0))
            valid_sform = np.allclose(zooms, img.header.get_zooms()[:3])

        # Matching affines
        matching_affines = valid_qform and np.allclose(qform, sform)

        # Both match, qform valid (implicit with match), codes okay -> do nothing, empty report
        if matching_affines and qform_code > 0 and sform_code > 0:
            self._results['out_file'] = self.inputs.in_file
            open(out_report, 'w').close()
            self._results['out_report'] = out_report
            return runtime

        # A new file will be written
        out_fname = fname_presuffix(self.inputs.in_file, suffix='_valid', newpath=runtime.cwd)
        self._results['out_file'] = out_fname

        # Row 2:
        if valid_qform and qform_code > 0 and (sform_code == 0 or not valid_sform):
            img.set_sform(qform, qform_code)
            warning_txt = 'Note on orientation: sform matrix set'
            description = """<p class="elem-desc">The sform has been copied from qform.</p>
"""
        # Rows 3-4:
        # Note: if qform is not valid, matching_affines is False
        elif (valid_sform and sform_code > 0) and (not matching_affines or qform_code == 0):
            img.set_qform(img.get_sform(), sform_code)
            warning_txt = 'Note on orientation: qform matrix overwritten'
            description = """<p class="elem-desc">The qform has been copied from sform.</p>
"""
            if not valid_qform and qform_code > 0:
                warning_txt = 'WARNING - Invalid qform information'
                description = """<p class="elem-desc">
    The qform matrix found in the file header is invalid.
    The qform has been copied from sform.
    Checking the original qform information from the data produced
    by the scanner is advised.
</p>
"""
        # Rows 5-6:
        else:
            affine = img.header.get_base_affine()
            img.set_sform(affine, nb.nifti1.xform_codes['scanner'])
            img.set_qform(affine, nb.nifti1.xform_codes['scanner'])
            warning_txt = 'WARNING - Missing orientation information'
            description = """<p class="elem-desc">
    FMRIPREP could not retrieve orientation information from the image header.
    The qform and sform matrices have been set to a default, LAS-oriented affine.
    Analyses of this dataset MAY BE INVALID.
</p>
"""
        snippet = '<h3 class="elem-title">%s</h3>\n%s\n' % (warning_txt, description)
        # Store new file and report
        img.to_filename(out_fname)
        with open(out_report, 'w') as fobj:
            fobj.write(indent(snippet, '\t' * 3))

        self._results['out_report'] = out_report
        return runtime
class GenerateSamplingReferenceInputSpec(BaseInterfaceInputSpec):
    fixed_image = File(exists=True, mandatory=True,
                       desc='the reference file, defines the FoV')
    moving_image = File(exists=True, mandatory=True, desc='the pixel size reference')
    xform_code = traits.Enum(None, 2, 4, usedefault=True,
                             desc='force xform code')
    fov_mask = traits.Either(None, File(exists=True), usedefault=True,
                             desc='mask to clip field of view (in fixed_image space)')


class GenerateSamplingReferenceOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='one file with all inputs flattened')


class GenerateSamplingReference(SimpleInterface):
    """
    Generates a reference grid for resampling one image keeping original resolution,
    but moving data to a different space (e.g. MNI).
    If the `fov_mask` optional input is provided, then the abbr:`FoV (field-of-view)`
    is cropped to a bounding box containing the brain mask plus an offest of two
    voxels along all dimensions. The `fov_mask` should be to the brain mask calculated
    from the T1w, and should not contain the brain stem. The mask is resampled into
    target space, and then the bounding box is calculated. Finally, the FoV is adjusted
    to that bounding box.
    """

    input_spec = GenerateSamplingReferenceInputSpec
    output_spec = GenerateSamplingReferenceOutputSpec

    def _run_interface(self, runtime):
        self._results['out_file'] = _gen_reference(
            self.inputs.fixed_image,
            self.inputs.moving_image,
            fov_mask=self.inputs.fov_mask,
            force_xform_code=self.inputs.xform_code
            #message='%s (niworkflows v%s)' % (self.__class__.__name__, __version__)
        )
        return runtime
def _gen_reference(fixed_image, moving_image, fov_mask=None, out_file=None,
                   message=None, force_xform_code=None):
    """
    Generates a sampling reference, and makes sure xform matrices/codes are
    correct
    """

    if out_file is None:
        out_file = fname_presuffix(fixed_image,
                                   suffix='_reference',
                                   newpath=os.getcwd())

    new_zooms = nli.load_img(moving_image).header.get_zooms()[:3]
    # Avoid small differences in reported resolution to cause changes to
    # FOV. See https://github.com/poldracklab/fmriprep/issues/512
    new_affine = np.diag(np.round(new_zooms, 3))

    resampled = nli.resample_img(fixed_image,
                                 target_affine=new_affine,
                                 interpolation='nearest')

    if fov_mask is not None:
        # If we have a mask, resample again dropping (empty) samples
        # out of the FoV.
        fixednii = nb.load(fixed_image)
        masknii = nb.load(fov_mask)

        if np.all(masknii.shape[:3] != fixednii.shape[:3]):
            raise RuntimeError(
                'Fixed image and mask do not have the same dimensions.')

        if not np.allclose(masknii.affine, fixednii.affine, atol=1e-5):
            raise RuntimeError(
                'Fixed image and mask have different affines')

        # Get mask into reference space
        masknii = nli.resample_img(fixed_image,
                                   target_affine=new_affine,
                                   interpolation='nearest')
        res_shape = np.array(masknii.shape[:3])

        # Calculate a bounding box for the input mask
        # with an offset of 2 voxels per face
        bbox = np.argwhere(masknii.get_data() > 0)
        new_origin = np.clip(bbox.min(0) - 2, a_min=0, a_max=None)
        new_end = np.clip(bbox.max(0) + 2, a_min=0,
                          a_max=res_shape - 1)

        # Find new origin, and set into new affine
        new_affine_4 = resampled.affine.copy()
        new_affine_4[:3, 3] = new_affine_4[:3, :3].dot(
            new_origin) + new_affine_4[:3, 3]

        # Calculate new shapes
        new_shape = new_end - new_origin + 1
        resampled = nli.resample_img(fixed_image,
                                     target_affine=new_affine_4,
                                     target_shape=new_shape.tolist(),
                                     interpolation='nearest')

    xform = resampled.affine  # nibabel will pick the best affine
    _, qform_code = resampled.header.get_qform(coded=True)
    _, sform_code = resampled.header.get_sform(coded=True)

    xform_code = sform_code if sform_code > 0 else qform_code
    if xform_code == 1:
        xform_code = 2

    if force_xform_code is not None:
        xform_code = force_xform_code

    # Keep 0, 2, 3, 4 unchanged
    resampled.header.set_qform(xform, int(xform_code))
    resampled.header.set_sform(xform, int(xform_code))
    resampled.header['descrip'] = 'reference image generated by %s.' % (
        message or '(unknown software)')
    resampled.to_filename(out_file)
    return out_file
class ANTSCommandInputSpec(CommandLineInputSpec):
    """Base Input Specification for all ANTS Commands
    """

    num_threads = traits.Int(
        LOCAL_DEFAULT_NUMBER_OF_THREADS,
        usedefault=True,
        nohash=True,
        desc="Number of ITK threads to use")


class ANTSCommand(CommandLine):
    """Base class for ANTS interfaces
    """

    input_spec = ANTSCommandInputSpec
    _num_threads = LOCAL_DEFAULT_NUMBER_OF_THREADS

    def __init__(self, **inputs):
        super(ANTSCommand, self).__init__(**inputs)
        self.inputs.on_trait_change(self._num_threads_update, 'num_threads')

        if not isdefined(self.inputs.num_threads):
            self.inputs.num_threads = self._num_threads
        else:
            self._num_threads_update()

    def _num_threads_update(self):
        self._num_threads = self.inputs.num_threads
        # ONLY SET THE ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS if requested
        # by the end user.  The default setting did not allow for
        # overwriting the default values.
        # In ITKv4 (the version used for all ANTS programs), ITK respects
        # the SGE controlled $NSLOTS environmental variable.
        # If user specifies -1, then that indicates that the system
        # default behavior should be the one specified by ITKv4 rules
        # (i.e. respect SGE $NSLOTS or environmental variables of threads, or
        # user environmental settings)
        if (self.inputs.num_threads == -1):
            if (ALT_ITKv4_THREAD_LIMIT_VARIABLE in self.inputs.environ):
                del self.inputs.environ[ALT_ITKv4_THREAD_LIMIT_VARIABLE]
            if (PREFERED_ITKv4_THREAD_LIMIT_VARIABLE in self.inputs.environ):
                del self.inputs.environ[PREFERED_ITKv4_THREAD_LIMIT_VARIABLE]
        else:
            self.inputs.environ.update({
                PREFERED_ITKv4_THREAD_LIMIT_VARIABLE:
                '%s' % self.inputs.num_threads
            })

    @staticmethod
    def _format_xarray(val):
        """ Convenience method for converting input arrays [1,2,3] to
        commandline format '1x2x3' """
        return 'x'.join([str(x) for x in val])

    @classmethod
    def set_default_num_threads(cls, num_threads):
        """Set the default number of threads for ITK calls
        This method is used to set the default number of ITK threads for all
        the ANTS interfaces. However, setting this will not update the output
        type for any existing instances.  For these, assign the
        <instance>.inputs.num_threads
        """
        cls._num_threads = num_threads

    @property
    def version(self):
        return Info.version()
    
class ApplyTransformsInputSpec(ANTSCommandInputSpec):
    dimension = traits.Enum(
        2,
        3,
        4,
        argstr='--dimensionality %d',
        desc=('This option forces the image to be treated '
              'as a specified-dimensional image. If not '
              'specified, antsWarp tries to infer the '
              'dimensionality from the input image.'))
    input_image_type = traits.Enum(
        0,
        1,
        2,
        3,
        argstr='--input-image-type %d',
        desc=('Option specifying the input image '
              'type of scalar (default), vector, '
              'tensor, or time series.'))
    input_image = File(
        argstr='--input %s',
        mandatory=True,
        desc=('image to apply transformation to (generally a '
              'coregistered functional)'),
        exists=True)
    output_image = traits.Str(
        argstr='--output %s',
        desc='output file name',
        genfile=True,
        hash_files=False)
    out_postfix = traits.Str(
        "_trans",
        usedefault=True,
        desc=('Postfix that is appended to all output '
              'files (default = _trans)'))
    reference_image = File(
        argstr='--reference-image %s',
        mandatory=True,
        desc='reference image space that you wish to warp INTO',
        exists=True)
    interpolation = traits.Enum(
        'Linear',
        'NearestNeighbor',
        'CosineWindowedSinc',
        'WelchWindowedSinc',
        'HammingWindowedSinc',
        'LanczosWindowedSinc',
        'MultiLabel',
        'Gaussian',
        'BSpline',
        argstr='%s',
        usedefault=True)
    interpolation_parameters = traits.Either(
        traits.Tuple(traits.Int()),  # BSpline (order)
        traits.Tuple(
            traits.Float(),  # Gaussian/MultiLabel (sigma, alpha)
            traits.Float()))
    transforms = traits.Either(
        InputMultiPath(File(exists=True)),
        'identity',
        argstr='%s',
        mandatory=True,
        desc='transform files: will be applied in reverse order. For '
        'example, the last specified transform will be applied first.')
    invert_transform_flags = InputMultiPath(traits.Bool())
    default_value = traits.Float(
        0.0, argstr='--default-value %g', usedefault=True)
    print_out_composite_warp_file = traits.Bool(
        False,
        requires=["output_image"],
        desc='output a composite warp file instead of a transformed image')
    float = traits.Bool(
        argstr='--float %d',
        default_value=False,
        usedefault=True,
        desc='Use float instead of double for computations.')


class ApplyTransformsOutputSpec(TraitedSpec):
    output_image = File(exists=True, desc='Warped image')


class ApplyTransforms(ANTSCommand):
    """ApplyTransforms, applied to an input image, transforms it according to a
    reference image and a transform (or a set of transforms).
    Examples
    --------
    >>> from nipype.interfaces.ants import ApplyTransforms
    >>> at = ApplyTransforms()
    >>> at.inputs.input_image = 'moving1.nii'
    >>> at.inputs.reference_image = 'fixed1.nii'
    >>> at.inputs.transforms = 'identity'
    >>> at.cmdline
    'antsApplyTransforms --default-value 0 --float 0 --input moving1.nii \
--interpolation Linear --output moving1_trans.nii \
--reference-image fixed1.nii -t identity'
    >>> at = ApplyTransforms()
    >>> at.inputs.dimension = 3
    >>> at.inputs.input_image = 'moving1.nii'
    >>> at.inputs.reference_image = 'fixed1.nii'
    >>> at.inputs.output_image = 'deformed_moving1.nii'
    >>> at.inputs.interpolation = 'Linear'
    >>> at.inputs.default_value = 0
    >>> at.inputs.transforms = ['ants_Warp.nii.gz', 'trans.mat']
    >>> at.inputs.invert_transform_flags = [False, False]
    >>> at.cmdline
    'antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input moving1.nii \
--interpolation Linear --output deformed_moving1.nii --reference-image fixed1.nii \
--transform [ ants_Warp.nii.gz, 0 ] --transform [ trans.mat, 0 ]'
    >>> at1 = ApplyTransforms()
    >>> at1.inputs.dimension = 3
    >>> at1.inputs.input_image = 'moving1.nii'
    >>> at1.inputs.reference_image = 'fixed1.nii'
    >>> at1.inputs.output_image = 'deformed_moving1.nii'
    >>> at1.inputs.interpolation = 'BSpline'
    >>> at1.inputs.interpolation_parameters = (5,)
    >>> at1.inputs.default_value = 0
    >>> at1.inputs.transforms = ['ants_Warp.nii.gz', 'trans.mat']
    >>> at1.inputs.invert_transform_flags = [False, False]
    >>> at1.cmdline
    'antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input moving1.nii \
--interpolation BSpline[ 5 ] --output deformed_moving1.nii --reference-image fixed1.nii \
--transform [ ants_Warp.nii.gz, 0 ] --transform [ trans.mat, 0 ]'
    """
    _cmd = 'antsApplyTransforms'
    input_spec = ApplyTransformsInputSpec
    output_spec = ApplyTransformsOutputSpec

    def _gen_filename(self, name):
        if name == 'output_image':
            output = self.inputs.output_image
            if not isdefined(output):
                _, name, ext = split_filename(self.inputs.input_image)
                output = name + self.inputs.out_postfix + ext
            return output
        return None

    def _get_transform_filenames(self):
        retval = []
        for ii in range(len(self.inputs.transforms)):
            if isdefined(self.inputs.invert_transform_flags):
                if len(self.inputs.transforms) == len(
                        self.inputs.invert_transform_flags):
                    invert_code = 1 if self.inputs.invert_transform_flags[
                        ii] else 0
                    retval.append("--transform [ %s, %d ]" %
                                  (self.inputs.transforms[ii], invert_code))
                else:
                    raise Exception((
                        "ERROR: The useInverse list must have the same number "
                        "of entries as the transformsFileName list."))
            else:
                retval.append("--transform %s" % self.inputs.transforms[ii])
        return " ".join(retval)

    def _get_output_warped_filename(self):
        if isdefined(self.inputs.print_out_composite_warp_file):
            return "--output [ %s, %d ]" % (
                self._gen_filename("output_image"),
                int(self.inputs.print_out_composite_warp_file))
        else:
            return "--output %s" % (self._gen_filename("output_image"))

    def _format_arg(self, opt, spec, val):
        if opt == "output_image":
            return self._get_output_warped_filename()
        elif opt == "transforms":
            if val == 'identity':
                return '-t identity'
            return self._get_transform_filenames()
        elif opt == 'interpolation':
            if self.inputs.interpolation in ['BSpline', 'MultiLabel', 'Gaussian'] and                     isdefined(self.inputs.interpolation_parameters):
                return '--interpolation %s[ %s ]' % (
                    self.inputs.interpolation, ', '.join([
                        str(param)
                        for param in self.inputs.interpolation_parameters
                    ]))
            else:
                return '--interpolation %s' % self.inputs.interpolation
        return super(ApplyTransforms, self)._format_arg(opt, spec, val)

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['output_image'] = os.path.abspath(
            self._gen_filename('output_image'))
        return outputs


class FixHeaderApplyTransforms(ApplyTransforms):
    """
    A replacement for nipype.interfaces.ants.resampling.ApplyTransforms that
    fixes the resampled image header to match the xform of the reference
    image
    """

    def _run_interface(self, runtime, correct_return_codes=(0,)):
        # Run normally
        runtime = super(FixHeaderApplyTransforms, self)._run_interface(
            runtime, correct_return_codes)

        _copyxform(self.inputs.reference_image,
                   os.path.abspath(self._gen_filename('output_image'))
                   #message='%s (niworkflows v%s)' % (
                   #    self.__class__.__name__, __version__))
        )
        return runtime
class MultiApplyTransformsInputSpec(ApplyTransformsInputSpec):
    input_image = InputMultiPath(File(exists=True), mandatory=True,
                                 desc='input time-series as a list of volumes after splitting'
                                      ' through the fourth dimension')
    num_threads = traits.Int(1, usedefault=True, nohash=True,
                             desc='number of parallel processes')
    save_cmd = traits.Bool(True, usedefault=True,
                           desc='write a log of command lines that were applied')
    copy_dtype = traits.Bool(False, usedefault=True,
                             desc='copy dtype from inputs to outputs')


class MultiApplyTransformsOutputSpec(TraitedSpec):
    out_files = OutputMultiPath(File(), desc='the output ITKTransform file')
    log_cmdline = File(desc='a list of command lines used to apply transforms')


class MultiApplyTransforms(SimpleInterface):

    """
    Apply the corresponding list of input transforms
    """
    input_spec = MultiApplyTransformsInputSpec
    output_spec = MultiApplyTransformsOutputSpec

    def _run_interface(self, runtime):
        # Get all inputs from the ApplyTransforms object
        ifargs = self.inputs.get()

        # Extract number of input images and transforms
        in_files = ifargs.pop('input_image')
        num_files = len(in_files)
        transforms = ifargs.pop('transforms')
        # Get number of parallel jobs
        num_threads = ifargs.pop('num_threads')
        save_cmd = ifargs.pop('save_cmd')

        # Remove certain keys
        for key in ['environ', 'ignore_exception',
                    'terminal_output', 'output_image']:
            ifargs.pop(key, None)

        # Get a temp folder ready
        tmp_folder = TemporaryDirectory(prefix='tmp-', dir=runtime.cwd)

        xfms_list = _arrange_xfms(transforms, num_files, tmp_folder)
        assert len(xfms_list) == num_files

        # Inputs are ready to run in parallel
        if num_threads < 1:
            num_threads = None

        if num_threads == 1:
            out_files = [_applytfms((
                in_file, in_xfm, ifargs, i, runtime.cwd))
                for i, (in_file, in_xfm) in enumerate(zip(in_files, xfms_list))
            ]
        else:
            from concurrent.futures import ThreadPoolExecutor
            with ThreadPoolExecutor(max_workers=num_threads) as pool:
                out_files = list(pool.map(_applytfms, [
                    (in_file, in_xfm, ifargs, i, runtime.cwd)
                    for i, (in_file, in_xfm) in enumerate(zip(in_files, xfms_list))]
                ))
        tmp_folder.cleanup()

        # Collect output file names, after sorting by index
        self._results['out_files'] = [el[0] for el in out_files]

        if save_cmd:
            self._results['log_cmdline'] = os.path.join(runtime.cwd, 'command.txt')
            with open(self._results['log_cmdline'], 'w') as cmdfile:
                print('\n-------\n'.join([el[1] for el in out_files]),
                      file=cmdfile)
        return runtime
def _mat2itk(args):
    from nipype.interfaces.c3 import C3dAffineTool
    from nipype.utils.filemanip import fname_presuffix

    in_file, in_ref, in_src, index, newpath = args
    # Generate a temporal file name
    out_file = fname_presuffix(in_file, suffix='_itk-%05d.txt' % index,
                               newpath=newpath)

    # Run c3d_affine_tool
    C3dAffineTool(transform_file=in_file, reference_file=in_ref, source_file=in_src,
                  fsl2ras=True, itk_transform=out_file, resource_monitor=False).run()
    transform = '#Transform %d\n' % index
    with open(out_file) as itkfh:
        transform += ''.join(itkfh.readlines()[2:])

    return (index, transform)


def _applytfms(args):
    """
    Applies ANTs' antsApplyTransforms to the input image.
    All inputs are zipped in one tuple to make it digestible by
    multiprocessing's map
    """
    import nibabel as nb
    from nipype.utils.filemanip import fname_presuffix
    #from niworkflows.interfaces.fixes import FixHeaderApplyTransforms as ApplyTransforms

    in_file, in_xform, ifargs, index, newpath = args
    out_file = fname_presuffix(in_file, suffix='_xform-%05d' % index,
                               newpath=newpath, use_ext=True)

    copy_dtype = ifargs.pop('copy_dtype', False)
    #xfm = ApplyTransforms(
    xfm = FixHeaderApplyTransforms(
        input_image=in_file, transforms=in_xform, output_image=out_file, **ifargs)
    xfm.terminal_output = 'allatonce'
    xfm.resource_monitor = False
    runtime = xfm.run().runtime

    if copy_dtype:
        nii = nb.load(out_file)
        in_dtype = nb.load(in_file).get_data_dtype()

        # Overwrite only iff dtypes don't match
        if in_dtype != nii.get_data_dtype():
            nii.set_data_dtype(in_dtype)
            nii.to_filename(out_file)

    return (out_file, runtime.cmdline)


def _arrange_xfms(transforms, num_files, tmp_folder):
    """
    Convenience method to arrange the list of transforms that should be applied
    to each input file
    """
    base_xform = ['#Insight Transform File V1.0', '#Transform 0']
    # Initialize the transforms matrix
    xfms_T = []
    for i, tf_file in enumerate(transforms):
        # If it is a deformation field, copy to the tfs_matrix directly
        if guess_type(tf_file)[0] != 'text/plain':
            xfms_T.append([tf_file] * num_files)
            continue

        with open(tf_file) as tf_fh:
            tfdata = tf_fh.read().strip()

        # If it is not an ITK transform file, copy to the tfs_matrix directly
        if not tfdata.startswith('#Insight Transform File'):
            xfms_T.append([tf_file] * num_files)
            continue

        # Count number of transforms in ITK transform file
        nxforms = tfdata.count('#Transform')

        # Remove first line
        tfdata = tfdata.split('\n')[1:]

        # If it is a ITK transform file with only 1 xform, copy to the tfs_matrix directly
        if nxforms == 1:
            xfms_T.append([tf_file] * num_files)
            continue

        if nxforms != num_files:
            raise RuntimeError('Number of transforms (%d) found in the ITK file does not match'
                               ' the number of input image files (%d).' % (nxforms, num_files))

        # At this point splitting transforms will be necessary, generate a base name
        out_base = fname_presuffix(tf_file, suffix='_pos-%03d_xfm-{:05d}' % i,
                                   newpath=tmp_folder.name).format
        # Split combined ITK transforms file
        split_xfms = []
        for xform_i in range(nxforms):
            # Find start token to extract
            startidx = tfdata.index('#Transform %d' % xform_i)
            next_xform = base_xform + tfdata[startidx + 1:startidx + 4] + ['']
            xfm_file = out_base(xform_i)
            with open(xfm_file, 'w') as out_xfm:
                out_xfm.write('\n'.join(next_xform))
            split_xfms.append(xfm_file)
        xfms_T.append(split_xfms)

    # Transpose back (only Python 3)
    return list(map(list, zip(*xfms_T)))

class MergeInputSpec(BaseInterfaceInputSpec):
    in_files = InputMultiPath(File(exists=True), mandatory=True,
                              desc='input list of files to merge')
    dtype = traits.Enum('f4', 'f8', 'u1', 'u2', 'u4', 'i2', 'i4',
                        usedefault=True, desc='numpy dtype of output image')
    header_source = File(exists=True, desc='a Nifti file from which the header should be copied')
    compress = traits.Bool(True, usedefault=True, desc='Use gzip compression on .nii output')


class MergeOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='output merged file')


class Merge(SimpleInterface):
    input_spec = MergeInputSpec
    output_spec = MergeOutputSpec

    def _run_interface(self, runtime):
        ext = '.nii.gz' if self.inputs.compress else '.nii'
        self._results['out_file'] = fname_presuffix(
            self.inputs.in_files[0], suffix='_merged' + ext, newpath=runtime.cwd, use_ext=False)
        new_nii = concat_imgs(self.inputs.in_files, dtype=self.inputs.dtype)

        if isdefined(self.inputs.header_source):
            src_hdr = nb.load(self.inputs.header_source).header
            new_nii.header.set_xyzt_units(t=src_hdr.get_xyzt_units()[-1])
            new_nii.header.set_zooms(list(new_nii.header.get_zooms()[:3]) +
                                     [src_hdr.get_zooms()[3]])

        new_nii.to_filename(self._results['out_file'])

        return runtime
class MatchHeaderInputSpec(BaseInterfaceInputSpec):
    reference = File(exists=True, mandatory=True,
                     desc='NIfTI file with reference header')
    in_file = File(exists=True, mandatory=True,
                   desc='NIfTI file which header will be checked')


class MatchHeaderOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='NIfTI file with fixed header')


class MatchHeader(SimpleInterface):
    input_spec = MatchHeaderInputSpec
    output_spec = MatchHeaderOutputSpec

    def _run_interface(self, runtime):
        refhdr = nb.load(self.inputs.reference).header.copy()
        imgnii = nb.load(self.inputs.in_file)
        imghdr = imgnii.header.copy()

        imghdr['dim_info'] = refhdr['dim_info']  # dim_info is lost sometimes

        # Set qform
        qform = refhdr.get_qform()
        qcode = int(refhdr['qform_code'])
        #if not np.allclose(qform, imghdr.get_qform()):
            #LOGGER.warning(
            #    'q-forms of reference and mask are substantially different')
        imghdr.set_qform(qform, qcode)

        # Set sform
        sform = refhdr.get_sform()
        scode = int(refhdr['sform_code'])
        #if not np.allclose(sform, imghdr.get_sform()):
        #    LOGGER.warning(
        #        's-forms of reference and mask are substantially different')
        imghdr.set_sform(sform, scode)

        out_file = fname_presuffix(self.inputs.in_file, suffix='_hdr',
                                   newpath=runtime.cwd)

        imgnii.__class__(imgnii.get_data(), imghdr.get_best_affine(),
                         imghdr).to_filename(out_file)
        self._results['out_file'] = out_file
        return runtime
    
def _create_mem_gb(bold_fname):
    bold_size_gb = os.path.getsize(bold_fname) / (1024**3)
    bold_tlen = nb.load(bold_fname).shape[-1]
    mem_gb = {
        'filesize': bold_size_gb,
        'resampled': bold_size_gb * 4,
        'largemem': bold_size_gb * (max(bold_tlen / 100, 1.0) + 4),
    }

    return bold_tlen, mem_gb

class TPM2ROIInputSpec(BaseInterfaceInputSpec):
    in_tpm = File(exists=True, mandatory=True, desc='Tissue probability map file in T1 space')
    in_mask = File(exists=True, mandatory=True, desc='Binary mask of skull-stripped T1w image')
    mask_erode_mm = traits.Float(xor=['mask_erode_prop'],
                                 desc='erode input mask (kernel width in mm)')
    erode_mm = traits.Float(xor=['erode_prop'],
                            desc='erode output mask (kernel width in mm)')
    mask_erode_prop = traits.Float(xor=['mask_erode_mm'],
                                   desc='erode input mask (target volume ratio)')
    erode_prop = traits.Float(xor=['erode_mm'],
                              desc='erode output mask (target volume ratio)')
    prob_thresh = traits.Float(0.95, usedefault=True,
                               desc='threshold for the tissue probability maps')


class TPM2ROIOutputSpec(TraitedSpec):
    roi_file = File(exists=True, desc='output ROI file')
    eroded_mask = File(exists=True, desc='resulting eroded mask')


class TPM2ROI(SimpleInterface):
    """Convert tissue probability maps (TPMs) into ROIs
    This interface follows the following logic:
    #. Erode ``in_mask`` by ``mask_erode_mm`` and apply to ``in_tpm``
    #. Threshold masked TPM at ``prob_thresh``
    #. Erode resulting mask by ``erode_mm``
    """

    input_spec = TPM2ROIInputSpec
    output_spec = TPM2ROIOutputSpec

    def _run_interface(self, runtime):
        mask_erode_mm = self.inputs.mask_erode_mm
        if not isdefined(mask_erode_mm):
            mask_erode_mm = None
        erode_mm = self.inputs.erode_mm
        if not isdefined(erode_mm):
            erode_mm = None
        mask_erode_prop = self.inputs.mask_erode_prop
        if not isdefined(mask_erode_prop):
            mask_erode_prop = None
        erode_prop = self.inputs.erode_prop
        if not isdefined(erode_prop):
            erode_prop = None
        roi_file, eroded_mask = _tpm2roi(
            self.inputs.in_tpm,
            self.inputs.in_mask,
            mask_erode_mm,
            erode_mm,
            mask_erode_prop,
            erode_prop,
            self.inputs.prob_thresh,
            newpath=runtime.cwd,
        )
        self._results['roi_file'] = roi_file
        self._results['eroded_mask'] = eroded_mask
        return runtime
def _tpm2roi(in_tpm, in_mask, mask_erosion_mm=None, erosion_mm=None,
             mask_erosion_prop=None, erosion_prop=None, pthres=0.95,
             newpath=None):
    """
    Generate a mask from a tissue probability map
    """
    tpm_img = nb.load(in_tpm)
    roi_mask = (tpm_img.get_data() >= pthres).astype(np.uint8)

    eroded_mask_file = None
    erode_in = (mask_erosion_mm is not None and mask_erosion_mm > 0 or
                mask_erosion_prop is not None and mask_erosion_prop < 1)
    if erode_in:
        eroded_mask_file = fname_presuffix(in_mask, suffix='_eroded',
                                           newpath=newpath)
        mask_img = nb.load(in_mask)
        mask_data = mask_img.get_data().astype(np.uint8)
        if mask_erosion_mm:
            iter_n = max(int(mask_erosion_mm / max(mask_img.header.get_zooms())), 1)
            mask_data = nd.binary_erosion(mask_data, iterations=iter_n)
        else:
            orig_vol = np.sum(mask_data > 0)
            while np.sum(mask_data > 0) / orig_vol > mask_erosion_prop:
                mask_data = nd.binary_erosion(mask_data, iterations=1)

        # Store mask
        eroded = nb.Nifti1Image(mask_data, mask_img.affine, mask_img.header)
        eroded.set_data_dtype(np.uint8)
        eroded.to_filename(eroded_mask_file)

        # Mask TPM data (no effect if not eroded)
        roi_mask[~mask_data] = 0

    # shrinking
    erode_out = (erosion_mm is not None and erosion_mm > 0 or
                 erosion_prop is not None and erosion_prop < 1)
    if erode_out:
        if erosion_mm:
            iter_n = max(int(erosion_mm / max(tpm_img.header.get_zooms())), 1)
            iter_n = int(erosion_mm / max(tpm_img.header.get_zooms()))
            roi_mask = nd.binary_erosion(roi_mask, iterations=iter_n)
        else:
            orig_vol = np.sum(roi_mask > 0)
            while np.sum(roi_mask > 0) / orig_vol > erosion_prop:
                roi_mask = nd.binary_erosion(roi_mask, iterations=1)

    # Create image to resample
    roi_fname = fname_presuffix(in_tpm, suffix='_roi', newpath=newpath)
    roi_img = nb.Nifti1Image(roi_mask, tpm_img.affine, tpm_img.header)
    roi_img.set_data_dtype(np.uint8)
    roi_img.to_filename(roi_fname)
    return roi_fname, eroded_mask_file or in_mask

class AddTSVHeaderInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='input file')
    columns = traits.List(traits.Str, mandatory=True, desc='header for columns')


class AddTSVHeaderOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='output average file')


class AddTSVHeader(SimpleInterface):
    r"""Add a header row to a TSV file
    .. testsetup::
    >>> import pandas as pd
    >>> from tempfile import TemporaryDirectory
    >>> tmpdir = TemporaryDirectory()
    >>> os.chdir(tmpdir.name)
    .. doctest::
    An example TSV:
    >>> np.savetxt('data.tsv', np.arange(30).reshape((6, 5)), delimiter='\t')
    Add headers:
    >>> addheader = AddTSVHeader()
    >>> addheader.inputs.in_file = 'data.tsv'
    >>> addheader.inputs.columns = ['a', 'b', 'c', 'd', 'e']
    >>> res = addheader.run()
    >>> df = pd.read_csv(res.outputs.out_file, delim_whitespace=True,
    ...                  index_col=None)
    >>> df.columns.ravel().tolist()
    ['a', 'b', 'c', 'd', 'e']
    >>> np.all(df.values == np.arange(30).reshape((6, 5)))
    True
    .. testcleanup::
    >>> tmpdir.cleanup()
    """
    input_spec = AddTSVHeaderInputSpec
    output_spec = AddTSVHeaderOutputSpec

    def _run_interface(self, runtime):
        out_file = fname_presuffix(self.inputs.in_file, suffix='_motion.tsv', newpath=runtime.cwd,
                                   use_ext=False)
        data = np.loadtxt(self.inputs.in_file)
        np.savetxt(out_file, data, delimiter='\t', header='\t'.join(self.inputs.columns),
                   comments='')

        self._results['out_file'] = out_file
        return runtime


class JoinTSVColumnsInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='input file')
    join_file = File(exists=True, mandatory=True, desc='file to be adjoined')
    side = traits.Enum('right', 'left', usedefault=True, desc='where to join')
    columns = traits.List(traits.Str, desc='header for columns')


class JoinTSVColumnsOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='output TSV file')


class JoinTSVColumns(SimpleInterface):
    r"""Add a header row to a TSV file
    .. testsetup::
    >>> import os
    >>> import pandas as pd
    >>> import numpy as np
    >>> from tempfile import TemporaryDirectory
    >>> tmpdir = TemporaryDirectory()
    >>> os.chdir(tmpdir.name)
    .. doctest::
    An example TSV:
    >>> data = np.arange(30).reshape((6, 5))
    >>> np.savetxt('data.tsv', data[:, :3], delimiter='\t')
    >>> np.savetxt('add.tsv', data[:, 3:], delimiter='\t')
    Join without naming headers:
    >>> join = JoinTSVColumns()
    >>> join.inputs.in_file = 'data.tsv'
    >>> join.inputs.join_file = 'add.tsv'
    >>> res = join.run()
    >>> df = pd.read_csv(res.outputs.out_file, delim_whitespace=True,
    ...                  index_col=None, dtype=float, header=None)
    >>> df.columns.ravel().tolist() == list(range(5))
    True
    >>> np.all(df.values.astype(int) == data)
    True
    Adding column names:
    >>> join = JoinTSVColumns()
    >>> join.inputs.in_file = 'data.tsv'
    >>> join.inputs.join_file = 'add.tsv'
    >>> join.inputs.columns = ['a', 'b', 'c', 'd', 'e']
    >>> res = join.run()
    >>> res.outputs.out_file  # doctest: +ELLIPSIS
    '...data_joined.tsv'
    >>> df = pd.read_csv(res.outputs.out_file, delim_whitespace=True,
    ...                  index_col=None)
    >>> df.columns.ravel().tolist()
    ['a', 'b', 'c', 'd', 'e']
    >>> np.all(df.values == np.arange(30).reshape((6, 5)))
    True
    >>> join = JoinTSVColumns()
    >>> join.inputs.in_file = 'data.tsv'
    >>> join.inputs.join_file = 'add.tsv'
    >>> join.inputs.side = 'left'
    >>> join.inputs.columns = ['a', 'b', 'c', 'd', 'e']
    >>> res = join.run()
    >>> df = pd.read_csv(res.outputs.out_file, delim_whitespace=True,
    ...                  index_col=None)
    >>> df.columns.ravel().tolist()
    ['a', 'b', 'c', 'd', 'e']
    >>> np.all(df.values == np.hstack((data[:, 3:], data[:, :3])))
    True
    .. testcleanup::
    >>> tmpdir.cleanup()
    """
    input_spec = JoinTSVColumnsInputSpec
    output_spec = JoinTSVColumnsOutputSpec

    def _run_interface(self, runtime):
        out_file = fname_presuffix(
            self.inputs.in_file, suffix='_joined.tsv', newpath=runtime.cwd,
            use_ext=False)

        header = ''
        if isdefined(self.inputs.columns) and self.inputs.columns:
            header = '\t'.join(self.inputs.columns)

        with open(self.inputs.in_file) as ifh:
            data = ifh.read().splitlines(keepends=False)

        with open(self.inputs.join_file) as ifh:
            join = ifh.read().splitlines(keepends=False)

        assert len(data) == len(join)

        merged = []
        for d, j in zip(data, join):
            line = '%s\t%s' % ((j, d) if self.inputs.side == 'left' else (d, j))
            merged.append(line)

        if header:
            merged.insert(0, header)

        with open(out_file, 'w') as ofh:
            ofh.write('\n'.join(merged))

        self._results['out_file'] = out_file
        return runtime
class GatherConfoundsInputSpec(BaseInterfaceInputSpec):
    signals = File(exists=True, desc='input signals')
    dvars = File(exists=True, desc='file containing DVARS')
    std_dvars = File(exists=True, desc='file containing standardized DVARS')
    fd = File(exists=True, desc='input framewise displacement')
    tcompcor = File(exists=True, desc='input tCompCorr')
    acompcor = File(exists=True, desc='input aCompCorr')
    cos_basis = File(exists=True, desc='input cosine basis')
    motion = File(exists=True, desc='input motion parameters')
    aroma = File(exists=True, desc='input ICA-AROMA')


class GatherConfoundsOutputSpec(TraitedSpec):
    confounds_file = File(exists=True, desc='output confounds file')
    confounds_list = traits.List(traits.Str, desc='list of headers')


class GatherConfounds(SimpleInterface):
    """
    Combine various sources of confounds in one TSV file
    .. testsetup::
    >>> from tempfile import TemporaryDirectory
    >>> tmpdir = TemporaryDirectory()
    >>> os.chdir(tmpdir.name)
    .. doctest::
    >>> pd.DataFrame({'a': [0.1]}).to_csv('signals.tsv', index=False, na_rep='n/a')
    >>> pd.DataFrame({'b': [0.2]}).to_csv('dvars.tsv', index=False, na_rep='n/a')
    >>> gather = GatherConfounds()
    >>> gather.inputs.signals = 'signals.tsv'
    >>> gather.inputs.dvars = 'dvars.tsv'
    >>> res = gather.run()
    >>> res.outputs.confounds_list
    ['Global signals', 'DVARS']
    >>> pd.read_csv(res.outputs.confounds_file, sep='\s+', index_col=None,
    ...             engine='python')  # doctest: +NORMALIZE_WHITESPACE
         a    b
    0  0.1  0.2
    .. testcleanup::
    >>> tmpdir.cleanup()
    """
    input_spec = GatherConfoundsInputSpec
    output_spec = GatherConfoundsOutputSpec

    def _run_interface(self, runtime):
        combined_out, confounds_list = _gather_confounds(
            signals=self.inputs.signals,
            dvars=self.inputs.dvars,
            std_dvars=self.inputs.std_dvars,
            fdisp=self.inputs.fd,
            tcompcor=self.inputs.tcompcor,
            acompcor=self.inputs.acompcor,
            cos_basis=self.inputs.cos_basis,
            motion=self.inputs.motion,
            aroma=self.inputs.aroma,
            newpath=runtime.cwd,
        )
        self._results['confounds_file'] = combined_out
        self._results['confounds_list'] = confounds_list
        return runtime

def _gather_confounds(signals=None, dvars=None, std_dvars=None, fdisp=None,
                      tcompcor=None, acompcor=None, cos_basis=None,
                      motion=None, aroma=None, newpath=None):
    """
    Load confounds from the filenames, concatenate together horizontally
    and save new file.
    >>> from tempfile import TemporaryDirectory
    >>> tmpdir = TemporaryDirectory()
    >>> os.chdir(tmpdir.name)
    >>> pd.DataFrame({'Global Signal': [0.1]}).to_csv('signals.tsv', index=False, na_rep='n/a')
    >>> pd.DataFrame({'stdDVARS': [0.2]}).to_csv('dvars.tsv', index=False, na_rep='n/a')
    >>> out_file, confound_list = _gather_confounds('signals.tsv', 'dvars.tsv')
    >>> confound_list
    ['Global signals', 'DVARS']
    >>> pd.read_csv(out_file, sep='\s+', index_col=None,
    ...             engine='python')  # doctest: +NORMALIZE_WHITESPACE
       global_signal  std_dvars
    0            0.1        0.2
    >>> tmpdir.cleanup()
    """

    def less_breakable(a_string):
        ''' hardens the string to different envs (i.e. case insensitive, no whitespace, '#' '''
        return ''.join(a_string.split()).strip('#')

    # Taken from https://stackoverflow.com/questions/1175208/
    # If we end up using it more than just here, probably worth pulling in a well-tested package
    def camel_to_snake(name):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def _adjust_indices(left_df, right_df):
        # This forces missing values to appear at the beggining of the DataFrame
        # instead of the end
        index_diff = len(left_df.index) - len(right_df.index)
        if index_diff > 0:
            right_df.index = range(index_diff,
                                   len(right_df.index) + index_diff)
        elif index_diff < 0:
            left_df.index = range(-index_diff,
                                  len(left_df.index) - index_diff)

    all_files = []
    confounds_list = []
    for confound, name in ((signals, 'Global signals'),
                           (std_dvars, 'Standardized DVARS'),
                           (dvars, 'DVARS'),
                           (fdisp, 'Framewise displacement'),
                           (tcompcor, 'tCompCor'),
                           (acompcor, 'aCompCor'),
                           (cos_basis, 'Cosine basis'),
                           (motion, 'Motion parameters'),
                           (aroma, 'ICA-AROMA')):
        if confound is not None and isdefined(confound):
            confounds_list.append(name)
            if os.path.exists(confound) and os.stat(confound).st_size > 0:
                all_files.append(confound)

    confounds_data = pd.DataFrame()
    for file_name in all_files:  # assumes they all have headings already
        new = pd.read_csv(file_name, sep="\t")
        for column_name in new.columns:
            new.rename(columns={column_name: camel_to_snake(less_breakable(column_name))},
                       inplace=True)

        _adjust_indices(confounds_data, new)
        confounds_data = pd.concat((confounds_data, new), axis=1)

    if newpath is None:
        newpath = os.getcwd()

    combined_out = os.path.join(newpath, 'confounds.tsv')
    confounds_data.to_csv(combined_out, sep='\t', index=False,
                          na_rep='n/a')

    return combined_out, confounds_list

    
def _maskroi(in_mask, roi_file):
    import numpy as np
    import nibabel as nb
    from nipype.utils.filemanip import fname_presuffix

    roi = nb.load(roi_file)
    roidata = roi.get_data().astype(np.uint8)
    msk = nb.load(in_mask).get_data().astype(bool)
    roidata[~msk] = 0
    roi.set_data_dtype(np.uint8)

    out = fname_presuffix(roi_file, suffix='_boldmsk')
    roi.__class__(roidata, roi.affine, roi.header).to_filename(out)
    return out

class SignalExtractionInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='4-D fMRI nii file')
    label_files = InputMultiPath(
        File(exists=True),
        mandatory=True,
        desc='a 3-D label image, with 0 denoting '
        'background, or a list of 3-D probability '
        'maps (one per label) or the equivalent 4D '
        'file.')
    class_labels = traits.List(
        mandatory=True,
        desc='Human-readable labels for each segment '
        'in the label file, in order. The length of '
        'class_labels must be equal to the number of '
        'segments (background excluded). This list '
        'corresponds to the class labels in label_file '
        'in ascending order')
    out_file = File(
        'signals.tsv',
        usedefault=True,
        exists=False,
        desc='The name of the file to output to. '
        'signals.tsv by default')


class SignalExtractionOutputSpec(TraitedSpec):
    out_file = File(
        exists=True,
        desc='tsv file containing the computed '
        'signals, with as many columns as there are labels and as '
        'many rows as there are timepoints in in_file, plus a '
        'header row with values from class_labels')


class SignalExtraction(SimpleInterface):
    """ Extract mean signals from a time series within a set of ROIs
    This interface is intended to be a memory-efficient alternative to
    nipype.interfaces.nilearn.SignalExtraction.
    Not all features of nilearn.SignalExtraction are implemented at
    this time.
    """
    input_spec = SignalExtractionInputSpec
    output_spec = SignalExtractionOutputSpec

    def _run_interface(self, runtime):
        mask_imgs = [nb.load(fname) for fname in self.inputs.label_files]
        if len(mask_imgs) == 1:
            mask_imgs = nb.four_to_three(mask_imgs[0])

        masks = [mask_img.get_data().astype(np.bool) for mask_img in mask_imgs]

        n_masks = len(masks)

        if n_masks != len(self.inputs.class_labels):
            raise ValueError("Number of masks must match number of labels")

        img = nb.load(self.inputs.in_file)

        series = np.zeros((img.shape[3], n_masks))

        data = img.get_data()
        for j in range(n_masks):
            series[:, j] = data[masks[j], :].mean(axis=0)

        output = np.vstack((self.inputs.class_labels, series.astype(str)))
        self._results['out_file'] = os.path.join(runtime.cwd,
                                                 self.inputs.out_file)
        np.savetxt(
            self._results['out_file'], output, fmt=b'%s', delimiter='\t')

        return runtime
    
class BIDSInfoInputSpec(BaseInterfaceInputSpec):
    in_file = File(mandatory=True, desc='input file, part of a BIDS tree')


class BIDSInfoOutputSpec(TraitedSpec):
    subject_id = traits.Str()
    session_id = traits.Str()
    task_id = traits.Str()
    acq_id = traits.Str()
    rec_id = traits.Str()
    run_id = traits.Str()


class BIDSInfo(SimpleInterface):
    """
    Extract metadata from a BIDS-conforming filename
    This interface uses only the basename, not the path, to determine the
    subject, session, task, run, acquisition or reconstruction.
    >>> from niworkflows.utils.bids import collect_data
    >>> bids_info = BIDSInfo()
    >>> bids_info.inputs.in_file = collect_data(str(datadir / 'ds114'), '01')[0]['bold'][0]
    >>> bids_info.inputs.in_file  # doctest: +ELLIPSIS
    '.../ds114/sub-01/ses-retest/func/sub-01_ses-retest_task-covertverbgeneration_bold.nii.gz'
    >>> res = bids_info.run()
    >>> res.outputs
    <BLANKLINE>
    acq_id = <undefined>
    rec_id = <undefined>
    run_id = <undefined>
    session_id = ses-retest
    subject_id = sub-01
    task_id = task-covertverbgeneration
    <BLANKLINE>
    """
    input_spec = BIDSInfoInputSpec
    output_spec = BIDSInfoOutputSpec

    def _run_interface(self, runtime):
        match = BIDS_NAME.search(self.inputs.in_file)
        params = match.groupdict() if match is not None else {}
        self._results = {key: val for key, val in list(params.items())
                         if val is not None}
        return runtime


class BIDSDataGrabberInputSpec(BaseInterfaceInputSpec):
    subject_data = traits.Dict(Str, traits.Any)
    subject_id = Str()


class BIDSDataGrabberOutputSpec(TraitedSpec):
    out_dict = traits.Dict(desc='output data structure')
    fmap = OutputMultiPath(desc='output fieldmaps')
    bold = OutputMultiPath(desc='output functional images')
    sbref = OutputMultiPath(desc='output sbrefs')
    t1w = OutputMultiPath(desc='output T1w images')
    roi = OutputMultiPath(desc='output ROI images')
    t2w = OutputMultiPath(desc='output T2w images')
    flair = OutputMultiPath(desc='output FLAIR images')


class BIDSDataGrabber(SimpleInterface):
    """
    Collect files from a BIDS directory structure
    >>> from niworkflows.utils.bids import collect_data
    >>> bids_src = BIDSDataGrabber(anat_only=False)
    >>> bids_src.inputs.subject_data = collect_data(str(datadir / 'ds114'), '01')[0]
    >>> bids_src.inputs.subject_id = 'ds114'
    >>> res = bids_src.run()
    >>> res.outputs.t1w  # doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    ['.../ds114/sub-01/ses-retest/anat/sub-01_ses-retest_T1w.nii.gz',
     '.../ds114/sub-01/ses-test/anat/sub-01_ses-test_T1w.nii.gz']
    """
    input_spec = BIDSDataGrabberInputSpec
    output_spec = BIDSDataGrabberOutputSpec
    _require_funcs = True

    def __init__(self, *args, **kwargs):
        anat_only = kwargs.pop('anat_only')
        super(BIDSDataGrabber, self).__init__(*args, **kwargs)
        if anat_only is not None:
            self._require_funcs = not anat_only

    def _run_interface(self, runtime):
        bids_dict = self.inputs.subject_data

        self._results['out_dict'] = bids_dict
        self._results.update(bids_dict)

        if not bids_dict['t1w']:
            raise FileNotFoundError('No T1w images found for subject sub-{}'.format(
                self.inputs.subject_id))

        if self._require_funcs and not bids_dict['bold']:
            raise FileNotFoundError('No functional images found for subject sub-{}'.format(
                self.inputs.subject_id))

        for imtype in ['bold', 't2w', 'flair', 'fmap', 'sbref', 'roi']:
            if not bids_dict[imtype]:
                LOGGER.warning('No "%s" images found for sub-%s',
                               imtype, self.inputs.subject_id)

        return runtime


class DerivativesDataSinkInputSpec(BaseInterfaceInputSpec):
    base_directory = traits.Directory(
        desc='Path to the base directory for storing data.')
    in_file = InputMultiPath(File(exists=True), mandatory=True,
                             desc='the object to be saved')
    source_file = File(exists=False, mandatory=True, desc='the input func file')
    space = Str('', usedefault=True, desc='Label for space field')
    desc = Str('', usedefault=True, desc='Label for description field')
    suffix = Str('', usedefault=True, desc='suffix appended to source_file')
    keep_dtype = traits.Bool(False, usedefault=True, desc='keep datatype suffix')
    extra_values = traits.List(Str)
    compress = traits.Bool(desc="force compression (True) or uncompression (False)"
                                " of the output file (default: same as input)")


class DerivativesDataSinkOutputSpec(TraitedSpec):
    out_file = OutputMultiPath(File(exists=True, desc='written file path'))
    compression = OutputMultiPath(
        traits.Bool, desc='whether ``in_file`` was compressed/uncompressed '
                          'or `it was copied directly.')


class DerivativesDataSink(SimpleInterface):
    """
    Saves the `in_file` into a BIDS-Derivatives folder provided
    by `base_directory`, given the input reference `source_file`.
    >>> import tempfile
    >>> from niworkflows.utils.bids import collect_data
    >>> tmpdir = Path(tempfile.mkdtemp())
    >>> tmpfile = tmpdir / 'a_temp_file.nii.gz'
    >>> tmpfile.open('w').close()  # "touch" the file
    >>> dsink = DerivativesDataSink(base_directory=str(tmpdir))
    >>> dsink.inputs.in_file = str(tmpfile)
    >>> dsink.inputs.source_file = collect_data(str(datadir / 'ds114'), '01')[0]['t1w'][0]
    >>> dsink.inputs.keep_dtype = True
    >>> dsink.inputs.suffix = 'target-mni'
    >>> res = dsink.run()
    >>> res.outputs.out_file  # doctest: +ELLIPSIS
    '.../niworkflows/sub-01/ses-retest/anat/sub-01_ses-retest_target-mni_T1w.nii.gz'
    >>> bids_dir = tmpdir / 'bidsroot' / 'sub-02' / 'ses-noanat' / 'func'
    >>> bids_dir.mkdir(parents=True, exist_ok=True)
    >>> tricky_source = bids_dir / 'sub-02_ses-noanat_task-rest_run-01_bold.nii.gz'
    >>> tricky_source.open('w').close()
    >>> dsink = DerivativesDataSink(base_directory=str(tmpdir))
    >>> dsink.inputs.in_file = str(tmpfile)
    >>> dsink.inputs.source_file = str(tricky_source)
    >>> dsink.inputs.keep_dtype = True
    >>> dsink.inputs.desc = 'preproc'
    >>> res = dsink.run()
    >>> res.outputs.out_file  # doctest: +ELLIPSIS
    '.../niworkflows/sub-02/ses-noanat/func/sub-02_ses-noanat_task-rest_run-01_\
desc-preproc_bold.nii.gz'
    """
    input_spec = DerivativesDataSinkInputSpec
    output_spec = DerivativesDataSinkOutputSpec
    out_path_base = "niworkflows"
    _always_run = True

    def __init__(self, out_path_base=None, **inputs):
        super(DerivativesDataSink, self).__init__(**inputs)
        self._results['out_file'] = []
        if out_path_base:
            self.out_path_base = out_path_base

    def _run_interface(self, runtime):
        src_fname, _ = _splitext(self.inputs.source_file)
        src_fname, dtype = src_fname.rsplit('_', 1)
        _, ext = _splitext(self.inputs.in_file[0])
        if self.inputs.compress is True and not ext.endswith('.gz'):
            ext += '.gz'
        elif self.inputs.compress is False and ext.endswith('.gz'):
            ext = ext[:-3]

        m = BIDS_NAME.search(src_fname)

        mod = Path(self.inputs.source_file).parent.name

        base_directory = runtime.cwd
        if isdefined(self.inputs.base_directory):
            base_directory = self.inputs.base_directory

        base_directory = Path(base_directory).resolve()
        out_path = base_directory / self.out_path_base /             '{subject_id}'.format(**m.groupdict())

        if m.groupdict().get('session_id') is not None:
            out_path = out_path / '{session_id}'.format(**m.groupdict())

        out_path = out_path / '{}'.format(mod)
        out_path.mkdir(exist_ok=True, parents=True)
        base_fname = str(out_path / src_fname)

        formatstr = '{bname}{space}{desc}{extra}{suffix}{dtype}{ext}'
        if len(self.inputs.in_file) > 1 and not isdefined(self.inputs.extra_values):
            formatstr = '{bname}{space}{desc}{suffix}{i:04d}{dtype}{ext}'

        space = '_space-{}'.format(self.inputs.space) if self.inputs.space else ''
        desc = '_desc-{}'.format(self.inputs.desc) if self.inputs.desc else ''
        suffix = '_{}'.format(self.inputs.suffix) if self.inputs.suffix else ''
        dtype = '' if not self.inputs.keep_dtype else ('_%s' % dtype)

        self._results['compression'] = []
        for i, fname in enumerate(self.inputs.in_file):
            extra = ''
            if isdefined(self.inputs.extra_values):
                extra = '_{}'.format(self.inputs.extra_values[i])
            out_file = formatstr.format(
                bname=base_fname,
                space=space,
                desc=desc,
                extra=extra,
                suffix=suffix,
                i=i,
                dtype=dtype,
                ext=ext,
            )
            self._results['out_file'].append(out_file)
            self._results['compression'].append(_copy_any(fname, out_file))
        return runtime


class ReadSidecarJSONInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='the input nifti file')
    fields = traits.List(traits.Str, desc='get only certain fields')


class ReadSidecarJSONOutputSpec(TraitedSpec):
    subject_id = traits.Str()
    session_id = traits.Str()
    task_id = traits.Str()
    acq_id = traits.Str()
    rec_id = traits.Str()
    run_id = traits.Str()
    out_dict = traits.Dict()


class ReadSidecarJSON(SimpleInterface):
    """
    A utility to find and read JSON sidecar files of a BIDS tree
    """
    expr = BIDS_NAME
    input_spec = ReadSidecarJSONInputSpec
    output_spec = ReadSidecarJSONOutputSpec
    _always_run = True

    def _run_interface(self, runtime):
        metadata = get_metadata_for_nifti(self.inputs.in_file)
        output_keys = [key for key in list(self.output_spec().get().keys()) if key.endswith('_id')]
        outputs = self.expr.search(op.basename(self.inputs.in_file)).groupdict()

        for key in output_keys:
            id_value = outputs.get(key)
            if id_value is not None:
                self._results[key] = outputs.get(key)

        if isdefined(self.inputs.fields) and self.inputs.fields:
            for fname in self.inputs.fields:
                self._results[fname] = metadata[fname]
        else:
            self._results['out_dict'] = metadata

        return runtime


class BIDSFreeSurferDirInputSpec(BaseInterfaceInputSpec):
    derivatives = Directory(exists=True, mandatory=True,
                            desc='BIDS derivatives directory')
    freesurfer_home = Directory(exists=True, mandatory=True,
                                desc='FreeSurfer installation directory')
    subjects_dir = traits.Str('freesurfer', usedefault=True,
                              desc='Name of FreeSurfer subjects directory')
    spaces = traits.List(traits.Str, desc='Set of output spaces to prepare')
    overwrite_fsaverage = traits.Bool(False, usedefault=True,
                                      desc='Overwrite fsaverage directories, if present')


class BIDSFreeSurferDirOutputSpec(TraitedSpec):
    subjects_dir = traits.Directory(exists=True,
                                    desc='FreeSurfer subjects directory')


class BIDSFreeSurferDir(SimpleInterface):
    """Create a FreeSurfer subjects directory in a BIDS derivatives directory
    and copy fsaverage from the local FreeSurfer distribution.
    Output subjects_dir = ``{derivatives}/{subjects_dir}``, and may be passed to
    ReconAll and other FreeSurfer interfaces.
    """
    input_spec = BIDSFreeSurferDirInputSpec
    output_spec = BIDSFreeSurferDirOutputSpec
    _always_run = True

    def _run_interface(self, runtime):
        subjects_dir = os.path.join(self.inputs.derivatives,
                                    self.inputs.subjects_dir)
        os.makedirs(subjects_dir, exist_ok=True)
        self._results['subjects_dir'] = subjects_dir

        spaces = list(self.inputs.spaces)
        # Always copy fsaverage, for proper recon-all functionality
        if 'fsaverage' not in spaces:
            spaces.append('fsaverage')

        for space in spaces:
            # Skip non-freesurfer spaces and fsnative
            if not space.startswith('fsaverage'):
                continue
            source = os.path.join(self.inputs.freesurfer_home, 'subjects', space)
            dest = os.path.join(subjects_dir, space)
            # Finesse is overrated. Either leave it alone or completely clobber it.
            if os.path.exists(dest) and self.inputs.overwrite_fsaverage:
                rmtree(dest)
            if not os.path.exists(dest):
                try:
                    copytree(source, dest)
                except FileExistsError:
                    LOGGER.warning("%s exists; if multiple jobs are running in parallel"
                                   ", this can be safely ignored", dest)

        return runtime
    
def _first(inlist):
    return inlist[0]

def _aslist(val):
    return [val]


# In[4]:


def reho_deneme(name,bold_file,parcellation):
    
    ctwf = pe.Workflow(name='hobare')
    
    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file','parcellation']),
                       name='inputnode')
    inputnode.inputs.bold_file=bold_file
    inputnode.inputs.parcellation=parcellation
    
    reho_node = pe.Node(afni.ReHo(),
                       name='reho')
    
    dt_sink = pe.Node(nio.DataSink(),name='data_sink')
    dt_sink.inputs.base_directory='/local/data/tsc/reho_deneme'
    
    ctwf.connect([
        (inputnode, reho_node,[('bold_file','in_file'),
                                        ('parcellation','label_set')]),
        (reho_node,dt_sink,[('out_file','reho.@ali'),
                           ('out_vals','reho.@veli')])
    ])
    
    return ctwf

hebe=reho_deneme(name='reho_deneme',
                bold_file='/local/data/tsc/bids/derivatives/fmriprep2/sub-c023/ses-03/bold_t1/sub-c023_ses-03_task-rest_bold_t1_res_bandpassed.nii.gz',
                parcellation='/local/data/tsc/bids/derivatives/sub-c023/ses-03/anat/sub-c023_ses-03_space-T1_parc.nii.gz')
hebe.run()


# In[3]:


# variable definitions
omp_nthreads=8
func = '/local/data/tsc/bids/sub-a003/ses-03/func/sub-a003_ses-03_task-rest_bold.nii.gz'

# repetition time, low-pass and high-pass cut-off frequencies in Hz
TR = 3
lpThr = 0.1
hpThr = 0.01
subject_data, layout = collect_data('/local/data/tsc/bids/', 'a006',None,None)
#ref_file='/local/data/tsc/bids/sub-a002/ses-01/func/sub-a002_ses-01_task-rest_bold.nii.gz'
metadata = layout.get_metadata(subject_data['bold'][0])
slice_encoding_direction=metadata.get('SliceEncodingDirection','k')
subject_data['bold'][0]
metadata
run_stc = ("SliceTiming" in metadata)
run_stc


# In[4]:


# brain extraction from fMRI data
def brain_extract(name='skull_strip_bold',pre_mask=False,omp_threads=1):
    
    bet = pe.Workflow(name=name)

    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file','pre_mask']),
                        name='inputnode')


    outputnode = pe.Node(util.IdentityInterface(fields=['bold_mask', 'bold_brain',
                                                        'bold_bias_corrected']),
                         name='outputnode')
    pre_dilate = pe.Node(fsl.DilateImage(
        operation='max', kernel_shape='sphere', kernel_size=3.0,
        internal_datatype='char'), name='pre_mask_dilate')
    check_hdr = pe.Node(MatchHeader(), name='check_hdr',
                        run_without_submitting=True)

    n4_mask = pe.Node(MaskEPI(upper_cutoff=0.75, opening=1,
                              no_sanitize=True),
                      name = 'n4_mask')
    n4_correct = pe.Node(ants.N4BiasFieldCorrection(dimension=3, copy_header=True),
                        name='n4_correct', n_procs=1)

    #Create a generous BET mask out of the bias-corrected EPI
    skullstrip_first_pass = pe.Node(fsl.BET(frac=0.2, mask=True),
                                    name='skullstrip_first_pass')
    bet_dilate = pe.Node(fsl.DilateImage(operation='max', kernel_shape='sphere',
                                         kernel_size=6.0, internal_datatype='char'),
                         name='skullstrip_first_dilate')
    bet_mask = pe.Node(fsl.ApplyMask(), name='skullstrip_first_mask')
    # Use AFNI's unifize for T2 constrast & fix header
    unifize = pe.Node(afni.Unifize(
            t2=True, outputtype='NIFTI_GZ',
            # Default -clfrac is 0.1, 0.4 was too conservative
            # -rbt because I'm a Jedi AFNI Master (see 3dUnifize's documentation)
            args='-clfrac 0.2 -rbt 18.3 65.0 90.0',
            out_file="uni.nii.gz"), name='unifize')
    fixhdr_unifize = pe.Node(CopyXForm(), name='fixhdr_unifize', mem_gb=0.1)

    # Run ANFI's 3dAutomask to extract a refined brain mask
    skullstrip_second_pass = pe.Node(afni.Automask(dilate=1,
                                                       outputtype='NIFTI_GZ'),
                                         name='skullstrip_second_pass')
    fixhdr_skullstrip2 = pe.Node(CopyXForm(), name='fixhdr_skullstrip2', mem_gb=0.1)

    # Take intersection of both masks
    combine_masks = pe.Node(fsl.BinaryMaths(operation='mul'),
                                name='combine_masks')

    # Compute masked brain
    apply_mask = pe.Node(fsl.ApplyMask(), name='apply_mask')
    
    if not pre_mask:
        bet.connect([
            (inputnode, n4_mask, [('bold_file', 'in_files')]),
            (n4_mask, pre_dilate, [('out_mask', 'in_file')]),
        ])
    else:
        bet.connect([
            (inputnode, pre_dilate, [('pre_mask', 'in_file')]),
        ])

    bet.connect([
            #(inputnode, n4_mask, [('in_file', 'in_files')]),
            (pre_dilate, check_hdr, [('out_file','in_file')]),
            (inputnode, check_hdr, [('bold_file','reference')]),
            (check_hdr, n4_correct, [('out_file','mask_image')]),
            #(pre_dilate, n4_correct, [('out_file', 'mask_image')]),
            (inputnode, n4_correct, [('bold_file','input_image')]),
            (inputnode, fixhdr_unifize, [('bold_file', 'hdr_file')]),
            (inputnode, fixhdr_skullstrip2, [('bold_file', 'hdr_file')]),
            #(n4_mask, n4_correct, [('out_mask', 'mask_image')]),
            (n4_correct, skullstrip_first_pass, [('output_image', 'in_file')]),
            #(pre_dilate,skullstrip_first_pass, [('out_file','in_file')]),
            (skullstrip_first_pass, bet_dilate, [('mask_file', 'in_file')]),
            (bet_dilate, bet_mask, [('out_file', 'mask_file')]),
            (skullstrip_first_pass, bet_mask, [('out_file', 'in_file')]),
            (bet_mask, unifize, [('out_file', 'in_file')]),
            (unifize, fixhdr_unifize, [('out_file', 'in_file')]),
            (fixhdr_unifize, skullstrip_second_pass, [('out_file', 'in_file')]),
            (skullstrip_first_pass, combine_masks, [('mask_file', 'in_file')]),
            (skullstrip_second_pass, fixhdr_skullstrip2, [('out_file', 'in_file')]),
            (fixhdr_skullstrip2, combine_masks, [('out_file', 'operand_file')]),
            (fixhdr_unifize, apply_mask, [('out_file', 'in_file')]),
            (combine_masks, apply_mask, [('out_file', 'mask_file')]),
            (combine_masks, outputnode, [('out_file', 'bold_mask')]),
            (apply_mask, outputnode, [('out_file', 'bold_brain')]),
            #(pre_dilate, outputnode, [('out_file','bold_bias_corrected')]),
            (n4_correct, outputnode, [('output_image', 'bold_bias_corrected')]),
    ])
    return bet
aa = brain_extract(name='bold_skull_strip',omp_threads=2,pre_mask=True)
aa.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')
#aa.run()


# In[5]:


# estimate reference bold image
def bold_reference(omp_nthreads, bold_file=None, pre_mask=False, 
                   name='gen_bold_ref'):
    ref= pe.Workflow(name=name)

    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file',
                                                       'bold_mask']),
                       name='inputnode')
    
    outputnode= pe.Node(util.IdentityInterface(fields=['bold_file',
                                                       'bold_ref',
                                                       'bold_mask',
                                                       'bold_brain',
                                                       'raw_ref_image',
                                                      'skip_vols']),
                       name='outputnode')
    validate = pe.Node(ValidateImage(),name='validate')

    gen_ref = pe.Node(EstimateReferenceImage(),name='gen_ref')

    validate_ref= pe.Node(ValidateImage(), name='validate_ref')

    bet = brain_extract(name='skull_strip_bold',omp_threads=2,pre_mask=pre_mask)
    
    if pre_mask:
        ref.connect([
            (inputnode, bet, [('bold_mask','inputnode.pre_mask')])
        ])
    ref.connect([
        (inputnode, validate, [('bold_file','in_file')]),
        (validate, gen_ref, [('out_file','in_file')]),
        (gen_ref, validate_ref, [('ref_image','in_file')]),
        (validate_ref,bet,[('out_file','inputnode.bold_file')]),
        (validate, outputnode, [('out_file','bold_file')]),
        (gen_ref, outputnode, [('n_volumes_to_discard','skip_vols')]),
        (validate_ref, outputnode, [('out_file','raw_ref_image')]),
        (bet, outputnode,[
            ('outputnode.bold_bias_corrected','bold_ref'),
            ('outputnode.bold_mask','bold_mask'),
            ('outputnode.bold_brain','bold_brain')]),
    ])
    return ref
aa=bold_reference(omp_nthreads=2, name='create_bold_ref')
aa.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')
#aa.run()


# In[6]:


# slice timing correction 
def slice_time_correct(metadata,name='slice_time_correction'):
    stc=pe.Workflow(name=name)

    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file',
                                                      'skip_vols']),
                       name='inputnode')

    outputnode= pe.Node(util.IdentityInterface(fields=['stc_file']),
                       name='outputnode')

    slice_timing_correction=pe.Node(
        afni.TShift(outputtype='NIFTI_GZ',
                    tr='{}s'.format(metadata["RepetitionTime"]),
                    slice_timing=metadata['SliceTiming']),
        name='slice_timing_correction')

    copy_xform=pe.Node(CopyXForm(), name='copy_xform')
    stc.connect([
        (inputnode, slice_timing_correction, [('bold_file','in_file'),
                                             ('skip_vols','ignore')]),
        (slice_timing_correction, copy_xform, [('out_file','in_file')]),
        (inputnode, copy_xform, [('bold_file','hdr_file')]),
        (copy_xform, outputnode, [('out_file','stc_file')]),
    ])
    return stc
stc=slice_time_correct(metadata=metadata,name='oya_okula_kos')
stc.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[7]:


# head motion correction (HMC) and motion parameter estimation
def head_motion(omp_nthreads,mem_gb=1, name='head_motion_correction', fd_threshold=0.5):
    hmc=pe.Workflow(name=name)

    inputnode=pe.Node(util.IdentityInterface(fields=['bold_file','bold_ref',
                                                     'threshold']),
                  name='inputnode')
    inputnode.inputs.threshold=fd_threshold

    outputnode = pe.Node(util.IdentityInterface(fields=['bold_file', 
                                                        'raw_ref_image',
                                                            'skip_vols', 
                                                        'ref_image',
                                                            'ref_image_brain', 
                                                        'bold_mask',
                                                        'bold_brain',
                                                        'bold_ref',
                                                        'remove_list', 
                                                        'xforms',
                                                        'movpar_file',
                                                            'fd_file',
                                                        'bold_mc',
                                                        'friston_24'
                                                           ]),
                             name='outputnode')


    fd_est = pe.Node(nac.FramewiseDisplacement(parameter_source='FSL',
                                               out_file='fd.txt',
                                               out_figure='fd.pdf'),
                    name= 'fd_est')

    mcflirt = pe.Node(fsl.MCFLIRT(save_mats=True,
                               save_plots=True),
                   name='mcflirt')

    # dmc = pe.Node(dynamicMissingdataCompletion(),
    #             name='dmc')

    remove_list = pe.Node(removeVolumeList(),
                          name='remove_vol_list')
    
    boldref= bold_reference(omp_nthreads=omp_nthreads)
    
    applymc = pe.Node(MultiApplyTransforms(interpolation='LanczosWindowedSinc',
                                          float=True, copy_dtype=True),
                     name='apply_motion_corr',mem_gb=mem_gb*3*omp_nthreads,
                     n_procs=omp_nthreads)
    merge=pe.Node(Merge(compress=True),
                 name='merge', mem_gb = mem_gb * 3)
    
    bold_split = pe.Node(fsl.Split(dimension='t'),
                        name='bold_split', mem_gb=mem_gb*3)

    fsl2itk = pe.Node(MCFLIRT2ITK(),
                     name='fsl2itk',
                     n_procs=omp_nthreads)
    
    friston24 = pe.Node(calcMotionParamDiff(out_name="friston24"),
                       name='friston_24')
    

    #normalize_motion = pe.Node(NormalizeMotionParams(format='FSL'),
    #                           name="normalize_motion",
    #                           mem_gb=DEFAULT_MEMORY_MIN_GB)


    hmc.connect([
        #(inputnode, gen_ref, [('bold_file','in_file')]),
        (inputnode, remove_list, [('threshold','thr')]),
        (inputnode, mcflirt, [('bold_file','in_file'),
                             ('bold_ref','ref_file')]),
        #(gen_ref, mcflirt, [('ref_image','ref_file')]),
        #(gen_ref, fsl2itk, [('ref_image','in_source'),
        #                     ('ref_image','in_reference')]),
        (inputnode, fsl2itk, [('bold_ref','in_source'),
                             ('bold_ref','in_reference')]),
        (mcflirt, fsl2itk, [('mat_file', 'in_files')]),
        (mcflirt, fd_est, [('par_file','in_file')]),
        (mcflirt, friston24, [('par_file','in_file')]),
        (friston24, outputnode, [('out_file','friston_24')]),
        #(mcflirt, dmc, [('out_file', 'in_file')]),
        (fd_est, remove_list, [('out_file','in_file')]),
        #(remove_list, dmc, [('out_file','remove_volumes')]),
        (mcflirt, outputnode, [('par_file','movpar_file')]),
        (fd_est, outputnode, [('out_file','fd_file')]),
        (fsl2itk, outputnode, [('out_file','xforms')]),
        (remove_list, outputnode, [('out_file','remove_list')]),
        #(hmc_inputnode, hmc_datasink, [('bold_file','original_bold_data')]),
        #(gen_ref, hmc_datasink, [('ref_image','bold_reference_image')]),
        #(mcflirt, hmc_datasink, [('out_file','bold_motion_corrected')]),
        #(fd_est, hmc_datasink, [('out_file','motion_parameters.@fd')]),
        #(remove_list, hmc_datasink, [('out_file','motion_parameters.@rmlist')]),
        #(dmc, hmc_datasink, [('out_file','dmc')]),
        #(hmc_outputnode, hmc_datasink, [('xforms','transforms.@xform')]),
        #(hmc_outputnode, hmc_datasink, [('movpar_file','motion_parameters.@mov_par')]),
        (inputnode,merge,[('bold_file','header_source')]),
        (applymc, merge, [('out_files','in_files')]),
        (merge, boldref, [('out_file','inputnode.bold_file')]),
        (boldref,outputnode, [('outputnode.bold_mask','bold_mask'),
                             ('outputnode.bold_ref','bold_ref'),
                             ('outputnode.bold_brain','bold_brain'),
                             ('outputnode.raw_ref_image','raw_ref_image')]),
        (merge, outputnode, [('out_file','bold_mc')]),
        (inputnode,bold_split, [('bold_file','in_file')]),
        (bold_split, applymc, [('out_files','input_image'),
                              (('out_files',_first),'reference_image')]),
        (fsl2itk, applymc, [(('out_file',_aslist),'transforms')]),
    ])
    return hmc
hh = head_motion(name='head',omp_nthreads=3)
hh.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[8]:


# bold to t1 registration
def bold2t1_reg(name='bold_to_t1w_registration',
                mem_gb=1,omp_nthreads=1,use_compression=False):
    DEFAULT_MEMORY_MIN_GB = 0.01
    bold2t1 = pe.Workflow(name=name)

    inputnode = pe.Node(util.IdentityInterface(fields=['bold_brain',
                                                       't1_brain',
                                                       'wm_mask',]),
                        name='inputnode')

    outputnode = pe.Node(util.IdentityInterface(fields=['itk_bold_to_t1',
                                                        'itk_t1_to_bold']),
                         name='outputnode')

    affine = pe.Node(fsl.FLIRT(dof=6),
                    name= 'affine')

    invt_bbr = pe.Node(fsl.ConvertXFM(invert_xfm=True), name='invt_bbr',
                           mem_gb=DEFAULT_MEMORY_MIN_GB)
    fsl2itk_fwd = pe.Node(c3.C3dAffineTool(fsl2ras=True, itk_transform=True),
                              name='fsl2itk_fwd', mem_gb=DEFAULT_MEMORY_MIN_GB)
    fsl2itk_inv = pe.Node(c3.C3dAffineTool(fsl2ras=True, itk_transform=True),
                              name='fsl2itk_inv', mem_gb=DEFAULT_MEMORY_MIN_GB)

    bbreg = pe.Node(fsl.FLIRT(cost_func='bbr',dof=9),
                   name='bbr')
    bbreg.inputs.schedule = '/local/fsl/etc/flirtsch/bbr.sch'



    bold2t1.connect([
        (inputnode, affine, [('t1_brain','reference'),
                           ('bold_brain','in_file')]),
        (inputnode, fsl2itk_fwd, [('t1_brain','reference_file'),
                                  ('bold_brain','source_file')]),
        (inputnode, fsl2itk_inv, [('bold_brain','reference_file'),
                                 ('t1_brain','source_file')]),
        (invt_bbr, fsl2itk_inv, [('out_file', 'transform_file')]),
        (fsl2itk_fwd, outputnode, [('itk_transform', 'itk_bold_to_t1')]),
        (fsl2itk_inv, outputnode, [('itk_transform', 'itk_t1_to_bold')]),
        (inputnode, bbreg, [('wm_mask','wm_seg'),
                           ('bold_brain','in_file'),
                           ('t1_brain','reference')]),
        (affine, bbreg, [('out_matrix_file', 'in_matrix_file')]),
        (bbreg, invt_bbr, [('out_matrix_file', 'in_file')]),
        (bbreg, fsl2itk_fwd, [('out_matrix_file', 'transform_file')]),
    ])
    return bold2t1
aa=bold2t1_reg(name='ali')
aa.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[9]:


DEFAULT_MEMORY_MIN_GB=1
# apply bold to t1 transform (combine with motion correction)
def apply_bold2t1(name='Apply_bold2t1_reg',
                 mem_gb=1,omp_nthreads=1,use_compression=False):
    app_bold2t1 = pe.Workflow(name=name)
    inputnode = pe.Node(util.IdentityInterface(
                        fields=['name_source', 'ref_bold_brain','bold_split',
                                'ref_bold_mask','t1_brain', 't1_mask',
                                't1_aseg','itk_bold_to_t1' 't1_aparc','bold_split', 'hmc_xforms','itk_bold_to_t1']),
                        name='inputnode')
    outputnode = pe.Node(util.IdentityInterface(
                        fields=['bold_t1', 'bold_t1_ref', 'bold_t1_mask']),
                         name='outputnode')

    #gen_ref = pe.Node(GenerateSamplingReference(), name='gen_ref',
    #                      mem_gb=0.3)  # 256x256x256 * 64 / 8 ~ 150MB

    mask_t1w_tfm = pe.Node(ApplyTransforms(interpolation='MultiLabel',
                                           float=True),
                           name='mask_t1w_tfm', mem_gb=0.1)

    app_bold2t1.connect([
        #(inputnode, gen_ref, [('ref_bold_brain', 'moving_image'),
        #                      ('t1_brain', 'fixed_image'),
        #                      ('t1_mask', 'fov_mask')]),
        (inputnode, mask_t1w_tfm, [('ref_bold_mask', 'input_image')]),
        #(gen_ref, mask_t1w_tfm, [('out_file', 'reference_image')]),
        (inputnode, mask_t1w_tfm, [('t1_brain','reference_image')]),
        (inputnode, mask_t1w_tfm, [('itk_bold_to_t1', 'transforms')]),
        (mask_t1w_tfm, outputnode, [('output_image', 'bold_t1_mask')]),
    ])

    bold_to_t1w_transform = pe.Node(MultiApplyTransforms(interpolation="LanczosWindowedSinc", float=True, copy_dtype=True),
            name='bold2t1_tfm', mem_gb=mem_gb * 3 * omp_nthreads, n_procs=omp_nthreads)

    # merge 3D volumes into 4D timeseries
    merge = pe.Node(Merge(compress=use_compression), name='merge', mem_gb=mem_gb)

    # Generate a reference on the target T1w space
    gen_final_ref = bold_reference(omp_nthreads, pre_mask=True)

    nforms  = 2
    merge_xforms = pe.Node(util.Merge(nforms),name='merge_xforms',
                          run_without_submitting=True,mem_gb=DEFAULT_MEMORY_MIN_GB)

    app_bold2t1.connect([
        (inputnode, merge_xforms, [('hmc_xforms','in2'),
                                  ('itk_bold_to_t1', 'in1')]),
        (merge_xforms, bold_to_t1w_transform, [('out','transforms')]),
        (inputnode, bold_to_t1w_transform, [('bold_split','input_image')]),
        (inputnode, merge, [('name_source', 'header_source')]),
        #(gen_ref, bold_to_t1w_transform, [('out_file', 'reference_image')]), 
        (bold_to_t1w_transform, merge, [('out_files', 'in_files')]),
        (inputnode, bold_to_t1w_transform, [('t1_brain','reference_image')]),
        (merge, gen_final_ref, [('out_file', 'inputnode.bold_file')]),
        (mask_t1w_tfm, gen_final_ref, [('output_image', 'inputnode.bold_mask')]),
        (merge, outputnode, [('out_file', 'bold_t1')]),
        (gen_final_ref, outputnode, [('outputnode.bold_ref', 'bold_t1_ref')]),
    ])
        
    return app_bold2t1
app=apply_bold2t1(name='veli',mem_gb =1,
                 omp_nthreads=8,use_compression=True)
app.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[10]:


# confound estimation
def confound_estimate(mem_gb,metadata,
                      name='Confound_estimation'):
    conf = pe.Workflow(name=name)
    inputnode = pe.Node(util.IdentityInterface(
                        fields=['bold', 'bold_mask','bold_t1_mask','bold_t1',
                                'movpar_file','skip_vols',
                                't1_bmask','t1_csfmask','friston_24',
                               't1_wmmask']),
                        name='inputnode')
    outputnode = pe.Node(util.IdentityInterface(
                        fields=['confounds_file',
                                'red_confounds_file',
                               'bold_t1_cut']),
                         name='outputnode')
    #csf_roi = pe.Node(TPM2ROI(erode_mm=0, mask_erode_mm=30), name='csf_roi')
    #wm_roi = pe.Node(TPM2ROI(
    #    erode_prop=0.6, mask_erode_prop=0.6**3),  # 0.6 = radius; 0.6^3 = volume
    #    name='wm_roi')
    
    # Ensure ROIs don't go off-limits (reduced FoV)
    csf_msk = pe.Node(util.Function(function=_maskroi), name='csf_msk')
    wm_msk = pe.Node(util.Function(function=_maskroi), name='wm_msk')
    #brain_msk = pe.Node(util.Function(function=_maskroi), name='brain_msk')

    #acc_msk = pe.Node(util.Function(function=_maskroi), name='acc_msk')
    #tcc_msk = pe.Node(util.Function(function=_maskroi), name='tcc_msk')
    
    # DVARS
    dvars = pe.Node(nac.ComputeDVARS(save_nstd=True, save_std=True, remove_zerovariance=True),
                    name="dvars", mem_gb=mem_gb)

    # Frame displacement
    fdisp = pe.Node(nac.FramewiseDisplacement(parameter_source="SPM"),
                    name="fdisp", mem_gb=mem_gb)
    
    def _pick_csf(files):
        return files[0]

    def _pick_wm(files):
        return files[-1]
    
    mrg_lbl = pe.Node(util.Merge(3), name='merge_rois', run_without_submitting=True)
    signals = pe.Node(SignalExtraction(class_labels=["csf", "white_matter", "global_signal"]),
                      name="signals", mem_gb=mem_gb)
    
    # Arrange confounds
    add_dvars_header = pe.Node(
        AddTSVHeader(columns=["dvars"]),
        name="add_dvars_header", mem_gb=0.01, run_without_submitting=True)
    add_std_dvars_header = pe.Node(
        AddTSVHeader(columns=["std_dvars"]),
        name="add_std_dvars_header", mem_gb=0.01, run_without_submitting=True)
    add_motion_headers = pe.Node(
        AddTSVHeader(columns=["trans_x", "trans_y", "trans_z", "rot_x", "rot_y", "rot_z"]),
        name="add_motion_headers", mem_gb=0.01, run_without_submitting=True)
    add_fris24_headers = pe.Node(
        AddTSVHeader(columns=["trans_x", "trans_y", "trans_z", "rot_x", "rot_y", "rot_z",
                              "sqr_trans_x", "sqr_trans_y", "sqr_trans_z", "sqr_rot_x", "sqr_rot_y", "sqr_rot_z",
                             "diff_trans_x", "diff_trans_y", "diff_trans_z", "diff_rot_x", "diff_rot_y", "diff_rot_z",
                             "sqrdiff_trans_x", "sqrdiff_trans_y", "sqrdiff_trans_z",
                            "sqrdiff_rot_x", "sqrdiff_rot_y", "sqrdiff_rot_z"]),
        name="add_fris24_headers", mem_gb=0.01, run_without_submitting=True)
    concat = pe.Node(GatherConfounds(), name="concat", mem_gb=0.01, run_without_submitting=True)

    updatecf = pe.Node(updateConfoundsFile(), name='update_confouds')
    
    rm_non_steady_state = pe.Node(util.Function(function=_remove_volumes,
                                                output_names=['bold_cut']),
                                  name='rm_nonsteady')
    
    conf.connect([
        # Mask ROIs with bold_mask
        (inputnode, csf_msk, [('t1_bmask', 'in_mask'),
                             ('t1_csfmask','roi_file')]),
        (inputnode, wm_msk, [('t1_bmask', 'in_mask'),
                            ('t1_wmmask','roi_file')]),
        
        #(inputnode, brain_msk, [('bold_t1_mask','in_mask')]),
        #(inputnode, acc_msk, [('bold_mask', 'in_mask')]),
        #(inputnode, tcc_msk, [('bold_mask', 'in_mask')]),
        # connect inputnode to each non-anatomical confound node
        #(inputnode, dvars, [('bold','in_file'),
        #                    ('bold_mask', 'in_mask')]),
        #(inputnode, fdisp, [('movpar_file', 'in_file')]),
        
        # non_steady volumes cut
        (inputnode, rm_non_steady_state, [('skip_vols','skip_vols'),
                                         ('bold_t1','bold_file')]),
        (rm_non_steady_state, outputnode, [('bold_cut','bold_t1_cut')]),

        # tCompCor
        #(inputnode, tcompcor, [('bold', 'realigned_file')]),
        #(inputnode, tcompcor, [('skip_vols', 'ignore_initial_volumes')]),
        #(tcc_tfm, tcc_msk, [('output_image', 'roi_file')]),
        #(tcc_msk, tcompcor, [('out', 'mask_files')]),

        # aCompCor
        #(inputnode, acompcor, [('bold', 'realigned_file')]),
        #(inputnode, acompcor, [('skip_vols', 'ignore_initial_volumes')]),
        #(acc_tfm, acc_msk, [('output_image', 'roi_file')]),
        #(acc_msk, acompcor, [('out', 'mask_files')]),

        # Global signals extraction (constrained by anatomy)
        (inputnode, signals, [('bold_t1', 'in_file')]),
        #(csf_tfm, csf_msk, [('output_image', 'roi_file')]),
        (csf_msk, mrg_lbl, [('out', 'in1')]),
        #(wm_tfm, wm_msk, [('output_image', 'roi_file')]),
        (wm_msk, mrg_lbl, [('out', 'in2')]),
        (inputnode, mrg_lbl, [('t1_bmask', 'in3')]),
        (mrg_lbl, signals, [('out', 'label_files')]),

        # Collate computed confounds together
        #(inputnode, add_motion_headers, [('movpar_file', 'in_file')]),
        (inputnode, add_fris24_headers, [('friston_24','in_file')]),
        #(dvars, add_dvars_header, [('out_nstd', 'in_file')]),
        #(dvars, add_std_dvars_header, [('out_std', 'in_file')]),
        (signals, concat, [('out_file', 'signals')]),
        #(fdisp, concat, [('out_file', 'fd')]),
        #(tcompcor, concat, [('components_file', 'tcompcor'),
        #                    ('pre_filter_file', 'cos_basis')]),
        #(acompcor, concat, [('components_file', 'acompcor')]),
        #(add_motion_headers, concat, [('out_file', 'motion')]),
        (add_fris24_headers, concat, [('out_file', 'motion')]),
        #(add_dvars_header, concat, [('out_file', 'dvars')]),
        #(add_std_dvars_header, concat, [('out_file', 'std_dvars')]),

        # Set outputs
        (concat, outputnode, [('confounds_file', 'confounds_file')]),
        (concat, updatecf, [('confounds_file','in_file')]),
        (inputnode, updatecf, [('skip_vols','skip_vols')]),
        (updatecf, outputnode, [('out_file','red_confounds_file')]),
        #(inputnode, rois_plot, [('bold', 'in_file'),
        #                        ('bold_mask', 'in_mask')]),
        #(tcompcor, mrg_compcor, [('high_variance_masks', 'in1')]),
        #(acc_msk, mrg_compcor, [('out', 'in2')]),
        #(mrg_compcor, rois_plot, [('out', 'in_rois')]),
        #(rois_plot, ds_report_bold_rois, [('out_report', 'in_file')]),
    ])
    return conf
aaa=confound_estimate(mem_gb =1,metadata=metadata,name='ali_veli_conf')
aaa.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[11]:


# bandpass filtering 
def bandpass(name,TR,lpThr=0.1,hpThr=0.01):
    bpass=pe.Workflow(name=name)

    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file']),
                       name='inputnode')

    outputnode= pe.Node(util.IdentityInterface(fields=['bp_file']),
                       name='outputnode')

    bandpass = pe.Node(bandpassFMRI(lpThr=lpThr,hpThr=hpThr,TR=TR),
                 name = 'bandpass')


    bpass.connect([
        (inputnode, bandpass, [('bold_file','in_file')]),
        (bandpass, outputnode, [('out_file','bp_file')]),
    ])
    return bpass
bpp=bandpass(name='ali_ata_bak',TR=3)
bpp.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[12]:


# bandpass filtering 
def meantimeseries(name,t1seg=None):
    mtswf=pe.Workflow(name=name)
    args='--label=' + t1seg
    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file']),
                       name='inputnode')

    outputnode= pe.Node(util.IdentityInterface(fields=['mts_file']),
                       name='outputnode')

    mtsnode = pe.Node(fsl.utils.ImageMeants(args=args),
                 name = 'mean_time_series')

    mtswf.connect([
        (inputnode, mtsnode, [('bold_file','in_file')]),
        (mtsnode, outputnode, [('out_file','mts_file')]),
    ])
    return mtswf
bpp=meantimeseries(name='ali_ata_bak',t1seg='/local/data/tsc/bids/derivatives/sub-a002/ses-01/anat/sub-a002_ses-01_space-T1_parc.nii.gz')
bpp.write_graph(graph2use='orig')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[17]:


# parameters to test
#omp_nthreads=8

# experiment folder variables
experiment_dir = '/local/data/tsc/bids'
#output_dir = '/local/data/tsc/fmriprep_trial/output_dir'
#working_dir = '/local/data/tsc/fmriprep_trial/working_dir'
subject_data, layout = collect_data(experiment_dir, 'a002', None, None)

#subject_list = ['sub-a012']
#session_list = ['ses-01','ses-02']

#TR = 3
#fwhm = [0, 6]
#n_dummy_volumes=0

def rsfmri_workflow(bold_file, t1_seg, name, omp_nthreads, layout=None):
    ref_file=bold_file
    dirname, base, _ = split_filename(ref_file)
    mem_gb = {'filesize': 1, 'resampled': 1, 'largemem': 1}
    bold_tlen = 10

    if os.path.isfile(ref_file):
        bold_tlen, mem_gb = _create_mem_gb(ref_file)
    metadata = layout.get_metadata(ref_file)
    run_stc = ("SliceTiming" in metadata)
    
    if 'RepetitionTime' in metadata:
        TR = metadata['RepetitionTime']
    else:
        TR = 3

    
    # metaflow
    rsfmri = pe.Workflow(name=name)


    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file','subjects_dir',
                                                      'subject_id','t1_preproc',
                                                       't1_mask',
                                                       't1_seg',
                                                       'csf_mask',
                                                       'wm_mask']),
                       name = 'inputnode')
    inputnode.inputs.bold_file = bold_file
    inputnode.inputs.t1_seg=t1_seg
    
    outputnode = pe.Node(util.IdentityInterface(fields=['bold_mask',
                                                        'bold_brain',
                                                        'bold_ref',
                                                        'bold_mc',
                                                        'xforms',
                                                        'movpar_file',
                                                        'fd_file',
                                                        'remove_vol_list',
                                                        'bold_t1',
                                                        'bold_t1_mask',
                                                        'bold_t1_ref',
                                                        'confounds',
                                                        'bold_t1_cut',
                                                       'stc_file',
                                                        't1_brain',
                                                        'bold_t1_bp',
                                                        'out_res',
                                                        'out_data',
                                                        'friston_24',
                                                        'out_glm',
                                                        'mts',
                                                        'red_confounds',
                                                        'ccoef_file',
                                                        'itk_bold_to_t1',
                                                        'bold_t1_scrubbed',
                                                        'mts_scrubbed',
                                                        'ccoef_scrubbed_file',
                                                       ]),
                        name= 'outputnode')

    boldbuffer = pe.Node(util.IdentityInterface(fields=['bold_file']),
                        name='bold_buffer')
    
    bmask = pe.Node(fsl.ApplyMask(),
                           name='mask_brain')

    # Tentative bold reference image
    bold_ref_wf= bold_reference(omp_nthreads=omp_nthreads)

    # Bold splitter
    bold_split= pe.Node(fsl.Split(dimension='t'),
                           name='bold_split',mem_gb=mem_gb['filesize'] * 3)

    # head motion correction
    bold_hmc = head_motion(name='head_motion_correction',mem_gb=mem_gb['filesize'],
                          omp_nthreads=omp_nthreads,fd_threshold=0.5)

    # bold to t1 registration
    bold2t1 = bold2t1_reg(name='bold_to_t1w_registration',
                          mem_gb=mem_gb['resampled'],
                          omp_nthreads=omp_nthreads,
                          use_compression=False)

    # apply bold 2 t1 registration
    app_bold2t1 = apply_bold2t1(name='bold_to_t1w_transfer',
                               mem_gb=mem_gb['resampled'],
                               omp_nthreads=omp_nthreads,
                               use_compression=False)

    confounds = confound_estimate(mem_gb=mem_gb['largemem'],metadata=metadata,
                          name='confound_estimation')
    
    bpass = bandpass(name='band_pass',TR=TR, lpThr=0.1,hpThr=0.01)
    
    regressor = pe.Node(fsl.GLM(dat_norm=True,demean=True,des_norm=True,
                                out_file = base + '_t1_glm.nii.gz',
                                out_data_name = base + '_t1_data.nii.gz',
                                out_res_name = base + '_t1_res.nii.gz'),
                       name='regressor')
    scrubber = pe.Node(scrubBasedOnRemoveVolumeList(),
                      name='scrub_high_motion_frames')
    
    fslmts = meantimeseries(name='mean_time_series',t1seg=t1_seg)
    
    cccoef = pe.Node(pearCorrCoef(),
                     name='pear_corr_coef')
    
    fslmts2 = meantimeseries(name='mean_time_series_scrubbed',t1seg=t1_seg)
    
    cccoef2 = pe.Node(pearCorrCoef(),
                     name='pear_corr_coef_scrubbed')
    
    # slice time correction - update bold buffer
    if run_stc is True:
        slice_time = slice_time_correct(metadata=metadata,name='slice_timer')
        rsfmri.connect([
            (inputnode, slice_time, [('bold_file','inputnode.bold_file')]),
            (bold_ref_wf, slice_time, [('outputnode.skip_vols',
                                        'inputnode.skip_vols')]),
            (slice_time, boldbuffer, [('outputnode.stc_file','bold_file')]),
            (slice_time, outputnode, [('outputnode.stc_file','stc_file')]),
        ])
    else:
        rsfmri.connect([
            (inputnode, boldbuffer, [('bold_file','bold_file')]),
        ])
            
    
    rsfmri.connect([
        # ref image
        (inputnode, bold_ref_wf, [('bold_file','inputnode.bold_file')]),
        (bold_ref_wf,bold2t1, [('outputnode.bold_brain',
                               'inputnode.bold_brain')]),
        (bold_ref_wf, app_bold2t1, [('outputnode.bold_mask',
                                     'inputnode.ref_bold_mask'),
                                   ('outputnode.bold_brain',
                                   'inputnode.ref_bold_brain')]),
        (bold_ref_wf, confounds, [('outputnode.skip_vols',
                                  'inputnode.skip_vols')]),
        (bold_ref_wf,outputnode, [('outputnode.bold_mask','bold_mask'),
                                 ('outputnode.bold_brain','bold_brain')]),
        # split
        (boldbuffer, bold_split, [('bold_file','in_file')]),
        # HMC
        (bold_ref_wf, bold_hmc, [('outputnode.bold_ref','inputnode.bold_ref'),
                                ('outputnode.bold_file','inputnode.bold_file')]),
        #Bold2T1
        (inputnode, bmask, [('t1_preproc','in_file'),
                           ('t1_mask','mask_file')]),
        (bmask, bold2t1, [('out_file','inputnode.t1_brain')]),
        (bmask, app_bold2t1, [('out_file','inputnode.t1_brain')]),
        (inputnode, bold2t1, [('wm_mask','inputnode.wm_mask')]),
        (inputnode, app_bold2t1, [('bold_file','inputnode.name_source'),
                                 ('t1_mask','inputnode.t1_mask')]),
        (bold_hmc, app_bold2t1, [('outputnode.xforms','inputnode.hmc_xforms')]),
        (bold2t1, app_bold2t1, [('outputnode.itk_bold_to_t1',
                                 'inputnode.itk_bold_to_t1')]),
        (app_bold2t1, outputnode, [('outputnode.bold_t1','bold_t1'),
                                  ('outputnode.bold_t1_ref','bold_t1_ref'),
                                  ('outputnode.bold_t1_mask','bold_t1_mask')]),
        (inputnode, confounds, [('t1_mask','inputnode.t1_bmask'),
                               ('wm_mask','inputnode.t1_wmmask'),
                               ('csf_mask','inputnode.t1_csfmask')]),
        (bold_hmc, confounds, [('outputnode.movpar_file','inputnode.movpar_file'),
                              ('outputnode.bold_mc','inputnode.bold'),
                              ('outputnode.bold_mask', 'inputnode.bold_mask'),
                              ('outputnode.friston_24','inputnode.friston_24')]),
        (app_bold2t1, confounds, [('outputnode.bold_t1','inputnode.bold_t1'),
                                 ('outputnode.bold_t1_mask',
                                 'inputnode.bold_t1_mask')]),
        (confounds, outputnode, [('outputnode.confounds_file','confounds'),
                                 ('outputnode.red_confounds_file','red_confounds'),
                                 ('outputnode.bold_t1_cut','bold_t1_cut')]),
        
        # boldsplit to bold2t1 transfer
        (bold_split, app_bold2t1, [('out_files','inputnode.bold_split')]),
        
        (bmask,outputnode,[('out_file','t1_brain')]),
        #(app_bold2t1, regressor, [('outputnode.bold_t1','in_file')]),
        (confounds, regressor, [('outputnode.red_confounds_file','design'),
                               ('outputnode.bold_t1_cut','in_file')]),
        (regressor, bpass, [('out_res','inputnode.bold_file')]),
        (bpass,outputnode,[('outputnode.bp_file','bold_t1_bp')]),
        (bpass,scrubber, [('outputnode.bp_file','in_file')]),
        (bold_hmc, scrubber, [('outputnode.remove_list','remove_list')]),
        (scrubber, outputnode, [('out_file','bold_t1_scrubbed')]),
        (bpass,fslmts, [('outputnode.bp_file','inputnode.bold_file')]),
        (scrubber, fslmts2, [('out_file','inputnode.bold_file')]),
        (fslmts,outputnode,[('outputnode.mts_file','mts')]),
        (fslmts2,outputnode,[('outputnode.mts_file','mts_scrubbed')]),
        (fslmts, cccoef,[('outputnode.mts_file','in_file')]),
        (fslmts2,cccoef2,[('outputnode.mts_file','in_file')]),
        (cccoef,outputnode,[('out_file','ccoef_file')]),
        (cccoef2,outputnode,[('out_file','ccoef_scrubbed_file')]),
        (bold_hmc,outputnode, [('outputnode.bold_mc','bold_mc'),
                              ('outputnode.xforms','xforms'),
                              ('outputnode.movpar_file','movpar_file'),
                              ('outputnode.fd_file','fd_file')]),
        (regressor,outputnode,[('out_res','out_res'),
                              ('out_data','out_data'),
                              ('out_file','out_glm')]),
        (bold2t1, outputnode, [('outputnode.itk_bold_to_t1',
                                 'itk_bold_to_t1')]),
        
    ])
    return rsfmri

# connect select_files and data_sink to the workflow
rsfmri=rsfmri_workflow(bold_file='/local/data/tsc/bids/sub-a002/ses-01/func/sub-a002_ses-01_task-rest_bold.nii.gz',
                       t1_seg='/local/data/tsc/bids/derivatives/sub-a002/ses-01/anat/sub-a002_ses-01_space-T1_parc.nii.gz',
                       name='ali_ata_bak', omp_nthreads=8, layout=layout)
rsfmri.write_graph(graph2use='orig')
Image(filename="/fileserver/motion/seyhmus/code/graph.png")


# In[29]:


# parameters to test
#omp_nthreads=8

# experiment folder variables
experiment_dir = '/local/data/tsc/bids'
#output_dir = '/local/data/tsc/fmriprep_trial/output_dir'
#working_dir = '/local/data/tsc/fmriprep_trial/working_dir'
subject_data, layout = collect_data(experiment_dir, 'a002', None, None)

#subject_list = ['sub-a012']
#session_list = ['ses-01','ses-02']

#TR = 3
#fwhm = [0, 6]
#n_dummy_volumes=0

def rsfmri_workflow(bold_file, t1_seg, name, omp_nthreads, layout=None):
    ref_file=bold_file
    dirname, base, _ = split_filename(ref_file)
    mem_gb = {'filesize': 1, 'resampled': 1, 'largemem': 1}
    bold_tlen = 10

    if os.path.isfile(ref_file):
        bold_tlen, mem_gb = _create_mem_gb(ref_file)
    metadata = layout.get_metadata(ref_file)
    run_stc = ("SliceTiming" in metadata)
    
    if 'RepetitionTime' in metadata:
        TR = metadata['RepetitionTime']
    else:
        TR = 3

    
    # metaflow
    rsfmri = pe.Workflow(name=name)


    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file','subjects_dir',
                                                      'subject_id','t1_preproc',
                                                       't1_mask',
                                                       't1_seg',
                                                       'csf_mask',
                                                       'wm_mask']),
                       name = 'inputnode')
    inputnode.inputs.bold_file = bold_file
    inputnode.inputs.t1_seg=t1_seg
    
    outputnode = pe.Node(util.IdentityInterface(fields=['bold_mask',
                                                        'bold_brain',
                                                        'bold_ref',
                                                        'bold_mc',
                                                        'xforms',
                                                        'movpar_file',
                                                        'fd_file',
                                                        'remove_vol_list',
                                                        'bold_t1',
                                                        'bold_t1_mask',
                                                        'bold_t1_ref',
                                                        'confounds',
                                                        'bold_t1_cut',
                                                       'stc_file',
                                                        't1_brain',
                                                        'bold_t1_bp',
                                                        'out_res',
                                                        'out_data',
                                                        'friston_24',
                                                        'out_glm',
                                                        'mts',
                                                        'red_confounds',
                                                        'ccoef_file',
                                                        'itk_bold_to_t1',
                                                        'bold_t1_scrubbed',
                                                        'mts_scrubbed',
                                                        'ccoef_scrubbed_file',
                                                       ]),
                        name= 'outputnode')

    boldbuffer = pe.Node(util.IdentityInterface(fields=['bold_file']),
                        name='bold_buffer')
    
    bmask = pe.Node(fsl.ApplyMask(),
                           name='mask_brain')

    # Tentative bold reference image
    bold_ref_wf= bold_reference(omp_nthreads=omp_nthreads)

    # Bold splitter
    bold_split= pe.Node(fsl.Split(dimension='t'),
                           name='bold_split',mem_gb=mem_gb['filesize'] * 3)

    # head motion correction
    bold_hmc = head_motion(name='head_motion_correction',mem_gb=mem_gb['filesize'],
                          omp_nthreads=omp_nthreads,fd_threshold=0.5)

    # bold to t1 registration
    bold2t1 = bold2t1_reg(name='bold_to_t1w_registration',
                          mem_gb=mem_gb['resampled'],
                          omp_nthreads=omp_nthreads,
                          use_compression=False)

    # apply bold 2 t1 registration
    app_bold2t1 = apply_bold2t1(name='bold_to_t1w_transfer',
                               mem_gb=mem_gb['resampled'],
                               omp_nthreads=omp_nthreads,
                               use_compression=False)

    confounds = confound_estimate(mem_gb=mem_gb['largemem'],metadata=metadata,
                          name='confound_estimation')
    
    bpass = bandpass(name='band_pass',TR=TR, lpThr=0.1,hpThr=0.01)
    
    regressor = pe.Node(fsl.GLM(dat_norm=True,demean=True,des_norm=True,
                                out_file = base + '_t1_glm.nii.gz',
                                out_data_name = base + '_t1_data.nii.gz',
                                out_res_name = base + '_t1_res.nii.gz'),
                       name='regressor')
    scrubber = pe.Node(scrubBasedOnRemoveVolumeList(),
                      name='scrub_high_motion_frames')
    
    #fslmts = meantimeseries(name='mean_time_series',t1seg=t1_seg)
    
    cccoef = pe.Node(pearCorrCoef(),
                     name='pear_corr_coef')
    
    fslmts2 = meantimeseries(name='mean_time_series',t1seg=t1_seg)
    
    cccoef2 = pe.Node(pearCorrCoef(),
                     name='pear_corr_coef')
    
    # slice time correction - update bold buffer
    if run_stc is True:
        slice_time = slice_time_correct(metadata=metadata,name='slice_timer')
        rsfmri.connect([
            (inputnode, slice_time, [('bold_file','inputnode.bold_file')]),
            (bold_ref_wf, slice_time, [('outputnode.skip_vols',
                                        'inputnode.skip_vols')]),
            (slice_time, bold_split, [('outputnode.stc_file','in_file')]),
            #(slice_time, outputnode, [('outputnode.stc_file','stc_file')]),
        ])
    else:
        rsfmri.connect([
            (inputnode, boldbuffer, [('bold_file','bold_file')]),
        ])
            
    
    rsfmri.connect([
        # ref image
        (inputnode, bold_ref_wf, [('bold_file','inputnode.bold_file')]),
        (bold_ref_wf,bold2t1, [('outputnode.bold_brain',
                               'inputnode.bold_brain')]),
        (bold_ref_wf, app_bold2t1, [('outputnode.bold_mask',
                                     'inputnode.ref_bold_mask'),
                                   ('outputnode.bold_brain',
                                   'inputnode.ref_bold_brain')]),
        (bold_ref_wf, confounds, [('outputnode.skip_vols',
                                  'inputnode.skip_vols')]),
        #(bold_ref_wf,outputnode, [('outputnode.bold_mask','bold_mask'),
        #                         ('outputnode.bold_brain','bold_brain')]),
        # split
        #(boldbuffer, bold_split, [('bold_file','in_file')]),
        # HMC
        (bold_ref_wf, bold_hmc, [('outputnode.bold_ref','inputnode.bold_ref'),
                                ('outputnode.bold_file','inputnode.bold_file')]),
        #Bold2T1
        (inputnode, bmask, [('t1_preproc','in_file'),
                           ('t1_mask','mask_file')]),
        (bmask, bold2t1, [('out_file','inputnode.t1_brain')]),
        (bmask, app_bold2t1, [('out_file','inputnode.t1_brain')]),
        (inputnode, bold2t1, [('wm_mask','inputnode.wm_mask')]),
        (inputnode, app_bold2t1, [('bold_file','inputnode.name_source'),
                                 ('t1_mask','inputnode.t1_mask')]),
        (bold_hmc, app_bold2t1, [('outputnode.xforms','inputnode.hmc_xforms')]),
        (bold2t1, app_bold2t1, [('outputnode.itk_bold_to_t1',
                                 'inputnode.itk_bold_to_t1')]),
        #(app_bold2t1, outputnode, [('outputnode.bold_t1','bold_t1'),
        #                          ('outputnode.bold_t1_ref','bold_t1_ref'),
        #                          ('outputnode.bold_t1_mask','bold_t1_mask')]),
        (inputnode, confounds, [('t1_mask','inputnode.t1_bmask'),
                               ('wm_mask','inputnode.t1_wmmask'),
                               ('csf_mask','inputnode.t1_csfmask')]),
        (bold_hmc, confounds, [('outputnode.movpar_file','inputnode.movpar_file'),
                              ('outputnode.bold_mc','inputnode.bold'),
                              ('outputnode.bold_mask', 'inputnode.bold_mask'),
                              ('outputnode.friston_24','inputnode.friston_24')]),
        (app_bold2t1, confounds, [('outputnode.bold_t1','inputnode.bold_t1'),
                                 ('outputnode.bold_t1_mask',
                                 'inputnode.bold_t1_mask')]),
        #(confounds, outputnode, [('outputnode.confounds_file','confounds'),
        #                         ('outputnode.red_confounds_file','red_confounds'),
        #                         ('outputnode.bold_t1_cut','bold_t1_cut')]),
        
        # boldsplit to bold2t1 transfer
        (bold_split, app_bold2t1, [('out_files','inputnode.bold_split')]),
        
        #(bmask,outputnode,[('out_file','t1_brain')]),
        #(app_bold2t1, regressor, [('outputnode.bold_t1','in_file')]),
        (confounds, regressor, [('outputnode.red_confounds_file','design'),
                               ('outputnode.bold_t1_cut','in_file')]),
        (regressor, bpass, [('out_res','inputnode.bold_file')]),
        #(bpass,outputnode,[('outputnode.bp_file','bold_t1_bp')]),
        (bpass,scrubber, [('outputnode.bp_file','in_file')]),
        (bold_hmc, scrubber, [('outputnode.remove_list','remove_list')]),
        #(scrubber, outputnode, [('out_file','bold_t1_scrubbed')]),
        #(bpass,fslmts, [('outputnode.bp_file','inputnode.bold_file')]),
        (scrubber, fslmts2, [('out_file','inputnode.bold_file')]),
        #(fslmts,outputnode,[('outputnode.mts_file','mts')]),
        #(fslmts2,outputnode,[('outputnode.mts_file','mts_scrubbed')]),
        #(fslmts, cccoef,[('outputnode.mts_file','in_file')]),
        (fslmts2,cccoef2,[('outputnode.mts_file','in_file')]),
        #(cccoef,outputnode,[('out_file','ccoef_file')]),
        (cccoef2,outputnode,[('out_file','ccoef_scrubbed_file')]),
        #(bold_hmc,outputnode, [('outputnode.bold_mc','bold_mc'),
        #                      ('outputnode.xforms','xforms'),
        #                      ('outputnode.movpar_file','movpar_file'),
        #                      ('outputnode.fd_file','fd_file')]),
        #(regressor,outputnode,[('out_res','out_res'),
        #                      ('out_data','out_data'),
        #                      ('out_file','out_glm')]),
        #(bold2t1, outputnode, [('outputnode.itk_bold_to_t1',
        #                         'itk_bold_to_t1')]),
        
    ])
    return rsfmri

# connect select_files and data_sink to the workflow
rsfmri=rsfmri_workflow(bold_file='/local/data/tsc/bids/sub-a002/ses-01/func/sub-a002_ses-01_task-rest_bold.nii.gz',
                       t1_seg='/local/data/tsc/bids/derivatives/sub-a002/ses-01/anat/sub-a002_ses-01_space-T1_parc.nii.gz',
                       name='ali_ata_bak', omp_nthreads=8, layout=layout)
rsfmri.write_graph(graph2use='orig')
Image(filename="/fileserver/motion/seyhmus/code/graph.png")


# In[21]:


def init_fmriprep_wf(subject_list, work_dir, output_dir, bids_dir,
                     low_mem, omp_nthreads):
    
    fmriprep_wf = pe.Workflow(name='fmriprep_wf')
    fmriprep_wf.base_dir = work_dir

    for subject_id in subject_list:
        sessions_list=os.listdir(os.path.join(bids_dir,subject_id))
        for session_id in sessions_list:
            out_dir = os.path.join(output_dir,subject_id,session_id)
            print(out_dir)
            single_subject_wf = init_single_subject_wf(
                subject_id=subject_id[4:8],
                ses_id=session_id[4:6],
                name="ss_" + subject_id[4:8] + "_" + session_id[4:6] + "_wf",
                output_dir=out_dir,
                bids_dir=bids_dir,
                low_mem=low_mem,
                omp_nthreads=omp_nthreads,
            )
            fmriprep_wf.add_nodes([single_subject_wf])
    return fmriprep_wf


def init_single_subject_wf(subject_id, ses_id, name, output_dir,
                           bids_dir, low_mem, omp_nthreads):

    subject_data, layout = collect_data(bids_dir, subject_id, None, None)

    sdir = bids_dir + '/sub-' + subject_id
    stext ='sub-' + subject_id + '_ses-' + ses_id
    src = bids_dir + '/sub-' + subject_id + '/ses-' + ses_id
    der = bids_dir + '/derivatives/sub-' + subject_id + '/ses-' + ses_id
    
    bold = src + '/func/' + stext + '_task-rest_bold.nii.gz'
    t1 = src + '/anat/' + stext + '_T1w.nii.gz'
    t2 = src + '/anat/' + stext +'_T2w.nii.gz'
    t1_bmask = der + '/anat/' + stext + '_space-T1_bmask.nii.gz'
    t1_wm = der + '/anat/' + stext + '_space-T1_wm.nii.gz'
    t1_gm = der + '/anat/' + stext + '_space-T1_gm.nii.gz'
    t1_csf = der + '/anat/' + stext + '_space-T1_csf.nii.gz'
    t1_parc = der + '/anat/' + stext + '_space-T1_parc.nii.gz'

    workflow = pe.Workflow(name=name)
    
    inputnode = pe.Node(util.IdentityInterface(fields=['subjects_dir',
                                                       'subject_id',
                                                       'bold','t1','t2',
                                                      't1_mask','t1_wm',
                                                      't1_gm','t1_csf',
                                                      't1_parc']),
                        name='inputnode')
    inputnode.inputs.subject_id=subject_id
    inputnode.inputs.bold = bold
    inputnode.inputs.t1 = t1
    inputnode.inputs.t2 = t2
    inputnode.inputs.t1_bmask = t1_bmask
    inputnode.inputs.t1_wm = t1_wm
    inputnode.inputs.t1_gm = t1_gm
    inputnode.inputs.t1_csf = t1_csf
    inputnode.inputs.t1_parc = t1_parc
    

    outputnode = pe.Node(util.IdentityInterface(fields=['bold_mask',
                                                        'bold_brain',
                                                        'bold_ref',
                                                        'bold_mc',
                                                        'xforms',
                                                        'movpar_file',
                                                        'fd_file',
                                                        'remove_vol_list',
                                                        'bold_t1',
                                                        'bold_t1_mask',
                                                        'bold_t1_ref',
                                                        'confounds',
                                                        'bold_t1_cut',
                                                       'stc_file',
                                                        't1_brain',
                                                        'bold_t1_bp',
                                                        'out_res',
                                                        'out_data',
                                                        'out_glm',
                                                        'mts',
                                                        'red_confounds',
                                                        'ccoef',
                                                        'itk_bold_to_t1',
                                                       'friston_24',
                                                        'ccoef_file',
                                                        'itk_bold_to_t1',
                                                        'bold_t1_scrubbed',
                                                        'mts_scrubbed',
                                                        'ccoef_scrubbed_file',
                                                       ]),
                         name='outputnode')
                                                            
    data_sink = pe.Node(nio.DataSink(),name='data_sink')
    data_sink.inputs.base_directory=output_dir

    # substitutions
    substitutions = [('subject_id_',''), ('session_id_','')]
    data_sink.inputs.substitutions = substitutions

    #bidssrc = pe.Node(BIDSDataGrabber(subject_data=subject_data, anat_only=None),
    #                  name='bidssrc')

    #bids_info = pe.Node(BIDSInfo(), name='bids_info', run_without_submitting=True)
    
    preproc = rsfmri_workflow(bold_file=bold,t1_seg=t1_parc, name='rsfmri_preproc',
                              omp_nthreads=omp_nthreads, layout=layout)
    
    workflow.connect([
        (inputnode,preproc,[('bold','inputnode.bold_file'),
                           ('t1','inputnode.t1_preproc'),
                           ('subject_id','inputnode.subject_id'),
                           ('t1_bmask','inputnode.t1_mask'),
                           ('t1_parc','inputnode.t1_seg'),
                           ('t1_csf','inputnode.csf_mask'),
                           ('t1_wm','inputnode.wm_mask')]),
        (preproc,outputnode,[('outputnode.bold_t1','bold_t1'),
                            ('outputnode.bold_t1_mask','bold_t1_mask'),
                            ('outputnode.bold_t1_ref','bold_t1_ref'),
                            ('outputnode.confounds','confounds'),
                            ('outputnode.stc_file','stc_file'),
                            ('outputnode.t1_brain','t1_brain'),
                            ('outputnode.bold_t1_bp','bold_t1_bp'),
                            ('outputnode.red_confounds','red_confounds'),
                            ('outputnode.ccoef_file','ccoef'),
                            ('outputnode.mts','mts'),
                            ('outputnode.bold_mask','bold_mask'),
                            ('outputnode.bold_brain','bold_brain'),
                            ('outputnode.bold_ref','bold_ref'),
                            ('outputnode.bold_mc','bold_mc'),
                            ('outputnode.xforms','xforms'),
                            ('outputnode.movpar_file','movpar_file'),
                            ('outputnode.fd_file','fd_file'),
                            ('outputnode.remove_vol_list','remove_vol_list'),
                            ('outputnode.bold_t1_cut','bold_t1_cut'),
                            ('outputnode.out_res','out_res'),
                            ('outputnode.out_data','out_data'),
                            ('outputnode.out_glm','out_glm'),
                            ('outputnode.itk_bold_to_t1','itk_bold_to_t1'),
                            ('outputnode.friston_24','friston_24'),
                            ('outputnode.bold_t1_scrubbed','outputnode.bold_t1_scrubbed'),
                            ('outputnode.mts_scrubbed','mts_scrubbed'),
                            ('outputnode.ccoef_scrubbed_file','ccoef_scrubbed_file'),
                            ]),                                
        (outputnode,data_sink,[#('bold_t1','bold_t1.@bold_t1_preproc'),
                             ('bold_t1_ref','bold_t1.@bold_t1_ref'),
                             ('red_confounds','confounds.@red_confounds'),
                             ('bold_t1_mask','bold_t1.@bold_t1_mask'),
                             ('confounds','confounds.@confounds'),
                             #('bold_t1_cut','confounds.@bold_t1_cut'),
                             #('stc_file','bold.@slice_time'),
                             ('ccoef','corrcoef.@ccoef'),
                             #('t1_brain','anat.@t1_brain'),
                             ('bold_t1_bp','bold_t1.@band_pass'),
                             ('mts','corrcoef.@meanTimeSeries'),
                             ('bold_mask','bold.@bold_mask'),
                             ('bold_brain','bold.@bold_brain'),
                             ('bold_ref','bold.@bold_ref'),
                             #('bold_mc','bold.@bold_mc'),
                             ('xforms','motion.@xforms'),
                             ('movpar_file','motion.@movpar_file'),
                             ('fd_file','motion.@fd'),
                             ('remove_vol_list','motion.@rml'),
                             #('out_res','regressor.@out_res'),
                             #('out_data','regressor.@out_data'),
                             #('out_glm','regressor.@out_glm'),
                             ('itk_bold_to_t1',
                              'registration.@itk_bold_to_t1'),
                             ('friston_24','motion.@friston_24'),
                             #('bold_t1_scrubbed','bold_t1.@scrubbed'),
                             ('mts_scrubbed','corrcoef.@mts_scrubbed'),
                             ('ccoef_scrubbed_file','corrcoef.@ccoef_scrubbed')]),
    ]) 
    return workflow

bids_dir = ('/local/data/tsc/bids')
subjects = [ 'sub-t042', 'sub-t043', 'sub-t044', 'sub-t045', 'sub-t046', 'sub-t047', 'sub-t048', 'sub-t049']
sub_ses=['sub-c021/ses-03']
# 
# 'sub-t035/ses-03', 
work_dir = ('/local/data/tsc/bids/derivatives/working_dir')
output_dir = ('/local/data/tsc/bids/derivatives/fmriprep_dmc')
low_mem=6
omp_nthreads=24

#for subject_id in subjects:
for subses in sub_ses:
        #sessions_list=os.listdir(os.path.join(bids_dir,subject_id))
        #for session_id in sessions_list:
            #out_dir = os.path.join(output_dir,subject_id,session_id)
        out_dir = os.path.join(output_dir,subses)
        single_subject_wf = init_single_subject_wf(
                #subject_id=subject_id[4:8],
            subject_id=subses[4:8],
                #ses_id=session_id[4:6],
            ses_id=subses[13:15],
                #name="ss_" + subject_id[4:8] + "_" + session_id[4:6] + "_wf",
            name="ss_" + subses[4:8] + "_" + subses[13:15] + "_wf",
            output_dir=out_dir,
            bids_dir=bids_dir,
            low_mem=low_mem,
            omp_nthreads=omp_nthreads,
        )
        single_subject_wf.base_dir = work_dir
        single_subject_wf.run()


# In[ ]:


#cc= np.loadtxt('/local/data/deneme_delete_final/corrcoef/sub-a002_ses-01_task-rest_bold_t1_res_bandpassed_ts_corrcoef.txt')
#cc2=np.loadtxt('/local/data/deneme_delete_final/corrcoef/sub-a002_ses-01_task-rest_bold_t1_res_bandpassed_scrubbed_ts_corrcoef.txt')
#plt.subplot(1,2,1)
#plt.imshow(cc)
#plt.subplot(1,2,2)
#plt.imshow(cc)
bids_dir ='/local/data/tsc/bids'
subjects_list=os.listdir(bids_dir)
print(subjects_list)
subjects = ['sub-a002', 'sub-a006', 'sub-a007', 'sub-a008', 'sub-a009', 'sub-a011', 'sub-a012', 'sub-a013', 'sub-a014', 'sub-a015', 'sub-a016', 'sub-a017', 'sub-a019', 'sub-a021', 'sub-a023', 'sub-a027', 'sub-a028', 'sub-a029', 'sub-c001', 'sub-c002', 'sub-t050', 'sub-a010', 'sub-c003', 'sub-c004', 'sub-c005', 'sub-c006', 'sub-c007', 'sub-a001', 'sub-a024', 'sub-c008', 'sub-c009', 'sub-c010', 'sub-c011', 'sub-c012', 'sub-c013', 'sub-c014', 'sub-c015', 'sub-c016', 'sub-c017', 'sub-c018', 'sub-c019', 'sub-c020', 'sub-c021', 'sub-c022', 'sub-c023', 'sub-c024', 'sub-c025', 'sub-c027', 'sub-c028', 'sub-c029', 'sub-c030', 'sub-c031', 'sub-c032', 'sub-c033', 'sub-c034', 'sub-c036', 'sub-c037', 'sub-c038', 'sub-c041', 'sub-c042', 'sub-c043', 'sub-c044', 'sub-c045', 'sub-c046', 'sub-c047', 'sub-c048', 'sub-t001', 'sub-t002', 'sub-t003', 'sub-t004', 'sub-t005', 'sub-t006', 'sub-t007', 'sub-t008', 'sub-t009', 'sub-t010', 'sub-t011', 'sub-t012', 'sub-t013', 'sub-t014', 'sub-t017', 'sub-t019', 'sub-t020', 'sub-t021', 'sub-t022', 'sub-t023', 'sub-t024', 'sub-t025', 'sub-t026', 'sub-t028', 'sub-t029', 'sub-t030', 'sub-t031', 'sub-t032', 'sub-t033', 'sub-t034', 'sub-t035', 'sub-t036', 'sub-t037', 'sub-t038', 'sub-t039', 'sub-t040', 'sub-t041', 'sub-t042', 'sub-t043', 'sub-t044', 'sub-t045', 'sub-t046', 'sub-t047', 'sub-t048', 'sub-t049']
for sid in subjects:
    ssid=sid[4:8]
    print(ssid)
    session_list = os.listdir(os.path.join(bids_dir, sid))
    for sesid in session_list:
        print(sesid)
        out_dir = os.path.join(bids_dir,'derivatives','fmriprep_dmc',sid,sesid)
        print(out_dir)


# In[ ]:


# parameters to test
#omp_nthreads=8

# experiment folder variables
experiment_dir = '/local/data/tsc/bids'
#output_dir = '/local/data/tsc/fmriprep_trial/output_dir'
#working_dir = '/local/data/tsc/fmriprep_trial/working_dir'
subject_data, layout = collect_data(experiment_dir, 'a002', None, None)


def denemewf(bold_file, confounds, name, omp_nthreads, layout=None):
    ref_file=bold_file
    mem_gb = {'filesize': 1, 'resampled': 1, 'largemem': 1}
    bold_tlen = 10

    if os.path.isfile(ref_file):
        bold_tlen, mem_gb = _create_mem_gb(ref_file)
    metadata = layout.get_metadata(ref_file)
    run_stc = ("SliceTiming" in metadata)
    
    if 'RepetitionTime' in metadata:
        TR = metadata['RepetitionTime']
    else:
        TR = 3

    
    # metaflow
    deneme = pe.Workflow(name=name)


    inputnode = pe.Node(util.IdentityInterface(fields=['bold_file',
                                                       'confounds']),
                       name = 'inputnode')
    inputnode.inputs.bold_file = bold_file
    inputnode.inputs.confounds = confounds
    
    outputnode = pe.Node(util.IdentityInterface(fields=['bold_resbp']),
                        name= 'outputnode')
    
    bpass = bandpass(name='band_pass',TR=TR, lpThr=0.1,hpThr=0.01)
    
    regressor = pe.Node(fsl.GLM(dat_norm=True,demean=True,des_norm=True,
                                out_res_name='bold_t1_res.nii.gz'),
                       name='regressor')
    data_sink = pe.Node(nio.DataSink(),name='data_sink')
    data_sink.inputs.base_directory='/local/data/denemewf'

    deneme.connect([
        (inputnode, regressor, [('bold_file','in_file')]),
        (inputnode, regressor, [('confounds','design')]),
        (regressor, bpass, [('out_res','inputnode.bold_file')]),
        (bpass,outputnode,[('outputnode.bp_file','bold_resbp')]),
        (outputnode,data_sink, [('bold_resbp','bolrespb')])
    ])
    return deneme

# connect select_files and data_sink to the workflow
oya=denemewf(bold_file='/local/data/tsc/bids/sub-a002/ses-01/func/sub-a002_ses-01_task-rest_bold.nii.gz',
                       confounds='/local/data/deneme_delete/confounds/confounds.txt',
                name='ali', omp_nthreads=8, layout=layout)
oya.write_graph(graph2use='orig')
Image(filename="/fileserver/motion/seyhmus/code/graph.png")
#oya.run()


# In[ ]:


anat_preproc = pe.Workflow(name='anat_preprocessing')

template4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0.nii.gz'
template_probability_map4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumProbabilityMask.nii.gz'
template_extraction_mask4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumRegistrationMask.nii.gz'
mni_reference_image='/local/fsl/data/standard/MNI152_T1_3mm_brain.nii.gz'

inputnode = pe.Node(util.IdentityInterface(fields=['t1w', 't2w','ref_image' 'subjects_dir', 'subject_id']),
                   name='inputnode')
inputnode.inputs.ref_image = mni_reference_image
inputnode.inputs.t1w = '/fileserver/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/anat/bestt1w_reoriented2std.nii.gz'
outputnode = pe.Node(util.IdentityInterface(fields=['t1_preproc', 't1_brain','t1_mask', 't1_seg','t1_tpms',
                                                   't1_2_mni_warped_image',
                                                    't1_2_mni_forward_transforms',
                                                    't1_2_mni_composite_transform',
                                                    'mni_2_t1_inverse_composite_transform',
                                                    'mni_2_t1_inverse_warped_image',
                                                    'mni_2_t1_reverse_transforms',
                                                   'subjects_dir', 'subject_id']),
                    name='outputnode')

t1_reorient = pe.Node(fsl.Reorient2Std(),
                     name='t1_reorient2std')

brain_extract = pe.Node(ants.BrainExtraction(), name='brain_extract')
brain_extract.inputs.anatomical_image = '/fileserver/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/anat/bestt1w_reoriented2std.nii.gz'
brain_extract.inputs.brain_probability_mask =template_probability_map4bet
brain_extract.inputs.brain_template = template4bet
brain_extract.inputs.extraction_registration_mask = template_extraction_mask4bet
brain_extract.inputs.out_prefix='highres'
brain_extract.inputs.num_threads=12

# Registration (good) - computes registration between subject's structural and MNI template.
antsreg = pe.Node(ants.Registration(args='--float',
                            collapse_output_transforms=True,
                            fixed_image=mni_reference_image,
                            initial_moving_transform_com=True,
                            num_threads=12,
                            output_inverse_warped_image=True,
                            output_warped_image=True,
                            sigma_units=['vox']*3,
                            transforms=['Rigid', 'Affine', 'SyN'],
                            terminal_output='file',
                            winsorize_lower_quantile=0.005,
                            winsorize_upper_quantile=0.995,
                            convergence_threshold=[1e-06],
                            convergence_window_size=[10],
                            metric=['MI', 'MI', 'CC'],
                            metric_weight=[1.0]*3,
                            number_of_iterations=[[1000, 500, 250, 100],
                                                  [1000, 500, 250, 100],
                                                  [100, 70, 50, 20]],
                            radius_or_number_of_bins=[32, 32, 4],
                            sampling_percentage=[0.25, 0.25, 1],
                            sampling_strategy=['Regular',
                                               'Regular',
                                               'None'],
                            shrink_factors=[[8, 4, 2, 1]]*3,
                            smoothing_sigmas=[[3, 2, 1, 0]]*3,
                            transform_parameters=[(0.1,),
                                                  (0.1,),
                                                  (0.1, 3.0, 0.0)],
                            use_histogram_matching=True,
                            write_composite_transform=True),
               name='antsreg')
        
anat_preproc.connect([
    (inputnode, t1_reorient, [('t1w','in_file')]),
    (inputnode, antsreg, [('ref_image','fixed_image')]),
    (t1_reorient, brain_extract, [('out_file','anatomical_image')]),
    (brain_extract, antsreg, [('BrainExtractionBrain','moving_image')]),
    (antsreg, outputnode, [('forward_transforms','t1_2_mni_forward_transforms')]),
    (antsreg, outputnode, [('composite_transform', 't1_2_mni_composite_transform')]),
    (antsreg, outputnode, [('inverse_warped_image','mni_2_t1_inverse_warped_image')]),
    (antsreg, outputnode, [('inverse_composite_transform','mni_2_t1_inverse_composite_')]),
    (antsreg, outputnode, [('warped_image','t1_2_mni_warped_image')]),
    (antsreg, outputnode, [('reverse_transforms','mni_2_t1_reverse_transforms')]),
])

