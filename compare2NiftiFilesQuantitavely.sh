#! /bin/sh
# compares two nifti data files and returns the difference (error) normalized by the squared magnitude of reference image
# 
# Author: Seyhmus Guler
#
# Note: The comparison is based on the metrics used in the FSL-FEEDS unit tests

data=`realpath $1`
ref=`realpath $2`
outputdir=$(pwd)

data_image_yn=`${FSLDIR}/bin/imtest $data`
ref_image_yn=`${FSLDIR}/bin/imtest $ref`

if [ $data_image_yn -eq 1 ] && [ $ref_image_yn -eq 1 ] ; then
    ${FSLDIR}/bin/fslmaths ${data} -sub ${ref} -sqr -nan ${outputdir}/errsq -odt float
    ${FSLDIR}/bin/fslmaths ${ref} -sqr -nan ${outputdir}/meansq -odt float
    
    sqerror_mean=`${FSLDIR}/bin/fslstats ${outputdir}/errsq -m`
    sqref_mean=`${FSLDIR}/bin/fslstats ${outputdir}/meansq -m`
    echo "sqerror_mean = $sqerror_mean"
    echo "sqref_mean = $sqref_mean"

    norm_error_mean=`echo " sqrt($sqerror_mean) " | bc -l`
    norm_ref_mean=`echo " sqrt($sqref_mean) " | bc -l`
    perror=`echo " $norm_error_mean / $norm_ref_mean * 100 " | bc -l`
    echo "% error = $perror"
    rm $outputdir/errsq* 
    rm $outputdir/meansq*
fi
