
# coding: utf-8

# In[31]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nibabel as nib
import matplotlib.pyplot as plt

from os.path import join as opj
from os.path import abspath
from IPython.display import Image


# In[69]:


experiment_dir = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis'
output_dir = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sca_nipype'
working_dir = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sca_nipype_tmp'

subject_list = ['sub-01']
session_list = ['ses-01','ses-02']
acq_list = ['nomo', 'sm10', 'sm20']

TR = 3
fwhm = [0, 6]
n_dummy_volumes=0


# In[105]:


# map node (multiple inputs to and input)
# nodename = pe.MapNode(interface_function(), name='label', iterfield=['in_file']) 

# slice timing correction
slice_timing = pe.Node(fsl.SliceTimer(), name='slice_timing')
slice_timing.inputs.interleaved=True

# motion outliers
motion_outliers = pe.Node(fsl.MotionOutliers(), name = 'motion_outliers')
motion_outliers.inputs.dummy=n_dummy_volumes
motion_outliers.inputs.out_file=opj(working_dir,'motion_confounders')
motion_outliers.inputs.metric='fd'
motion_outliers.inputs.threshold=0.5
motion_outliers.inputs.out_metric_values=opj(working_dir,'fd.txt')
motion_outliers.inputs.out_metric_plot=opj(working_dir,'fd.png')
motion_outliers.inputs.args='-v --nocleanup -t '+ working_dir

# smoothing
smoothing = pe.Node(fsl.Smooth(), name='smooth')
smoothing.iterables=("fwhm", fwhm)


# In[85]:


preproc = pe.Workflow(name='preprocessing')
preproc.base_dir = working_dir
preproc.connect([(slice_timing, motion_outliers, [('slice_time_corrected_file','in_file')]),
                (slice_timing, smoothing, [('slice_time_corrected_file','in_file')]),
                ])


# In[86]:


# infosource - function free node to iterate over the list of subjects and sessions
info_source = pe.Node(util.IdentityInterface(fields=['subject_id',
                                                     'session_id',
                                                    'acq_id']), name='info_source')
info_source.iterables = [('subject_id', subject_list),
                        ('session_id', session_list),
                        ('acq_id', acq_list)]

# select files
#templates={'func': '{experiment_dir}/{subject_id}/{session_id}/func/{subject_id}_{session_id}_task-rest_acq-{acq_id}_rec-ori_bold.nii.gz',
#          
templates={'func': opj(experiment_dir,'{subject_id}/{session_id}/func/{subject_id}_{session_id}_task-rest_acq-{acq_id}_rec-ori_bold.nii.gz'),
          'anat': opj(experiment_dir,'{subject_id}/ses-01/anat/{subject_id}_ses-01_T1w.nii.gz')}
select_files = pe.Node(io.SelectFiles(templates,
                                     base_directory = experiment_dir), name = 'select_files')

# data sink
data_sink = pe.Node(io.DataSink(),name='data_sink')
data_sink.inputs.base_directory=output_dir
# data_sink.inputs.container='preprocessing'

# substitutions
substitutions = [('subject_id_',''), ('session_id_',''), ('acq_id_','')]
data_sink.inputs.substitutions = substitutions

# connect select_files and data_sink to the workflow

preproc.connect([(info_source, select_files, [('subject_id', 'subject_id'),
                                             ('session_id', 'session_id'),
                                             ('acq_id', 'acq_id')]),
                (select_files, slice_timing, [('func', 'in_file')]),
                (smoothing, data_sink, [('smoothed_file','smooth')]),
                (motion_outliers, data_sink, [('out_file','motion.@confounders'),
                                             ('out_metric_plot', 'motion.@metric_plot'),
                                             ('out_metric_values', 'motion.@metric_values')]),
                (slice_timing, data_sink, [('slice_time_corrected_file', 'slicet')])])


# In[87]:


preproc.write_graph(graph2use='flat')


# In[88]:


Image(filename="/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sca_nipype_tmp/preprocessing/graph.png")


# In[93]:


io.SelectFiles.help()
# preproc.run('MultiProc',plugin_args={'n_procs': 12})

