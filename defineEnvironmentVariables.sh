#! /bin/sh
# Define environment variables that may be needed in other scripts

#Written by Seyhmus Guler

#======================================================================
# fmri data
#----------------------------------------------------------------------

# fmri data directory
DATADIR=/common/motion/seyhmus/data/jack-temp/case5-scan01/data_for_analysis/fmri_finger_nomoco_nomotion

# subject directory 
SUBJECTDIR=/common/motion/seyhmus/data/jack-temp/case5-scan01/data_for_analysis/

# structural data directory
STRUCTDIR=$SUBJECTDIR

# stimulus patterns - paradigm specifications
STIMDIR=$DATADIR

#======================================================================


#======================================================================
# fsl-related directories (already set up)
#----------------------------------------------------------------------

# fsl directory
FSLDIR=$FSLDIR

