
# coding: utf-8

# In[2]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nibabel as nib
import matplotlib.pyplot as plt
from nipype.interfaces.base import (
    traits,
    TraitedSpec,
    CommandLineInputSpec,
    CommandLine,
    File,
    Directory,
    isdefined,
    SimpleInterface,
)

from os.path import join as opj
from os.path import abspath
from IPython.display import Image
import tempfile as tempfile
import os


# In[ ]:


class dmcInputSpec(BaseInterfaceInputSpec):
    in_file = InputMultiPath(File(exists=True), mandatory=True,
                              desc='list of MAT files from MCFLIRT')
    in_reference = File(exists=True, mandatory=True,
                        desc='input image for spatial reference')
    in_source = File(exists=True, mandatory=True,
                     desc='input image for spatial source')
    num_threads = traits.Int(1, usedefault=True, nohash=True,
                             desc='number of parallel processes')


class dmcOutputSpec(TraitedSpec):
    out_file = File(desc='the output ITKTransform file')

class dynamic_motion_completion(SimpleInterface):

    input_spec = MCFLIRT2ITKInputSpec
    output_spec = MCFLIRT2ITKOutputSpec

    def _run_interface(self, runtime):
        num_threads = self.inputs.num_threads
        if num_threads < 1:
            num_threads = None
        with TemporaryDirectory(prefix='tmp-', dir=runtime.cwd) as tmp_folder:
            # Inputs are ready to run in parallel
            if num_threads is None or num_threads > 1:
                from concurrent.futures import ThreadPoolExecutor
                with ThreadPoolExecutor(max_workers=num_threads) as pool:
                    itk_outs = list(pool.map(_mat2itk, [
                        (in_mat, self.inputs.in_reference, self.inputs.in_source, i, tmp_folder)
                        for i, in_mat in enumerate(self.inputs.in_files)]
                    ))
            else:
                itk_outs = [_mat2itk((
                    in_mat, self.inputs.in_reference, self.inputs.in_source, i, tmp_folder))
                    for i, in_mat in enumerate(self.inputs.in_files)
                ]

        # Compose the collated ITK transform file and write
        tfms = '#Insight Transform File V1.0\n' + ''.join(
            [el[1] for el in sorted(itk_outs)])

        self._results['out_file'] = os.path.join(runtime.cwd, 'mat2itk.txt')
        with open(self._results['out_file'], 'w') as f:
            f.write(tfms)

        return runtime

