import numpy as np
import sys

numvolumes = int(sys.argv[1])

inds = np.arange(numvolumes)

for val in inds:
 print "1 {linval} {quadval}".format(linval=val, quadval=np.square(val))
