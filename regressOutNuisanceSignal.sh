#! /bin/sh
# regresses out nuisance signals from fMRI data
#
# author : seyhmus guler
# date : 4/4/2017
#
#

#======================================================================
# usage 
#----------------------------------------------------------------------

print_usage () {
    echo "
    Regressing out nuisance signal from fMRI data using FSL tools
    
    Usage: 
    `basename $0` -i <input_data> [options]

    Compulsory arguments (You MUST set all of):
        -i      input file name (4D image file)
    
    Optional arguments for nuisance signal regression
        -o      output file name, default: <input_file_name>_nui_res
        -g      global signal regression on (1) off (0), default: off (on requires -b option)
        -b      brain mask
        -c      csf mean signal regression on (1) off (0), default: off (on requires -f option)
        -f      csf mask
        -w      wm mean signal regression on (1) off (0), default: off (on requires -m option)
        -m      white matter mask
        -d      design matrix (e.g. in .txt format) of additional nuisance regressors
        -n      normalize the residual data
        -h      display this help text"

    }

if [ $# -eq 0 ] ; then print_usage ; exit 1 ; fi 
#======================================================================


#=====================================================================
# options
#----------------------------------------------------------------------

global_signal_regression_yn=0
csf_mean_signal_regression_yn=0
wm_mean_signal_regression_yn=0

design_file=
options=':i:o:g:b:c:f:w:m:d:tlqh'
while getopts $options OPT; do
    case $OPT in

        i)
            func=$OPTARG
            func=$(realpath $(dirname $func))/$(basename $func)
            echo "input functional data:  $func"
            if [ ! -f $func ] ; then echo -e "\e[31mERROR: input data file $func not found\e[0m" ; exit 1 ; fi 
            ;;
        
        o)
            out_file=`realpath $OPTARG`
            echo "output file name: $out_file"
            if [ -f $out_file ] ; then
                echo "WARNING: output file $output_dir already exists, some files may be overwritten"
            fi
            ;;
        
        g)
            global_signal_regression_yn=$OPTARG
            echo "global signal regression : $global_signal_regression_yn"
            ;;
        
        b)
            brain_mask=$OPTARG
            brain_mask=$(realpath $(dirname $brain_mask))/$(basename $brain_mask)
            echo "brain mask for global signal regression: $brain_mask"
            if [ ! -f $brain_mask ] ; then echo -e "\e[31mERROR: brain mask $brain_mask not found\e[0m" ; exit 1 ; fi 
            ;;

        c)
            csf_mean_signal_regression_yn=$OPTARG
            echo "csf mean signal regression : $csf_mean_signal_regression_yn"
            ;;
        
        f)
            csf_mask=$OPTARG
            csf_mask=$(realpath $(dirname $csf_mask))/$(basename $csf_mask)
            echo "csf mask for csf mean signal regression: $csf_mask"
            if [ ! -f $csf_mask ] ; then echo -e "\e[31mERROR: csf mask $csf_mask not found\e[0m" ; exit 1 ; fi 
            ;;
        
        w)
            wm_mean_signal_regression_yn=$OPTARG
            echo "white matter mean signal regression : $wm_mean_signal_regression_yn"
            ;;
        m)
            wm_mask=$OPTARG
            wm_mask=$(realpath $(dirname $wm_mask))/$(basename $wm_mask)
            echo "white matter mask for wm mean signal regression: $wm_mask"
            if [ ! -f $wm_mask ] ; then echo -e "\e[31mERROR: white matter mask $wm_mask not found\e[0m" ; exit 1 ; fi 
            ;;

        d)
            design_file=`realpath $OPTARG`
            echo "design file : $design_file"
            if [ ! -f $design_file ] ; then
                echo -e "\e[31mERROR: design file $design_file not found\e[0m"
            fi
            ;;

        t)
            constant_regression_yn=1
            echo "constant signal regression: YES" 
            ;;
        l)
            linear_regression_yn=1
            echo "linear signal regression: YES"
            ;;
        q)
            quadratic_regression_yn=1
            echo "quadratic signal regression: YES"
            ;;

        n)
            normalize_yn=1
            ;;
        h)
            print_usage
            exit 1 
            ;;
    esac
done

if [ -z $out_file ] ; then
    out_file=$(pwd)/$(basename $(basename $func .gz) .nii)
    out_file=$(pwd)/${out_file}_nui
else
    out_file=$(dirname $out_file)/$(basename $(basename $out_file .gz) .nii)
fi

code_dir=/common/motion/seyhmus/code
#======================================================================



#======================================================================
# design file for global, csf mean, and wm mean signal regression 
#----------------------------------------------------------------------
rm -f nuisanceSignalsRemoved.txt
rm -f design.txt
rm -f constant_linear_quadratic_regressors.txt
if [ $global_signal_regression_yn -eq 1 ] ; then
    if [ ! -z $brain_mask ] ; then
        fslmeants -i $func -o global_signal.txt -m $brain_mask -w
        paste $design_file global_signal.txt > design_temp.txt
        mv design_temp.txt design.txt
        design_file=design.txt
        output_file_suffix=${output_file_suffix}_gs_1
        echo "Global signal regression : YES " >> nuisanceSignalsRemoved.txt
    else
        echo -e "\e[31mERROR: a brain mask is needed for global signal regression\e[0m"
        exit 1
    fi
else
    echo "Global signal regression : NO " >> nuisanceSignalsRemoved.txt
fi

if [ $csf_mean_signal_regression_yn -eq 1 ] ; then
    if [ ! -z $csf_mask ] ; then
        fslmeants -i $func -o csf_mean_signal.txt -m $csf_mask -w
        paste $design_file csf_mean_signal.txt > design_temp.txt
        mv design_temp.txt design.txt
        design_file=design.txt
        echo "CSF mean signal regression : YES " >> nuisanceSignalsRemoved.txt
    else
        echo -e "\e[31mERROR: a csf mask is needed for csf mean signal regression\e[0m"
        exit 1
    fi
else
    echo "CSF mean signal regression : NO " >> nuisanceSignalsRemoved.txt
fi

if [ $wm_mean_signal_regression_yn -eq 1 ] ; then
    if [ ! -z $wm_mask ] ; then
        fslmeants -i $func -o wm_mean_signal.txt -m $wm_mask -w
        paste $design_file wm_mean_signal.txt > design_temp.txt
        mv design_temp.txt design.txt
        design_file=design.txt
        echo "WM mean signal regression : YES " >> nuisanceSignalsRemoved.txt
    else
        echo -e "\e[31mERROR: a white matter mask is needed for wm mean signal regression\e[0m"
        exit 1
    fi
else
    echo "WM mean signal regression : NO " >> nuisanceSignalsRemoved.txt
fi

if [[ $constant_regression_yn -eq 1 ]] && [[ $linear_regression_yn -eq 1 ]] && [[ $quadratic_regression_yn -eq 1 ]] ; then
    echo "Constant signal regression : YES " >> nuisanceSignalsRemoved.txt
    echo "Linear signal regression : YES " >> nuisanceSignalsRemoved.txt
    echo "Quadratic signal regression : YES " >> nuisanceSignalsRemoved.txt
    n_input_data_volumes=`fslval $func dim4 | grep -o "[0-9.]*"`
    python ${code_dir}/createConstantLinearQuadraticRegressors.py $n_input_data_volumes >> constant_linear_quadratic_regressors.txt
    paste $design_file constant_linear_quadratic_regressors.txt > design_temp.txt
    mv design_temp.txt design.txt
    design_file=design.txt
fi

#======================================================================



#======================================================================
# regressing out the nuisance signals with GLM
#----------------------------------------------------------------------
glm_options="--demean --des_norm"
if [ $normalize_yn -eq 1 ] ; then
    glm_options="$glm_options --dat_norm"
fi
fsl_glm -i $func -d $design_file --out_res=${out_file}_res.nii.gz --out_data=${out_file}_data.nii.gz $glm_options

#======================================================================
