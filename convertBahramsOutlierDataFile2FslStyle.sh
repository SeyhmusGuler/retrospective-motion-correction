#! /bin/sh
# Converts Bahram's outlier.dat to FSL style confound file

if [ ! $# -eq 2 ] ; then echo "Usage: $0 <Bahrams_outlier_dat_file> <outliers_fsl_format>. "; exit 1; fi

if [ -s $2 ] ; then echo "File $2 is not empty. Exiting..." ; exit 1 ; fi

runIndex=1
nVolumes=`wc $1 | awk {'print $1'}`

while [ $runIndex -le $nVolumes ] ; do
    echo 0 >> temporaryAllZeroColumnFile
    runIndex=`echo "${runIndex} + 1 " | bc`
done

touch $2
lineIndex=1
while read line ; do 
    if [ ${line} -eq 0 ] ; then
        cp temporaryAllZeroColumnFile newColumn
        sed -i "${lineIndex}s/0/1/" newColumn
        paste $2 newColumn > tmpfile
        cat tmpfile > $2
    fi
    lineIndex=`echo "${lineIndex} + 1 " | bc`
done < $1

rm -f newColumn
rm -f tmpfile
rm -f temporaryAllZeroColumnFile

