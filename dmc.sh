if [ $# -eq 0 ] ; then
    echo "Usage: $0 -in <func> -remove_list <remove_volume_list> -data_type <data_type>"
    exit 1
fi


while [[ $1 != '' ]] ; do
    if [[ $1 == '-in' ]]
    then
        func=$2
        func=$(realpath $(dirname $func))/$(basename $func)
        echo "func: $func"
        shift; shift
    elif [[ $1 == '-remove_list' ]]
    then
        remove_volume_idx=$2
        echo "remove: $remove_volume_idx"
        shift; shift
    elif [[ $1 == '-data_type' ]]
    then
        datatype=$2
        shift;shift
    fi
done
        

if [[ ! -z $remove_volume_idx ]] ; then
    if [[ -z $datatype ]] ; then
        datatype=`fslval $func data_type`
        echo " datatype: $datatype" 
    fi
    func_filename=$(basename $(basename $func .gz) .nii)
    output_dir=$(realpath $(dirname $func))
    outfile=${output_dir}/${func_filename}_dmc.nii.gz
    echo "outfile : ${outfile}"
    echo "/common/motion/code/fMRIMissingDataCompletion/fMRIMissingDataCompletion${datatype} $func ${outfile} $remove_volume_idx"
    /common/motion/code/fMRIMissingDataCompletion/fMRIMissingDataCompletion${datatype} $func ${outfile} $remove_v    olume_idx
else
    fslmaths $func ${outfile}
fi


