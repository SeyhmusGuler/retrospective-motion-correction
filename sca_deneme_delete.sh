#! /bin/sh

seedBasedConnectivityAnalysisUsingFslTools.sh \
    -in /common/motion/seyhmus/data/rsFMRIwithMotion/motionIncludedRestFMRI2/nifti/5_FMRI_RESTING_NOMOTION.nii.gz \
    -out /common/motion/seyhmus/data/sca_rswithmotion_DELETE \
    -seed_mask /common/motion/seyhmus/data/rsFMRIwithMotion/rois_111_115_111and115.nii.gz \
    -anat /common/motion/seyhmus/data/rsFMRIwithMotion/motionIncludedRestFMRI/analysis/data_for_analysis/bestt1w.nrrd \
    -parcellation /common/motion/seyhmus/data/rsFMRIwithMotion/motionIncludedRestFMRI/analysis/common-processed/modules/Parcellation/NVM/ParcellationNVM.nrrd \
    -smoothing 6 \
    -design_file /common/motion/seyhmus/data/tsc/derivatives/controls/sbca/sub-004/ses-01/func/sub-004_ses-01_task-rest_acq-30x30x30/original/design.fsf \
    -wm_mask ~/Desktop/fsl/data/standard/tissuepriors/3mm/avg152T1_white_bin.nii.gz \
    -csf_mask ~/Desktop/fsl/data/standard/tissuepriors/3mm/avg152T1_csf_bin.nii.gz \
    -detrend_clq \
    -slice_timing /common/motion/seyhmus/data/rsFMRIwithMotion/motionIncludedRestFMRI2/derivatives/nomotion_sliceTimingFslStyle.txt \
    -bbr \
    -prefix sca_rswithmotion_delete \
    -reorient2std \
    -use_our_preprocessing

