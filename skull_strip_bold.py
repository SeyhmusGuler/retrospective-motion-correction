
# coding: utf-8

# In[2]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nibabel as nb
import matplotlib.pyplot as plt
import nilearn as nil
import nipype.algorithms.confounds as nac
from nilearn.masking import compute_epi_mask
from nilearn.image import concat_imgs
import nipype.interfaces.afni as afni
from nipype.utils.filemanip import fname_presuffix
import shutil
import numpy as np

from os.path import join as opj
from os.path import abspath
from IPython.display import Image
from nipype.interfaces.base import (
    traits, TraitedSpec, BaseInterfaceInputSpec, File, InputMultiPath, OutputMultiPath,
SimpleInterface)


# In[3]:


__version__ = '0.3.5'

class MaskEPIInputSpec(BaseInterfaceInputSpec):
    in_files = InputMultiPath(File(exists=True), mandatory=True,
                              desc='input EPI or list of files')
    lower_cutoff = traits.Float(0.2, usedefault=True)
    upper_cutoff = traits.Float(0.85, usedefault=True)
    connected = traits.Bool(True, usedefault=True)
    opening = traits.Int(2, usedefault=True)
    exclude_zeros = traits.Bool(False, usedefault=True)
    ensure_finite = traits.Bool(True, usedefault=True)
    target_affine = traits.Either(None, traits.File(exists=True),
                                  default=None, usedefault=True)
    target_shape = traits.Either(None, traits.File(exists=True),
                                 default=None, usedefault=True)
    no_sanitize = traits.Bool(False, usedefault=True)


class MaskEPIOutputSpec(TraitedSpec):
    out_mask = File(exists=True, desc='output mask')


class MaskEPI(SimpleInterface):
    input_spec = MaskEPIInputSpec
    output_spec = MaskEPIOutputSpec

    def _run_interface(self, runtime):
        masknii = compute_epi_mask(
            self.inputs.in_files,
            lower_cutoff=self.inputs.lower_cutoff,
            upper_cutoff=self.inputs.upper_cutoff,
            connected=self.inputs.connected,
            opening=self.inputs.opening,
            exclude_zeros=self.inputs.exclude_zeros,
            ensure_finite=self.inputs.ensure_finite,
            target_affine=self.inputs.target_affine,
            target_shape=self.inputs.target_shape
        )

        if self.inputs.no_sanitize:
            in_file = self.inputs.in_files
            if isinstance(in_file, list):
                in_file = in_file[0]
            nii = nb.load(in_file)
            qform, code = nii.get_qform(coded=True)
            masknii.set_qform(qform, int(code))
            sform, code = nii.get_sform(coded=True)
            masknii.set_sform(sform, int(code))

        self._results['out_mask'] = fname_presuffix(
            self.inputs.in_files[0], suffix='_mask', newpath=runtime.cwd)
        masknii.to_filename(self._results['out_mask'])
        return runtime
    
class CopyXFormInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='the file we get the data from')
    hdr_file = File(exists=True, mandatory=True, desc='the file we get the header from')


class CopyXFormOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='written file path')


class CopyXForm(SimpleInterface):
    """
    Copy the x-form matrices from `hdr_file` to `out_file`.
    """
    input_spec = CopyXFormInputSpec
    output_spec = CopyXFormOutputSpec

    def _run_interface(self, runtime):
        out_name = fname_presuffix(self.inputs.in_file,
                                   suffix='_xform',
                                   newpath=runtime.cwd)
        # Copy and replace header
        shutil.copy(self.inputs.in_file, out_name)
        _copyxform(self.inputs.hdr_file, out_name,
                   message='CopyXForm (niworkflows v%s)' % __version__)
        self._results['out_file'] = out_name
        return runtime
    
def _copyxform(ref_image, out_image, message=None):
    # Read in reference and output
    resampled = nb.load(out_image)
    orig = nb.load(ref_image)
    
    # Copy xform infos
    qform, qform_code = orig.header.get_qform(coded=True)
    sform, sform_code = orig.header.get_sform(coded=True)
    header = resampled.header.copy()
    header.set_qform(qform, int(qform_code))
    header.set_sform(sform, int(sform_code))
    header['descrip'] = 'xform matrices modified by %s.' % (message or '(unknown)')

    newimg = resampled.__class__(resampled.get_data(), orig.affine, header)
    newimg.to_filename(out_image)



# In[7]:


enhance_t2=False
skull_strip_bold = pe.Workflow(name='skull_strip_bold')
skull_strip_bold.base_dir = '/fileserver/motion/seyhmus/code/ssb'

inputnode = pe.Node(util.IdentityInterface(fields=['in_file']),
                    name='inputnode')
inputnode.inputs.in_file='/local/data/tsc/bids/sub-a002/ses-01/func/sub-a002_ses-01_task-rest_bold.nii.gz'

outputnode = pe.Node(util.IdentityInterface(fields=['mask_file', 'skull_stripped_file',
                                                    'bias_corrected_file']),
                     name='outputnode')
n4_mask = pe.Node(MaskEPI(upper_cutoff=0.75, opening=1,
                          no_sanitize=True),
                  name = 'n4_mask')
n4_correct = pe.Node(ants.N4BiasFieldCorrection(dimension=3, copy_header=True),
                    name='n4_correct', n_procs=1)

#Create a generous BET mask out of the bias-corrected EPI
skullstrip_first_pass = pe.Node(fsl.BET(frac=0.2, mask=True),
                                name='skullstrip_first_pass')
bet_dilate = pe.Node(fsl.DilateImage(operation='max', kernel_shape='sphere',
                                     kernel_size=6.0, internal_datatype='char'),
                     name='skullstrip_first_dilate')
bet_mask = pe.Node(fsl.ApplyMask(), name='skullstrip_first_mask')
# Use AFNI's unifize for T2 constrast & fix header
unifize = pe.Node(afni.Unifize(
        t2=True, outputtype='NIFTI_GZ',
        # Default -clfrac is 0.1, 0.4 was too conservative
        # -rbt because I'm a Jedi AFNI Master (see 3dUnifize's documentation)
        args='-clfrac 0.2 -rbt 18.3 65.0 90.0',
        out_file="uni.nii.gz"), name='unifize')
fixhdr_unifize = pe.Node(CopyXForm(), name='fixhdr_unifize', mem_gb=0.1)

# Run ANFI's 3dAutomask to extract a refined brain mask
skullstrip_second_pass = pe.Node(afni.Automask(dilate=1,
                                                   outputtype='NIFTI_GZ'),
                                     name='skullstrip_second_pass')
fixhdr_skullstrip2 = pe.Node(CopyXForm(), name='fixhdr_skullstrip2', mem_gb=0.1)

# Take intersection of both masks
combine_masks = pe.Node(fsl.BinaryMaths(operation='mul'),
                            name='combine_masks')

# Compute masked brain
apply_mask = pe.Node(fsl.ApplyMask(), name='apply_mask')

skull_strip_bold.connect([
        (inputnode, n4_mask, [('in_file', 'in_files')]),
        (inputnode, n4_correct, [('in_file', 'input_image')]),
        (inputnode, fixhdr_unifize, [('in_file', 'hdr_file')]),
        (inputnode, fixhdr_skullstrip2, [('in_file', 'hdr_file')]),
        (n4_mask, n4_correct, [('out_mask', 'mask_image')]),
        (n4_correct, skullstrip_first_pass, [('output_image', 'in_file')]),
        (skullstrip_first_pass, bet_dilate, [('mask_file', 'in_file')]),
        (bet_dilate, bet_mask, [('out_file', 'mask_file')]),
        (skullstrip_first_pass, bet_mask, [('out_file', 'in_file')]),
        (bet_mask, unifize, [('out_file', 'in_file')]),
        (unifize, fixhdr_unifize, [('out_file', 'in_file')]),
        (fixhdr_unifize, skullstrip_second_pass, [('out_file', 'in_file')]),
        (skullstrip_first_pass, combine_masks, [('mask_file', 'in_file')]),
        (skullstrip_second_pass, fixhdr_skullstrip2, [('out_file', 'in_file')]),
        (fixhdr_skullstrip2, combine_masks, [('out_file', 'operand_file')]),
        (fixhdr_unifize, apply_mask, [('out_file', 'in_file')]),
        (combine_masks, apply_mask, [('out_file', 'mask_file')]),
        (combine_masks, outputnode, [('out_file', 'mask_file')]),
        (apply_mask, outputnode, [('out_file', 'skull_stripped_file')]),
        (n4_correct, outputnode, [('output_image', 'bias_corrected_file')]),
])


# In[8]:


skull_strip_bold.write_graph(graph2use='flat')


# In[11]:


Image(filename='/fileserver/motion/seyhmus/code/ssb/skull_strip_bold/graph.png')


# In[12]:


skull_strip_bold.run()
# ants.N4BiasFieldCorrection.help()

