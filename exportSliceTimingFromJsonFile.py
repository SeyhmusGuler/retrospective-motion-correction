# exports and prints the slice timing from a json file
#
# author : burak erem
#
#

import json
import numpy as np
import sys

with open(str(sys.argv[1])) as data_file:
   data = json.load(data_file)
   TR = data['RepetitionTime']
   sliceTiming = np.array(data['SliceTiming'])
   sliceTiming = -sliceTiming/TR + 0.5
   for val in sliceTiming:
       print val
