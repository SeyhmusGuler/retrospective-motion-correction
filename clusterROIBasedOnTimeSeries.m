function [clusters] = clusterROIBasedOnTimeSeries(func_data,roi_mask,k)
% Applies kmeans clustering to divides the ROI into k clusters based on the time series functional data
%
% author : seyhmus guler
% date : 3/8/2017
%
%

% read in the data
func_nii = load_untouch_nii(func_data);
mask_nii = load_untouch_nii(roi_mask);
[~,name,~] = fileparts(roi_mask);
[~,name,~] = fileparts(name);
mask = mask_nii.img;
logical_mask = logical(mask);
funcdata = double(func_nii.img);
tsize = size(funcdata,4);
logical_mask4D = repmat(logical_mask,1,1,1,tsize);
 
roi_time_series1D = funcdata(logical_mask4D);
roi_time_series2D = reshape(roi_time_series1D,numel(roi_time_series1D)/tsize,[]);
% calculate clusters
idx = kmeans(roi_time_series2D,k,'Distance','correlation');
% save the data
mask(mask>0) = idx;
mask_nii.img = mask;
save_untouch_nii(mask_nii, strcat(name,'_clustered'));
