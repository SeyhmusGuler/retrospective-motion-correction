#! /bin/sh
# preprocessing based on the feat design file and cpac default pipeline
#
# author: seyhmus Guler
#

#======================================================================
# check inputs
#----------------------------------------------------------------------

if [ $# -ne 1 ] ; then
    echo "USAGE: $0 <design.fsf>"
    exit 1
else
    cp $1 temp_design.fsf
fi

# create the output directory 'FD' and move the design file there
FD=`cat temp_design.fsf | grep "fmri(outputdir)" | awk {'print $3'} | tr -d '"'`
if [ -z $FD ] ; then 
    FD=`cat temp_design.fsf | grep "feat_files(1)" | awk {'print $3'} | tr -d '"'`
fi
FD=`realpath $FD`
FD=${FD}.feat
if [ -d "$FD" ] ; then 
    echo -e "\e[33mWARNING: The output directory $FD exists, exiting with no preprocessing...\e[0m"
    exit 1
else
    mkdir -p $FD
    mv temp_design.fsf ${FD}/design.fsf
fi

pushd $FD > /dev/null
#======================================================================



#======================================================================
# initialization
#----------------------------------------------------------------------

code_dir=/common/motion/seyhmus/code

data_pointer=`cat design.fsf | grep "feat_files(1)" | awk {'print $3'} | tr -d '"'`
step_counter=0

# copy data and set datatype to float
echo "${FSLDIR}/bin/fslmaths ${data_pointer} ${step_counter}_prefiltered_func_data -odt float"
echo "data_pointer=${step_counter}_prefiltered_func_data"
${FSLDIR}/bin/fslmaths ${data_pointer} ${step_counter}_prefiltered_func_data -odt float
data_pointer=${step_counter}_prefiltered_func_data
step_counter=`echo " ${step_counter} + 1 " | bc`

#delete volumes
number_of_volumes=`cat design.fsf | grep "fmri(npts)" | awk {'print $3'}`
number_of_delete_volumes=`cat design.fsf | grep "fmri(ndelete)" | awk {'print $3'}`
number_of_remaining_volumes=`echo " $number_of_volumes - $number_of_delete_volumes " | bc`

echo "${FSLDIR}/bin/fslroi $data_pointer ${step_counter}_prefiltered_func_data_delv $number_of_delete_volumes $number_of_remaining_volumes"
echo "data_pointer=${step_counter}_prefiltered_func_data_delv"
${FSLDIR}/bin/fslroi $data_pointer ${step_counter}_prefiltered_func_data_delv $number_of_delete_volumes $number_of_remaining_volumes
data_pointer=${step_counter}_prefiltered_func_data_delv
step_counter=`echo " ${step_counter} + 1 " | bc`

#create example functional image (default: first volume after deleting the initial volumes)
# example_functional_volume_index=0
# alternative: middle volume (if uncomment the below line) 
example_functional_volume_index=`echo "$number_of_volumes / 2 " | bc`

# reference volume for registration and (potentially) motion correction
echo "${FSLDIR}/bin/fslroi $data_pointer example_func $example_functional_volume_index 1"
echo "example_functional_volume=example_func"
${FSLDIR}/bin/fslroi $data_pointer example_func $example_functional_volume_index 1
example_functional_volume=example_func

# TR
echo "trSec=`cat design.fsf | grep "fmri(tr)" | awk {'print $3'}`"
trSec=`cat design.fsf | grep "fmri(tr)" | awk {'print $3'}`

if [ `echo " $trSec > 20 " | bc` -eq 1 ] ; then
    echo -e "\e[33mWARNING: TR seems to be greather than 20 seconds. Make sure this is correct. \e[0m"
fi

#======================================================================



#======================================================================
# registration (change later for non-linear registration schemes)
#----------------------------------------------------------------------

flirtOpts=
version=`cat design.fsf | grep "fmri(version)" | awk {'print $3'}`
reghighres_yn=`cat design.fsf | grep "fmri(reghighres_yn)" | awk {'print $3'}`
regstandard_yn=`cat design.fsf | grep "fmri(regstandard_yn)" | awk {'print $3'}`
nonlinear_regstandard_yn=`cat design.fsf | grep "regstandard_nonlinear_yn)" | awk {'print $3'}`

if [ $nonlinear_regstandard_yn -eq 1 ] ; then
    warpres=`cat design.fsf | grep "regstandard_nonlinear_warpres)" | awk {'print $3'}`
    flirtOpts="$flirtOpts -n $warpres"
fi

if [ $reghighres_yn -eq 1 ] ; then
    highresf=`cat design.fsf | grep "highres_files(1)" | awk {'print $3'} | tr -d '"'`
    hrdof=`cat design.fsf | grep "fmri(reghighres_dof)" | awk {'print $3'}`
    hrsearch=`cat design.fsf | grep "fmri(reghighres_search)" | awk {'print $3'}` 
    flirtOpts="$flirtOpts -h $highresf -w $hrdof -x $hrsearch"
fi

if [ $regstandard_yn -eq 1 ] ; then
    standardf=`cat design.fsf | grep "fmri(regstandard)" | awk {'print$3'} | tr -d '"'`
    stdof=`cat design.fsf | grep "fmri(regstandard_dof)" | awk {'print $3'}`
    stsearch=`cat design.fsf | grep "fmri(regstandard_search)" | awk {'print $3'}` 
    flirtOpts="$flirtOpts -s $standardf -y $stdof -z $stsearch"
fi

echo "${FSLDIR}/bin/mainfeatreg -F $version -d ${FD} -i ${FD}/example_func.nii.gz $flirtOpts"
${FSLDIR}/bin/mainfeatreg -F $version -d ${FD} -i ${FD}/example_func.nii.gz $flirtOpts

#======================================================================



#======================================================================
# motion correction
#----------------------------------------------------------------------

moco_yn=`cat design.fsf | grep "fmri(mc)" | awk {'print $3'}`
if [ $moco_yn -eq 1 ] ; then

    echo "${FSLDIR}/bin/mcflirt -in $data_pointer -refvol $example_functional_volume_index -out ${step_counter}_prefiltered_func_data_mcf -mats -plots -rmsrel -rmsabs -spline_final"
    echo "data_pointer=${step_counter}_prefiltered_func_data_mcf"
    ${FSLDIR}/bin/mcflirt -in $data_pointer -refvol $example_functional_volume_index -out ${step_counter}_prefiltered_func_data_mcf -mats -plots -meanvol -rmsrel -rmsabs -spline_final

    /bin/mkdir -p mc
    /bin/mv -f ${step_counter}_prefiltered_func_data_mcf.mat ${step_counter}_prefiltered_func_data_mcf.par ${step_counter}_prefiltered_func_data_mcf_abs.rms ${step_counter}_prefiltered_func_data_mcf_abs_mean.rms ${step_counter}_prefiltered_func_data_mcf_rel.rms ${step_counter}_prefiltered_func_data_mcf_rel_mean.rms mc

    # visualize
    ${FSLDIR}/bin/fsl_tsplot -i mc/${step_counter}_prefiltered_func_data_mcf.par -t 'MCFLIRT estimated rotations (radians)' -u 1 --start=1 --finish=3 -a x,y,z -w 960 -h 216 -o mc/rot.png 
    ${FSLDIR}/bin/fsl_tsplot -i mc/${step_counter}_prefiltered_func_data_mcf.par -t 'MCFLIRT estimated translations (mm)' -u 1 --start=4 --finish=6 -a x,y,z -w 960 -h 216 -o mc/trans.png 
    ${FSLDIR}/bin/fsl_tsplot -i mc/${step_counter}_prefiltered_func_data_mcf_abs.rms,mc/${step_counter}_prefiltered_func_data_mcf_rel.rms -t 'MCFLIRT estimated mean displacement (mm)' -u 1 -w 960 -h 216 -a absolute,relative -o mc/disp.png 
    cp mc/${step_counter}_prefiltered_func_data_mcf.par mc/motion_parameters_original_6.par
    data_pointer=${step_counter}_prefiltered_func_data_mcf
    step_counter=`echo " ${step_counter} + 1 " | bc`
fi

#======================================================================



#======================================================================
# slice timing correction
#----------------------------------------------------------------------

stIdx=`cat design.fsf | grep "fmri(st)" | awk {'print $3'}` # if 0, no slice timing correction
if [ $stIdx -gt 0 ] ; then
    case $stIdx in 
    
        2)
            stOpt='--down'
            ;;
        3)
            stfile=`cat design.fsf | grep "fmri(st_file)" | awk {'print $3'} | tr -d '"'`
            stOpt="--ocustom=$stfile"
            ;;
        4)
            stfile=`cat design.fsf | grep "fmri(st_file)" | awk {'print $3'} | tr -d '"'`
            stOpt="--tcustom=$stfile"
            ;;
        5)
            stOpt='--odd'
            ;;
    esac
    
    echo "${FSLDIR}/bin/slicetimer -i $data_pointer --out=${step_counter}_prefiltered_func_data_st -r ${trSec} $stOpt"
    echo "data_pointer=${step_counter}_prefiltered_func_data_st"
    ${FSLDIR}/bin/slicetimer -i $data_pointer --out=${step_counter}_prefiltered_func_data_st -r ${trSec} $stOpt
    data_pointer=${step_counter}_prefiltered_func_data_st
    step_counter=`echo " ${step_counter} + 1 " | bc`
fi

#======================================================================



#======================================================================
# intensity threshold to create a dilated mask
#----------------------------------------------------------------------

funcdata_unmasked=$data_pointer

# brain mask
alternative_mask=`cat design.fsf | grep "fmri(alternative_mask)" | awk {'print $3'} | tr -d '"'`
bet_yn=`cat design.fsf | grep "fmri(bet_yn)" | awk {'print $3'}`

if [ ! -z $alternative_mask ] ; then 
    echo "${FSLDIR}/bin/fslmaths $data_pointer -mas $alternative_mask ${step_counter}_prefiltered_func_data_altmasked"
    echo "data_pointer=${step_counter}_prefiltered_func_data_altmasked"
    ${FSLDIR}/bin/fslmaths $data_pointer -mas $alternative_mask ${step_counter}_prefiltered_func_data_altmasked
    data_pointer=${step_counter}_prefiltered_func_data_altmasked
    step_counter=`echo " ${step_counter} + 1 " | bc`

elif [ $bet_yn -eq 1 ] ; then # mask based on BET brain extraction
    echo "${FSLDIR}/bin/fslmaths $data_pointer -Tmean mean_func"
    echo "${FSLDIR}/bin/bet2 mean_func mask -f 0.3 -n -m"
    echo "${FSLDIR}/bin/immv mask_mask mask"
    echo "${FSLDIR}/bin/fslmaths $data_pointer -mas mask ${step_counter}_prefiltered_func_data_bet"
    echo "data_pointer=${step_counter}_prefiltered_func_data_bet"
    ${FSLDIR}/bin/fslmaths $data_pointer -Tmean mean_func
    ${FSLDIR}/bin/bet2 mean_func mask -f 0.3 -n -m
    ${FSLDIR}/bin/immv mask_mask mask
    ${FSLDIR}/bin/fslmaths $data_pointer -mas mask ${step_counter}_prefiltered_func_data_bet
    data_pointer=${step_counter}_prefiltered_func_data_bet
    step_counter=`echo " ${step_counter} + 1 " | bc`
fi

#intensity thresholding
perc=`${FSLDIR}/bin/fslstats $data_pointer -p 2 -p 98`
perc2=`echo $perc | awk {'print $1'}`
perc98=`echo $perc | awk {'print $2'}`
brainthr=`cat design.fsf | grep "fmri(brain_thresh)" | awk {'print $3'}`
intensity_threshold=`echo " $perc2 + ( $brainthr * ( $perc98 - $perc2 ) / 100.0 ) " | bc -l`

echo "perc2=$perc2 ; perc98=$perc98 ; intensity_threshold=$intensity_threshold"

if [ $brainthr -gt 0 ] ; then # intensity based thresholding
    echo "${FSLDIR}/bin/fslmaths $data_pointer -thr ${intensity_threshold} -Tmin -bin mask -odt char"
    echo "median_intensity=`${FSLDIR}/bin/fslstats $funcdata_unmasked -k mask -p 50`"
    echo "${FSLDIR}/bin/fslmaths mask -dilF mask"
    echo "${FSLDIR}/bin/fslmaths $funcdata_unmasked -mas mask ${step_counter}_prefiltered_func_data_thresh"
    echo "data_pointer=${step_counter}_prefiltered_func_data_thresh"
    ${FSLDIR}/bin/fslmaths $data_pointer -thr ${intensity_threshold} -Tmin -bin mask -odt char
    median_intensity=`${FSLDIR}/bin/fslstats $funcdata_unmasked -k mask -p 50`
    ${FSLDIR}/bin/fslmaths mask -dilF mask
    ${FSLDIR}/bin/fslmaths $funcdata_unmasked -mas mask ${step_counter}_prefiltered_func_data_thresh
    data_pointer=${step_counter}_prefiltered_func_data_thresh
    step_counter=`echo " ${step_counter} + 1 " | bc`

else
    echo "${FSLDIR}/bin/fslmaths example_func -mul 0 -add 1 -odt char"
    echo "median_intensity=`${FSLDIR}/bin/fslstats $data_pointer -p 90`"
    ${FSLDIR}/bin/fslmaths example_func -mul 0 -add 1 mask -odt char
    median_intensity=`${FSLDIR}/bin/fslstats $data_pointer -p 90`
fi

#======================================================================



#======================================================================
# spatial smoothing
#----------------------------------------------------------------------

echo "fwhm=`cat design.fsf | grep "fmri(smooth)" | awk {'print $3'}`"
fwhm=`cat design.fsf | grep "fmri(smooth)" | awk {'print $3'}`
smooThr=0.01

if [ 1 -eq "$(echo "${fwhm} > ${smooThr}" | bc)" ] ; then
    smoothSigma=`echo " $fwhm / 2.355 " | bc -l`
    susanInt=`echo " $median_intensity * 3 / 4 - $perc2 * 3 / 4 " | bc -l` 
    echo "smoothSigma= $smoothSigma and susanInt=$susanInt"
    echo "${FSLDIR}/bin/fslmaths $data_pointer -Tmean mean_func"
    echo "${FSLDIR}/bin/susan $data_pointer $susanInt $smoothSigma 3 1 1 mean_func $susanInt ${step_counter}_prefiltered_func_data_smooth"
    echo "${FSLDIR}/bin/fslmaths ${step_counter}_prefiltered_func_data_smooth -mas mask ${step_counter}_prefiltered_func_data_smooth"
    echo "data_pointer=${step_counter}_prefiltered_func_data_smooth"
    
    ${FSLDIR}/bin/fslmaths $data_pointer -Tmean mean_func
    ${FSLDIR}/bin/susan $data_pointer $susanInt $smoothSigma 3 1 1 mean_func $susanInt ${step_counter}_prefiltered_func_data_smooth
    ${FSLDIR}/bin/fslmaths ${step_counter}_prefiltered_func_data_smooth -mas mask ${step_counter}_prefiltered_func_data_smooth
    data_pointer=${step_counter}_prefiltered_func_data_smooth
    step_counter=`echo " ${step_counter} + 1 " | bc`
fi

#======================================================================


# intesity normalization, perfusion subtraction (not applied)


#======================================================================
# temporal filtering
#----------------------------------------------------------------------

hpfilter_yn=`cat design.fsf | grep "fmri(temphp_yn)" | awk {'print $3'}`
lpfilter_yn=`cat design.fsf | grep "fmri(templp_yn)" | awk {'print $3'}`


if [ $hpfilter_yn -eq 1 ] || [ $lpfilter_yn -eq 1 ] ; then 
    restoreMeanCmd=
    hpSigmaVol=-1
    if [ $hpfilter_yn -eq 1 ] ; then
        hpPeriodSec=`cat design.fsf | grep "fmri(paradigm_hp)" | awk {'print $3'}`
        hpSigmaSec=`echo " $hpPeriodSec / 2 " | bc -l`
        hpSigmaVol=`echo " $hpSigmaSec / ${trSec} " | bc -l`
        ${FSLDIR}/bin/fslmaths $data_pointer -Tmean tempMean
        restoreMeanCmd="-add tempMean"
    fi
    lpSigmaVol=-1 #do not perform low-pass filtering
    if [ $lpfilter_yn -eq 1 ] ; then
        #   lpSigmaSec=2.8 (default value of FSL) 
        lpPeriodSec=10  # corresponds to 0.1 Hz cutoff freq (to match default CPAC lowpass cutoff)
        lpSigmaSec=`echo " $lpPeriodSec / 18 " | bc -l`
        lpSigmaVol=`echo " ${lpSigmaSec} / ${trSec} " | bc -l`
    fi

    # temporal filtering
    echo "Low pass filter cutoff (in volumes) : $lpSigmaVol"
    echo "High pass filter cutoff (in volumes) : $hpSigmaVol"
    echo "${FSLDIR}/bin/fslmaths $data_pointer -bptf $hpSigmaVol $lpSigmaVol $restoreMeanCmd ${step_counter}_prefiltered_func_data_tempfilt"
    ${FSLDIR}/bin/fslmaths $data_pointer -bptf $hpSigmaVol $lpSigmaVol $restoreMeanCmd ${step_counter}_prefiltered_func_data_tempfilt_fsl
    python ${code_dir}/bandpass_nii.py ${data_pointer}.nii.gz ${trSec} ${step_counter}_prefiltered_func_data_tempfilt_python_script.nii.gz
    if [ $hpfilter_yn -eq 1 ] ; then 
        echo "${FSLDIR}/bin/imrm tempMean"
        ${FSLDIR}/bin/imrm tempMean
    fi
    data_pointer=${step_counter}_prefiltered_func_data_tempfilt_python_script
    step_counter=`echo " ${step_counter} + 1 " | bc`
fi

#======================================================================



#======================================================================
# finalize
#----------------------------------------------------------------------

echo "${FSLDIR}/bin/fslmaths $data_pointer filtered_func_data"
${FSLDIR}/bin/fslmaths $data_pointer filtered_func_data
${FSLDIR}/bin/fslmaths filtered_func_data -Tmean mean_func
popd > /dev/null
#======================================================================
