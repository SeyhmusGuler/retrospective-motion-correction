#! /bin/sh
# Change the design file for FEAT according to the options provided.

# Written by : Seyhmus Guler
# Last edit  : 1/4/2016 by Seyhmus

#=====================================================================
# Check the syntax
#---------------------------------------------------------------------

if [ $# -lt 1 ] ; then
    echo "Usage: $0 [options] <design_file>"
    echo "Options:
    -a (analysis stages) : 0 (no first level, group stats only), 7 (full first-level), 1 (pre), 2 (stats)
    -c (confound files) : <motion_confound_file>
    -d (delete volumes) : <# delete volumes>
    -f (alternative reference image) : <alternative_functional_reference_image_file>
    -h (high resolution image) : <structural_image_file>
    -i (input fmri data file) : <input_data_file>
    -w (progress watcher, browser) : 0 (off), 1 (on)
    -m (mcflirt motion correction) : 0 (off), 1 (on)
    -o (output directory) : <output_directory_name>
    -r (standard image) : <standard_atlas_image>
    -s (slice timing correction) : 0 none, 1 up, 2 down, 5 interleaved, 3 and 4 use files
    -t (slice timing file) : <slice_timing_file> 
    -z (alternative brain mask) : <alternative_brain_mask_file>"
    exit 1
elif [ $# -eq 1 ] && [ -e $1 ] ; then
    echo "No options given. No changes made to the file, exiting..."
    exit 1
else
    for design ; do true ; done
    if [ ! -s $design ] ; then
        echo "The design file $design doesn't exist or is empty. Nothing to do, exiting..."
        exit 2
    else
        length=$(($#-1))
        array=${@:1:$length}
        #echo "Changing design file ${design}, with options: $array"
    fi
fi

#=====================================================================


#=====================================================================
# Copy the design file and make changes
#---------------------------------------------------------------------

while getopts ":a:c:d:f:h:i:w:m:o:r:s:t:z:" OPT; do

    # option -$OPT with parameter $OPTARG is triggered 
    case $OPT in
        
        a)
            # which analysis stages to run 
            if [ $OPTARG -eq 0 ] || [ $OPTARG -eq 1 ] ||  [ $OPTARG -eq 2 ] ||  [ $OPTARG -eq 7 ] ; then 
                echo "-${OPT} (analysis stages to run) : ${OPTARG}"
                sed -i "s@set fmri(analysis).*@set fmri(analysis) ${OPTARG}@g" ${design}
            else
                echo "WARNING: -${OPT} can be called with argmuments 0, 1, 2, and 7 only. No changes made."
            fi
            ;;

        c)
            # confound EVs file   
            echo "-${OPT} (confound EV file) : ${OPTARG}"
            OPTARG=`realpath $OPTARG`
            if [ ! -s ${OPTARG} ] ; then
                echo "${OPTARG} doesn't exist or is empty. No confound EVs are set." 
                sed -i "s@ set fmri(confoundevs).*@set fmri(confoundevs) 0@g" ${design}
            else
                        sed -i "s@set fmri(confoundevs).*@set fmri(confoundevs) 1@g" ${design}
                        sed -i '/set confoundev_files/d' ${design}
                        sed -i "/set fmri(confoundevs).*/a\set confoundev_files(1) \"${OPTARG}\"" ${design}
            fi
            ;;
        
        d)
            # Number of volumes to delete at the very beginning 
            echo "-${OPT} (# volumes to delete) : ${OPTARG}"
            sed -i "s@set fmri(ndelete).*@set fmri(ndelete) ${OPTARG}@g" ${design}
            ;;
        f)
            # alternative reference image (default is middle image) 
            if [ ! -s ${OPTARG} ] ; then
                echo "Not found: exam_func file ${OPTARG}. Exiting..."
                exit 1
            fi
            OPTARG=`realpath ${OPTARG}`
            echo "-${OPT} (alternative reference func image (exam_func)) : ${OPTARG}"
            sed -i "s@set fmri(alternateReference_yn).*@set fmri(alternateReference_yn) 1@g" ${design}
            sed -i "/set alt_ex_func.*/d" ${design}
            sed -i "/set fmri(confoundevs).*/a\set alt_ex_func(1) \"${OPTARG}\"" ${design}
            ;;
        
        h)
            #high resolution image
            if [ ! -s ${OPTARG} ] ; then 
                echo "Not found: structural file ${OPTARG}. Exiting..."
                exit 1
            fi
            OPTARG=`realpath ${OPTARG}`
            echo "-${OPT} (high resolution image) : ${OPTARG}"
            sed -i "s@set fmri(reghighres_yn).*@set fmri(reghighres_yn) 1@g" ${design}
            sed -i "/set highres_files.*/d" ${design}
            sed -i "/set fmri(reghighres_yn).*/a\set highres_files(1) \"${OPTARG}\"" ${design}
            ;;

        i)
            # the input data    
            if [ ! -s ${OPTARG} ] ; then 
                echo "Not found: data file ${OPTARG}. Exiting..."
                exit 1
            fi
            
            # total number of data files
            sed -i "s@set fmri(multiple).*@set fmri(multiple) 1@g" ${design}
            OPTARG=`realpath $OPTARG`
            dfn=`basename ${OPTARG} .gz`
            dfn=`basename ${dfn} .nii`
            dfnP=`dirname ${OPTARG}`
            n_input_data_volumes=`fslval ${OPTARG} dim4 | grep -o "[0-9.]*"` 
            repetition_time=`fslval ${OPTARG} pixdim4 | grep -o "[0-9.]*"`
            echo "-${OPT} (input functional data): ${OPTARG} (npts: $n_input_data_volumes, tr: $repetition_time)"
            
            sed -i "s@set feat_files(1).*@set feat_files(1) \"${dfnP}\/${dfn}\"@g" ${design}
            sed -i "s@set fmri(tr).*@set fmri(tr) $repetition_time@g" ${design} 
            sed -i "s@set fmri(npts).*@set fmri(npts) $n_input_data_volumes@g" ${design} 
            ;;

        w)
            # Progress watcher. Toggle the browser on (1) and off (0)
            if [ $OPTARG -eq 0 ] || [ $OPTARG -eq 1 ] ; then 
                echo "-${OPT} (progress watcher) : ${OPTARG}"
                sed -i "s/set fmri(featwatcher_yn).*/set fmri(featwatcher_yn) ${OPTARG}/g" ${design}
                echo "Option -$OPT (feat watcher) is set to ${OPTARG}."
            fi
            ;;

        m)
            # Toggle MCFLIRT motion correction on and off
            echo "-${OPT} (motion correction) : ${OPTARG}"
            sed -i "s/set fmri(mc).*/set fmri(mc) ${OPTARG}/g" ${design}
            ;;

        o)
            # Output directory   
            echo "-${OPT} (output directory) : ${OPTARG}"
            OUTPUTDIR=`realpath $OPTARG`
            sed -i "s@set fmri(outputdir).*@set fmri(outputdir) \"${OUTPUTDIR}\"@" ${design}
            ;;
        
        r)
            #standard image
            if [ ! -s ${OPTARG} ] ; then 
                echo "Not found: standard image file ${OPTARG}. Exiting..."
                exit 1
            fi
            OPTARG=`realpath ${OPTARG}`
            echo "-${OPT} (standard image) : ${OPTARG}"
            sed -i "s@set fmri(regstandard_yn).*@set fmri(regstandard_yn) 1@g" ${design}
            sed -i "s@set fmri(regstandard).*@set fmri(regstandard) \"${OPTARG}\"@g" ${design}
            #sed -i "s@set fmri(regstandard_dof).*@set fmri(regstandard_dof) 12@g" ${design}
            #sed -i "s@set fmri(regstandard_search).*@set fmri(regstandard_search) 90@g" ${design}
            #sed -i "s@set fmri(regstandard_nonlinear_yn).*@set fmri(regstandard_nonlinear_yn) 1@g" ${design}
            ;;

        s)
            # slice timing (0:none, 1: up, 2: down, 5: interleaved, 3 and 4: use files)
            if [ $OPTARG -eq 0 ] || [ $OPTARG -eq 1 ] || [ $OPTARG -eq 2 ] || [ $OPTARG -eq 3 ] || [ $OPTARG -eq 4 ] || [ $OPTARG -eq 5 ] ; then
                echo "-${OPT} (slice timing correction) : ${OPTARG}"
                sed -i "s@set fmri(st).*@set fmri(st) ${OPTARG}@g" ${design}
            fi
            ;;

        t)
            # slice timing (or order file)
            stfile=$OPTARG
            if [ ! -f $stfile ] ; then
                echo "Not found: slice timing file $stfile. Exiting..."
                exit 1
            fi
            sed -i "s@set fmri(st_file).*@set fmri(st_file) \"${stfile}\"@g" ${design}
            ;;

        z)
            # alternative brain mask
            sed -i "s@set fmri(alternative_mask).*@set fmri(alternative_mask) \"$OPTARG\"@g" ${design}
            ;;
            
        \?)
            # unrecognized option, give error and exit.    
            echo "-$OPTARG : Invalid option. Exiting..."
            exit 1
            ;;
    esac
done
#=====================================================================

