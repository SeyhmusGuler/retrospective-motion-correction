#! /bin/sh

# Creates a random 4x4 affine transformation matrix within given range and saves it as a matlab file

if $# -eq 2
    range=$1
    nmat=$2
fi

if $# -eq 1
    range=$1
    nmat=1
fi

matlab -nodesktop -nosplash -r "a=rand(4); save('a','a.mat');"
