
# coding: utf-8

# # Two Sample T-Test for Lesion Network Analysis Notebook

# ### Authors: Alexander Cohen and Louis Soussand - Version 1.72 06/19/2018
# Changelog:
# 
# 1.0 Initial Release
# 
# 1.1 Now able to receive dataset info from XNAT Gate and access the data by streaming XNAT backup on the Millenium Falcon.
# 
# 1.2 Cleaned up cells and connectome.sh function call  
# 1.2.1 need to copy in fslview call:  
# 1.2.2 Fixed the Subject List option
# 
# 1.3 Added nimlab module  
# 1.31 Fixed input/output for concatenated map.
# 
# 1.4 Added nimlab software module
# 
# 1.5 Added auto_resample and dtype to image concatenation
# 
# 1.6 Changes made to corrects the color map to align with the thresholding
# 
# 1.7 Changed source of dependencies
# 
# 1.71 Added nimlab module
# 
# 1.72 Changed graphic interface location

# #### This notebook is intendend to be run cell by cell to conduct lesion network analysis on a single set of lesions. Cells that contain #??? at the begininng have settings that you can/should modify.
# 
# Using this notebook, you can:
# 
# 1) Load necessary packages
# 
# 2) Select the input and output directories
# (This notebook expects the input to have subdirectories of "Lesions" and "Functional_Connectivity" with the connectome.sh output in the latter.)
# 
# 3) Perform a Two-Sample T-Test using FSL's PALM
# 
# 4) Look at the results and zoom in on specific ROIs
# 
# 5) For interactive analysis, I provide the appropriate commands to run fslview or fsleyes with the:
# - MNI brain
# - T map
# - FWE corrected P value
# - Uncorrected P value
# - any ROIs of interest

# In[ ]:


## Packages and environmental settings:

##Packages:
import sys
import subprocess
import string
import os
import glob
import math
import numpy as np
import hdf5storage as hdf
import scipy as sp
import pandas as pd
import nilearn
import nibabel as nb
from nilearn import plotting, image, masking, regions

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib.image as mpimg

import multiprocessing

from time import time
from random import random

## nimlab package is hosted internally on the M Falcon
from nimlab.functions import *
from nimlab.software import *

## Environment:
get_ipython().magic(u"config InlineBackend.figure_format = 'retina'")
get_ipython().magic(u'matplotlib inline')


# ## Where are the lesion and functional connectivity files and where do you want your output?

# In[ ]:


#??? Inputs Required:

# ------------------------- XNAT_Gate Datasets ----------------------
XNAT_file_1 = 'Prosopagnosia.csv'
XNAT_file_2 = 'Corbetta_Lesions.csv'

if XNAT_file_1:
    dataset_name_1 = XNAT_file_1.replace(".csv", "")
    path = "XNAT Dataset"

    # Target Dataset:
    Dataset = dataset_name_1

    # List of Specific Subject IDs you want to examine (set to '' if you want everyone):
    # Subject_List='/home/acohen8/projects/PA/notebook_analysis/Prosopagnosia_Cases_output_directory/ROIs/Prosopagnosia_Cases_FC_Peaks_T9_43of44_ROI0_exclusion_list.txt'
    Subject_List = ''
if XNAT_file_2:
    dataset_name_2 = XNAT_file_2.replace(".csv", "")
    control_path = "XNAT Dataset"

    # Control Dataset:
    Control_Dataset = dataset_name_2

    # List of Specific Subject IDs you want to examine (set to '' if you want everyone):
    # Control_Subject_List='/home/acohen8/projects/PA/notebook_analysis/Prosopagnosia_Cases_output_directory/ROIs/Prosopagnosia_Cases_FC_Peaks_T9_43of44_ROI0_exclusion_list.txt'
    Control_Subject_List = ''

# ------------------------- XNAT_Gate Datasets ----------------------




# ------------------------- Local Datasets Group 1 ------------------
# Set this to the location of your target data, with sub-folders of: "Lesions" and "Functional_Connectivity":

if not XNAT_file_1:
    path = "/home/acohen8/projects/PA"

    # Target Dataset:
    Dataset = 'Prosopagnosia'

    # List of Specific Subject IDs you want to examine (set to '' if you want everyone):
    # Subject_List='/home/acohen8/projects/PA/notebook_analysis/Prosopagnosia_Cases_output_directory/ROIs/Prosopagnosia_Cases_FC_Peaks_T9_43of44_ROI0_exclusion_list.txt'
    Subject_List = ''
# ------------------------- Local Datasets Group 1 ------------------

# ------------------------- Local Datasets Group 2 ------------------
# Set this to the location of your control data, with sub-folders of: "Lesions" and "Functional_Connectivity":

if not XNAT_file_2:
    control_path = "/home/acohen8/projects/Network_Localization/Corbetta"

    # Control Dataset:
    Control_Dataset = 'Corbetta'

    # List of Specific Subject IDs you want to examine (set to '' if you want everyone):
    # Control_Subject_List='/home/acohen8/projects/PA/notebook_analysis/Prosopagnosia_Cases_output_directory/ROIs/Prosopagnosia_Cases_FC_Peaks_T9_43of44_ROI0_exclusion_list.txt'
    Control_Subject_List = ''
# ------------------------- Local Datasets Group 2 ------------------



# ------------------------- Common Settings -------------------------
# Set this to where you would like the outputs to be written (the output below will be put into a sub-directory here)
output_folder = "/data2/jupyter/notebooks/lsoussan/forge/proving_grounds/"

# Specific ROIs I am interested in:
ROI_folder = '/data2/jupyter/notebooks/lsoussan/forge/proving_grounds/Prosopagnosia_PA_nonFFA_Patients_output/ROIs/'
ROIs = [
    ROI_folder+'Prosopagnosia_PA_nonFFA_Patients_FC_Peaks_T10_18of44_ROI5.nii.gz',
    ROI_folder+'Prosopagnosia_PA_nonFFA_Patients_FC_Peaks_T10_18of44_ROI0.nii.gz'
]

# Target (this is a unique label to distinguish this analysis from another analysis)
Target = 'PA_vs_Corbetta'
# ------------------------- Common Settings -------------------------


# ### Print Selections:

# In[ ]:


print 'The Target dataset label is: '+ Dataset
print 'The Target input directory is: '+ path
print ""
print 'The Control dataset label is: '+ Control_Dataset
print 'The Control input directory is: '+ control_path
print ""
print 'The output directory being used is: ' + output_folder
print ""
print 'The contrast we are going to study is: '+ Target


# In[ ]:


## Output control:

indir=path+'/'
outdir=output_folder+'/'+Dataset+'_'+Target+'_output/'
    
if not os.path.exists(outdir):
    #create the output directory
    os.makedirs(outdir)
   
    #Within the output directory, 4 subfolders are created:
if not os.path.exists(outdir+'PALM_2Sample_TTest/'):
    os.makedirs(outdir+'PALM_2Sample_TTest/')
    #output images
if not os.path.exists(outdir+'images/'):
    os.makedirs(outdir+'images/')
    #final ROIs (not lesions)
if not os.path.exists(outdir+'ROIs/'):
    os.makedirs(outdir+'ROIs/')
    #Analyses: empty subfolder for subsequent analyses
if not os.path.exists(outdir+'Analyses/'):
    os.makedirs(outdir+'Analyses/')
    #Data: for storing 4D .nii files for PALM
if not os.path.exists(outdir+'Data/'):
    os.makedirs(outdir+'Data/')


# ### Generate Lists of Files for the Chosen Datasets:

# In[ ]:


# ------------------------- For the Target group -------------------------:

# Get IDs

if not XNAT_file_1:
    
    TF = sorted(glob.glob(indir+'Lesions/'+"*gz"))
    lcs=longest_common_suffix(TF)

    Target_IDs=[]
    for i in range(0, len(TF)):
        sub=os.path.basename(TF[i][0:-len(lcs)])
        Target_IDs.append(sub)

    # Selection of Specific Subjects
    if Subject_List:
        f = open(Subject_List, 'r')
        Target_IDs = f.read().splitlines()
    N_of_images=len(Target_IDs)

    # Get Lesion, fc_T Files, and fc_Fz Files
    Target_files = [glob.glob(indir+'Lesions/'+str(entry)+"*gz")[0] for entry in Target_IDs]
    Target_fc_files = [glob.glob(indir+'Functional_Connectivity/'+str(entry)+"*_func_seed_T.nii.gz")[0] for entry in Target_IDs]
    Target_fc_Fz_files = [glob.glob(indir+'Functional_Connectivity/'+str(entry)+"*_func_seed_AvgR_Fz.nii.gz")[0] for entry in Target_IDs]
else:
    #Read csv file in panda data frame format:
    pd_df_1=pd.read_csv(XNAT_file_1)

    Target_IDs=pd_df_1['subject']

    # Selection of Specific Subjects
    if Subject_List:
        f = open(Subject_List, 'r')
        Target_IDs = f.read().splitlines()
        pd_df_1 = pd_df_1.loc[pd_df_1['subject'].isin(Target_IDs)]
        pd_df_1.index = pd.RangeIndex(len(pd_df_1.index))
    N_of_images=len(Target_IDs)

    # Get Lesion, fc_T Files, and fc_Fz Files
    Target_files = pd_df_1['Lesion']
    Target_fc_files = pd_df_1['func_T']
    Target_fc_Fz_files = pd_df_1['AvgR_Fz']


# ------------------------- For the Control group -------------------------:
if not XNAT_file_2:
    # Get IDs
    TF = sorted(glob.glob(control_path+'/'+'Lesions/'+"*gz"))
    lcs=longest_common_suffix(TF)

    Control_IDs=[]
    for i in range(0, len(TF)):
        sub=os.path.basename(TF[i][0:-len(lcs)])
        Control_IDs.append(sub)

    # Selection of Specific Subjects
    if Control_Subject_List:
        f = open(Control_Subject_List, 'r')
        Control_IDs = f.read().splitlines()
    N_of_control_images=len(Control_IDs)

    # Get Lesion, fc_T Files, and fc_Fz Files
    Control_files = [glob.glob(control_path+'/'+'Lesions/'+str(entry)+"*gz")[0] for entry in Control_IDs]
    Control_fc_files = [glob.glob(control_path+'/'+'Functional_Connectivity/'+str(entry)+"*_func_seed_T.nii.gz")[0] for entry in Control_IDs]
    Control_fc_Fz_files = [glob.glob(control_path+'/'+'Functional_Connectivity/'+str(entry)+"*_func_seed_AvgR_Fz.nii.gz")[0] for entry in Control_IDs]
else:
    pd_df_2=pd.read_csv(XNAT_file_2)

    Control_IDs=pd_df_2['subject']

    # Selection of Specific Subjects
    if Control_Subject_List:
        f = open(Control_Subject_List, 'r')
        Target_IDs = f.read().splitlines()
        pd_df_2 = pd_df_2.loc[pd_df_2['subject'].isin(Control_IDs)]
        pd_df_2.index = pd.RangeIndex(len(pd_df_2.index))
    N_of_control_images=len(Control_IDs)

    # Get Lesion, fc_T Files, and fc_Fz Files
    Control_files = pd_df_2['Lesion']
    Control_fc_files = pd_df_2['func_T']
    Control_fc_Fz_files = pd_df_2['AvgR_Fz']
    


# In[ ]:


# Write text files for each list of target and controls

# ------------------------- For the Target group -------------------------:
Target_lesion_filelist = open(outdir+'PALM_2Sample_TTest/'+Dataset+'_lesion_files.txt','w+')
for entry in Target_files:
    print>>Target_lesion_filelist,entry
Target_lesion_filelist.close()

Target_fc_filelist = open(outdir+'PALM_2Sample_TTest/'+Dataset+'_fcT_files.txt','w+')
for entry in Target_fc_files:
    print>>Target_fc_filelist,entry
Target_fc_filelist.close()

Target_fc_Fz_filelist = open(outdir+'PALM_2Sample_TTest/'+Dataset+'_Fz_files.txt','w+')
for entry in Target_fc_Fz_files:
    print>>Target_fc_Fz_filelist,entry
Target_fc_Fz_filelist.close()



# ------------------------- For the Control group -------------------------:
Control_lesion_filelist = open(outdir+'PALM_2Sample_TTest/'+Control_Dataset+'_lesion_files.txt','w+')
for entry in Control_files:
    print>>Control_lesion_filelist,entry
Control_lesion_filelist.close()

Control_fc_filelist = open(outdir+'PALM_2Sample_TTest/'+Control_Dataset+'_fcT_files.txt','w+')
for entry in Control_fc_files:
    print>>Control_fc_filelist,entry
Control_fc_filelist.close()

Control_fc_Fz_filelist = open(outdir+'PALM_2Sample_TTest/'+Control_Dataset+'_Fz_files.txt','w+')
for entry in Control_fc_Fz_files:
    print>>Control_fc_Fz_filelist,entry
Control_fc_Fz_filelist.close()


# ## Prepare data for PALM

# In[ ]:


source_dir=outdir+'Data'
target_dir=outdir+'PALM_2Sample_TTest'

# NOTE: THIS STEP MAY TAKE 1-2 MINUTES
# PALM requests all subjects be provided as a single 4D.nii image
image.concat_imgs([Target_fc_Fz_files, Control_fc_Fz_files]).to_filename(source_dir+'/'+Target+'_Fz_maps.nii')

# Create Design matrix for the two groups
design_matrix=np.zeros((len(Target_fc_Fz_files)+len(Control_fc_Fz_files),2))
design_matrix[0:len(Target_fc_Fz_files),0]=1
design_matrix[-len(Control_fc_Fz_files):,1]=1

design_matrix_file=target_dir+'/'+Target+'.mat'
np.savetxt(design_matrix_file, design_matrix, delimiter=" ",fmt='%1d')
insert(design_matrix_file,"/Matrix\n")
insert(design_matrix_file,"/PPheights 1 1\n")
insert(design_matrix_file,"/NumPoints "+str(len(Target_fc_Fz_files)+len(Control_fc_Fz_files))+"\n")
insert(design_matrix_file,"/NumWaves 2\n")

# Show me the design matrix
plt.imshow(design_matrix, interpolation='nearest', aspect='auto')
plt.colorbar()
plt.show()

# Create Contrasts
contrast_matrix=np.zeros((2,2))
contrast_matrix[0,0]=1
contrast_matrix[1,0]=-1
contrast_matrix[0,1]=-1
contrast_matrix[1,1]=1

contrast_matrix_file=target_dir+'/'+Target+'.con'
np.savetxt(contrast_matrix_file, contrast_matrix, delimiter=" ",fmt='%1d')
insert(contrast_matrix_file,"/Matrix\n")
insert(contrast_matrix_file,"/PPheights 1 1\n")
insert(contrast_matrix_file,"/NumContrasts 2\n")
insert(contrast_matrix_file,"/NumWaves 2\n")


# In[ ]:


# Run PALM

read_input=source_dir+'/'+Target+'_Fz_maps.nii'
design_matrix_file=design_matrix_file
contrast_matrix_file=contrast_matrix_file
output_directory=target_dir+'/'+Target+'_palm'

# call_palm
call_palm(read_input, design_matrix_file, contrast_matrix_file=contrast_matrix_file, output_directory=output_directory, mode='t-test',iterations=2000)


# In[ ]:


# View the results
tmap=target_dir+'/'+Target+'_palm'+'_vox_tstat_c1.nii'
uncorrected_p_pos=target_dir+'/'+Target+'_palm'+'_vox_tstat_uncp_c1.nii'
fwe_p_pos=target_dir+'/'+Target+'_palm'+'_vox_tstat_fwep_c1.nii'

uncorrected_p_neg=target_dir+'/'+Target+'_palm'+'_vox_tstat_uncp_c2.nii'
fwe_p_neg=target_dir+'/'+Target+'_palm'+'_vox_tstat_fwep_c2.nii'


img_threshold=0.001
which_p='FWE'
# which_p='uncorrected'
if which_p=='FWE':
    p_map_pos=fwe_p_pos
    p_map_neg=fwe_p_neg
else:
    p_map_pos=uncorrected_p_pos
    p_map_neg=uncorrected_p_neg

tmap_thres_pos=image.math_img('tmap*(img<'+str(img_threshold)+')',tmap=tmap,img=p_map_pos)
tmap_thres_neg=image.math_img('tmap*(img<'+str(img_threshold)+')',tmap=tmap,img=p_map_neg)
tmap_thres=image.math_img('img1+img2',img1=tmap_thres_pos,img2=tmap_thres_neg)

tmap_thres_image=tmap_thres.get_data()
posthres=np.min(tmap_thres_image[tmap_thres_image>0])/np.max(tmap_thres_image[tmap_thres_image>0])
negthres=np.max(tmap_thres_image[tmap_thres_image<0])/np.min(tmap_thres_image[tmap_thres_image<0])

fast_glass_brain_plot(tmap,plot_abs=False,colorbar=True,display_mode='lyrz',title=Target+' T-map full value',cmap=hotBlues(),output_file=outdir+"images/"+Target+"_PALM_2sample_T-map_glass_full.png")
fast_glass_brain_plot(tmap_thres,plot_abs=False,colorbar=True,display_mode='lyrz',title=Target+' T-map '+which_p+' p<'+str(img_threshold),cmap=hotBlues(posthres,negthres),output_file=outdir+"images/"+Target+"_PALM_2sample_T-map_glass_corrected.png")

fidl_plot2(tmap,title_to_use=Target+' T-map full value',filename_to_use=outdir+"images/"+Target+"_PALM_2sample_T-map_fidl_full.png",cmap=hotBlues())
fidl_plot2(tmap_thres,title_to_use=Target+' T-map '+which_p+' p<'+str(img_threshold),cmap=hotBlues(posthres,negthres),filename_to_use=outdir+"images/"+Target+"_PALM_2sample_T-map_fidl_corrected.png")

print "The smallest positive value corresponding to a p<"+str(img_threshold)+" was "+str(posthres)
print "The smallest negative value corresponding to a p<"+str(img_threshold)+" was "+str(negthres)

if ROIs:
    print "Show me the values at the locations of the ROIs I am interested in:"
    for ROI in ROIs:
        coords = plotting.find_xyz_cut_coords(ROI)
        display = plotting.plot_stat_map(tmap_thres,cut_coords=coords,title='Tmap at ROI'+os.path.basename(ROI[:-7])+' '+which_p+' p<'+str(img_threshold),threshold='auto')
        display.add_contours(ROI, levels=[.5],colors='r')

# For now, switch to fsleyes/fslview to interact with the data:
print "Copy one of these commands to view these files locally:"
print ""
print "fslview "+uncorrected_p_pos+fwe_p_pos+tmap+" "+" ".join(ROIs)
print "fslview "+uncorrected_p_neg+fwe_p_neg+tmap+" "+" ".join(ROIs)
print ""
print "fsleyes --standard "+uncorrected_p_pos+fwe_p_pos+tmap+" "+" ".join(ROIs)
print "fsleyes --standard "+uncorrected_p_neg+fwe_p_neg+tmap+" "+" ".join(ROIs)

