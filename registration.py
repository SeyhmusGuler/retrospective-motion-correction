
# coding: utf-8

# In[3]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nibabel as nib
import matplotlib.pyplot as plt
import nilearn as nil

from os.path import join as opj
from os.path import abspath
from IPython.display import Image


# In[4]:


template4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0.nii.gz'
template_probability_map4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumProbabilityMask.nii.gz'
template_extraction_mask4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumRegistrationMask.nii.gz'

output_dir = '/common/motion/seyhmus/code/registration'
template = '/local/fsl/data/standard/MNI152_T1_3mm_brain.nii.gz'
anatomical_image = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/anat/bestt1w.nii.gz'


# In[5]:


inputnode=pe.Node(util.IdentityInterface(fields=['ref_bold_brain','t1_brain',
                                                 'hmc_xforms','subject_id','t1_2_func_xform']),
                 name='inputnode')

outputnode=pe.Node(util.IdentityInterface(fields=['itk_bold_2_t1','itk_t1_2_bold','out_report',
                                                 'bold_t1','bold_mask_t1']),
                  name='outputnode')

bbr_reg=pe.Node(fsl.FLIRT,name='bbr_reg')
bbr_reg.inputs.reference='' #t1 brain image
bbr_reg.inputs.in_file='' #reference bold image
bbr_reg.inputs.cost='bbr'

reorient = pe.Node(fsl.Reorient2Std(), name = 'reorient2std')
reorient.inputs.in_file = anatomical_image

brain_extract = pe.Node(ants.BrainExtraction(), name='brain_extract')
brain_extract.inputs.brain_probability_mask =template_probability_map4bet
brain_extract.inputs.brain_template = template4bet
brain_extract.inputs.extraction_registration_mask = template_extraction_mask4bet
brain_extract.inputs.out_prefix='highres'
brain_extract.inputs.num_threads=12

antsreg = pe.Node(ants.Registration(args='--float',
                            collapse_output_transforms=True,
                            fixed_image=template,
                            initial_moving_transform_com=True,
                            num_threads=12,
                            output_inverse_warped_image=True,
                            output_warped_image=True,
                            sigma_units=['vox']*3,
                            transforms=['Rigid', 'Affine', 'SyN'],
                            terminal_output='file',
                            winsorize_lower_quantile=0.005,
                            winsorize_upper_quantile=0.995,
                            convergence_threshold=[1e-06],
                            convergence_window_size=[10],
                            metric=['MI', 'MI', 'CC'],
                            metric_weight=[1.0]*3,
                            number_of_iterations=[[1000, 500, 250, 100],
                                                  [1000, 500, 250, 100],
                                                  [100, 70, 50, 20]],
                            radius_or_number_of_bins=[32, 32, 4],
                            sampling_percentage=[0.25, 0.25, 1],
                            sampling_strategy=['Regular',
                                               'Regular',
                                               'None'],
                            shrink_factors=[[8, 4, 2, 1]]*3,
                            smoothing_sigmas=[[3, 2, 1, 0]]*3,
                            transform_parameters=[(0.1,),
                                                  (0.1,),
                                                  (0.1, 3.0, 0.0)],
                            use_histogram_matching=True,
                            write_composite_transform=True),
               name='antsreg')


# In[13]:


registration_wf = pe.Workflow(name='registration_wf')
registration_wf.connect([
    (inputnode, bbr_reg,[('ref_bold_brain','')])])


# In[20]:




