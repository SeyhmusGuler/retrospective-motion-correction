#! /bin/sh
# Merges nifti format fmri volumes in a given directory and reorients the resulting 4D image to FSL standard space

# Written by Seyhmus Guler
# Reviewed by (Enter your name)

if [ $# -eq 0 ] ; then 
    echo "Usage: $0 <directory_of_3d_volumes>"
    echo "Usage: $0 <directory_of_3d_volumes> [tr value in seconds]"
    exit 1
elif [ $# -ge 2 ] ; then 
    TR=$2
fi  

# if TR is unset, set it to 1
if [ -z ${TR+x} ] ; then 
    TR=1
fi

DATADIR=$1

if [ ! -d $DATADIR ] ; then echo "The directory $DATADIR doesn't exist, exiting... " ; exit 2 ; fi 

#check for consistent filenames.
CWD=$(pwd)

#Entering the directory $DATADIR
cd $DATADIR

#Check if the data volumes are 3-digit or 4-digit indexed 
nVolumes3=`ls *[0123456789][0123456789][0123456789]*nii* | wc | awk {'print $1'}`
nVolumes4=`ls *[0123456789][0123456789][0123456789][0123456789]*nii* | wc | awk {'print $1'}`
#Find the files' naming convention
if [ $nVolumes3 -gt $nVolumes4 ] ; then
    mergedVolumes=`echo *[0123456789][0123456789][0123456789]*nii*`
    exampleFile=`echo *[0123456789][0123456789][0123456789]*nii* | awk {'print $1'}`
    nVolumes=$nVolumes3
elif [ $nVolumes4 -ge $nVolumes3 ] ; then
    mergedVolumes=`echo *[0123456789][0123456789][0123456789][0123456789]*nii*`
    exampleFile=`echo *[0123456789][0123456789][0123456789][0123456789]*nii* | awk {'print $1'}`
    nVolumes=$nVolumes4
else
    echo "Automatic merging is not done. Merge volumes manually."
    exit 3 
fi

exampleFileID=`echo "$exampleFile" | grep -oP '\d+(?=\.)'`
baseName=`echo $exampleFile | awk -F "$exampleFileID" {'print $1'}`
outputName=${baseName}4D
if [ -s ${outputName}* ] ; then 
    echo "Merged 4D data already exists, skipping."
else
    echo "Merging ${nVolumes} volumes: ${mergedVolumes} ---> into $outputName."
    ${FSLDIR}/bin/fslmerge -tr $outputName $mergedVolumes $TR
fi
echo "$outputName"
${FSLDIR}/bin/fslreorient2std $outputName ${outputName}_oriented2std

#Leaving the directory $DATADIR
cd $CWD
