#! /bin/sh

nskip=0

while [[ $1 != '' ]] ; do
    if [[ $1 == '-in' ]]
    then
        infile=$2
        shift; shift
    elif [[ $1 == '-skip' ]]
    then
        nskip=$2
        shift; shift
    fi
done

fname=`basename $infile .tsv`
bdir=`dirname $infile`
nline=`cat $infile | wc -l`
nlinem1=`echo " $nline - 1 - $nskip " | bc`

cat $infile | tail -n $nlinem1 | sed 's~n/a~0~g' > ${bdir}/${fname}.txt
awk '!($3="")' ${bdir}/${fname}.txt > ${bdir}/${fname}_woGs.txt
