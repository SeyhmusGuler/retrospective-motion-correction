#! /bin/sh
# Dynamic missing data completion and seed connectivity analysis of rsfMRI, with preprocessing
# performed by fmriprep-docker
#
# author: seyhmus guler
# date: 10/02/2018
#
#

#======================================================================
# inputs and data completion
#----------------------------------------------------------------------

if [ $# -lt 2 ] ; then
    echo "Usage: $0 <bids_data> <output_directory>"
    exit 1
fi

data_dir=$1
mkdir -p $2
output_dir=`realpath $2`
mkdir -p ${output_dir}/temporary_files
pushd ${output_dir}/temporary_files

find $1 -type d -name "sub*" | while read line; do echo `realpath $line` ; done > ${output_dir}/subject_list.txt
participant_label=
while read line; do
    subject_id=`basename $line`
    mkdir -p ${output_dir}/${subject_id}
    participant_label="$participant_label ${subject_id#sub-}"
    find $line -name "*_bold.nii.gz" | while read linee; do echo `realpath $linee`; done > ${output_dir}/${subject_id}/func_list.txt
    while read linee; do
        func=$linee
        func_filename=$(basename $(basename $func .gz) .nii)
        mkdir -p ${output_dir}/${subject_id}/func/${func_filename}
        temp_out_dir=${output_dir}/${subject_id}/func/${func_filename}
        func_parent_dir=`dirname $func`
        json_filename=${func_parentdir}/${func_filename}.json

        # motion outliers
        mkdir -p ${temp_out_dir}/motion_outliers

        # delete dummy scans?
        n_delete_volumes=0
        n_input_data_volumes=`fslval $func dim4`
        n_input_data_volumes=`echo " $n_input_data_volumes - $n_delete_volumes " | bc`

        # motion outliers (FD > 0.5)
        ${FSLDIR}/bin/fsl_motion_outliers -i $func -o ${temp_out_dir}/motion_outliers/${func_filename}_motion_confounders --fd --thresh=0.5 -v --nocleanup -t ${temp_out_dir}/motion_outliers --dummy=${n_delete_volumes} -p ${temp_out_dir}/motion_outliers/fd.png -s ${temp_out_dir}/motion_outliers/fd.txt | tee ${temp_out_dir}/motion_outliers/log.txt


        outlier_idx=`cat ${temp_out_dir}/motion_outliers/log.txt | grep "Found spikes at" | sed -n -e 's/^.*at//p' | sed 's/^[ \t]*//'`

        if [[ -z "${outlier_idx// }" ]] ; then
            echo "Volumes with FD > 0.5 : NONE"
            echo "Remove volume list for DMC : NONE"
            touch ${temp_out_dir}/motion_outliers/high_motion_volume_list.txt
            touch ${temp_out_dir}/motion_outliers/remove_volume_list.txt
        else
            remove_volume_idx=`echo ${outlier_idx} | sed -e 's/\s\+/\n/g' | while read in; do echo $in; echo $(($in-1)); echo $(($in+1)); echo $(($in+2)); done | sort -n | uniq | while read in; do if [ $in -ge 0 ] && [ $in -lt $n_input_data_volumes ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`
            echo ${outlier_idx} > ${temp_out_dir}/motion_outliers/high_motion_volume_list.txt
            echo $remove_volume_idx > ${temp_out_dir}/remove_volume_list.txt
            echo "Volumes with FD > 0.5 : $outlier_idx"
            echo "Remove volume list for DMC : $remove_volume_idx"
        fi

        # motion parameters
        original_six_parameters=`find ${temp_out_dir}/motion_outliers/ -name "fmri_mcf.par"`
        cp $original_six_parameters ${temp_out_dir}/motion_outliers/motion_parameters_original_6.par
        ${FSLDIR}/bin/mp_diffpow.sh ${original_six_parameters} ${temp_out_dir}/motion_outliers/motion_parameters_extended_18
        paste -d ' ' ${original_six_parameters} ${temp_out_dir}/motion_outliers/motion_parameters_extended_18.dat > ${temp_out_dir}/motion_outliers/motion_parameters_all_24.par
        motion_parameters=${temp_out_dir}/motion_outliers/motion_parameters_all_24.par

        # motion corrected fmri data
        motion_corrected=`find ${temp_out_dir}/motion_outliers/ -name "fmri_mcf.nii.gz"`
        $FSLDIR/bin/fslmaths $motion_corrected ${temp_out_dir}/${func_filename}_mcf.nii.gz
        func=${temp_out_dir}/${func_filename}_mcf.nii.gz
        rm -r ${temp_out_dir}/motion_outliers/*_mc

        # dmc (dynamic missing data completion)
        if [[ ! -z $remove_volume_idx ]] ; then
            datatype=`fslval $func data_type`
            func_filename=$(basename $(basename $func .gz) .nii)
            outfile=${temp_out_dir}/${func_filename}_dmc.nii.gz
            #/fileserver/motion/code/fMRIMissingDataCompletion/fMRIMissingDataCompletion${datatype} $func ${outfile} $remove_volume_idx
            func_dmc=${outfile}
           
            # copy dmc files to bids folder
            dmc_file_name=$(basename $(basename $func_filename _bold_mcf) _rec-ori)
            $FSLDIR/bin/fslmaths $func_dmc ${func_parent_dir}/${dmc_file_name}_rec-dmc_bold.nii.gz
            cp ${func_parent_dir}/$json_filename ${func_parent_dir}/${dmc_file_name}_rec-dmc_bold.json
        fi

    done < ${output_dir}/${subject_id}/func_list.txt
done < ${output_dir}/subject_list.txt

#======================================================================



#======================================================================
# preprocessing using fmri-prep docker
#----------------------------------------------------------------------
fmriprep-docker $data_dir /local/data/fmriprep_motion participant --participant_label $participant_label --nthreads 32 --omp-nthreads 8 --use-syn-sdc --use-aroma --ignore-aroma-denoising-errors --force-bbr --write-graph
#======================================================================


#======================================================================
# preparing preprocessed data for further processing 
#----------------------------------------------------------------------

find /local/data/fmriprep_motion/fmriprep -name "*bold_space-MNI152NLin2009cAsym_preproc.nii.gz" | while read line; do
func=$line
func_basename=$(basename $func .nii.gz)
aroma_func=$(basename $func _preproc.nii.gz)_variant-smoothAROMAnonaggr_preproc.nii.gz
aroma_basename=$(basename $aroma_func .nii.gz)
func_dir=`dirname $func`
conf_file=$(basename $func _space-MNI152NLin2009cAsym_preproc.nii.gz)_confounds.tsv
cat $line | tail -n $numtp | awk {'print $1 "\t" $2 "\t" $7 "\t" $14 "\t" $15 "\t" $16 "\t" $17 "\t" $18 "\t" $19 "\t" $26 "\t" $27 "\t" $28 "\t" $29 "\t" $30 "\t" $31 "\t" $32 "\t" $33 "\t" $34 "\t" $35 "\t" $36 '} > $output_dir/temp_confound_file.txt
conf_file=$output_dir/temp_confound_file.txt
#======================================================================


#======================================================================
# nuisance signal regression
#----------------------------------------------------------------------
fsl_glm -i $func -d $conf_file --out_res=${output_dir}/${func_basename}_res.nii.gz --out_data=${output_dir}/${func_basename}_data.nii.gz --demean --des_norm --dat_norm
fsl_glm -i $aroma_func -d $conf_file --out_res=${output_dir}/${aroma_basename}_res.nii.gz --out_data=${output_dir}/${aroma_basename}_data.nii.gz --demean --des_norm --dat_norm
func=${output_dir}/${func_basename}_res.nii.gz
aroma_func=${output_dir}/${func_basename}_res.nii.gz
#======================================================================




#======================================================================
# temporal filtering 
#----------------------------------------------------------------------
tr_in_seconds=`fslval $func pixdim4`
func_basename=$(basename $(basename $func .gz) .nii)
aroma_basename=$(basename $aroma_func .nii.gz)
python ${code_dir}/bandpass_nii.py $func $tr_in_seconds ${output_dir}/${func_basename}_tempfilt_python_script.nii.gz
func=${output_dir}/${func_basename}_tempfilt_python_script.nii.gz
python ${code_dir}/bandpass_nii.py $aroma_func $tr_in_seconds ${output_dir}/${aroma_basename}_tempfilt_python_script.nii.gz
aroma_func=${output_dir}/${aroma_basename}_tempfilt_python_script.nii.gz
#======================================================================



#======================================================================
# scrubbing
#----------------------------------------------------------------------

if [ ! -z $remove_volume_indices_file ] ; then
    func_basename=$(basename $(basename $func .gz) .nii)
    aroma_basename=$(basename $aroma_func .nii.gz)
    cp $remove_volume_indices_file ${output_dir}/remove_volumes_list.txt
    echo " Remove volume list for scrubbing : " 
    ${code_dir}/scrubBasedOnRemoveVolumeList.sh $func ${output_dir}/remove_volumes_list.txt
    func=${output_dir}/${func_basename}_scrubbed.nii.gz
    ${code_dir}/scrubBasedOnRemoveVolumeList.sh $aroma_func ${output_dir}/remove_volumes_list.txt
    aroma_func=${output_dir}/${aroma_basename}_scrubbed.nii.gz
fi

#======================================================================



#======================================================================
# roi-based functional connectivity
#----------------------------------------------------------------------

num_of_seed_ROIs=`fslval $seed_mask_stan dim4`
echo "Number of seed regions: $num_of_seed_ROIs"
seed_roi_idx=0
mkdir -p ${output_dir}/seed_rois
${FSLDIR}/bin/fslsplit $seed_mask_stan ${output_dir}/seed_rois/seed_roi_
for seed_roi in ${output_dir}/seed_rois/seed_roi_* ; do
    ${code_dir}/computePearsonsCorrelationCoefficient.sh $func $seed_roi
    seed_roi_basename=$(basename $(basename $seed_roi .gz) .nii)
    mv ${seed_roi_basename}_corr.nii.gz ${output_dir}/${outfile}_${seed_roi_basename}.nii.gz
    mv ${seed_roi_basename}_mts.txt ${output_dir}/${outfile}_${seed_roi_basename}_mts.txt
done

# merge correlation maps
${FSLDIR}/bin/fslmerge -t ${output_dir}/${outfile}.nii.gz ${output_dir}/${outfile}_*.nii.gz
paste ${output_dir}/${outfile}_*_mts.txt > ${output_dir}/${outfile}.txt
rm -f ${output_dir}/${outfile}_*

# mask correlation maps
if [[ ! -z $brain_mask_stan ]] ; then
    ${FSLDIR}/bin/fslmaths ${output_dir}/${outfile}.nii.gz -mas ${output_dir}/brain_mask_stan.nii.gz ${output_dir}/${outfile}.nii.gz
fi


# fisher z transform
${code_dir}/applyFisherTransformUsingFslTools.sh ${output_dir}/${outfile}.nii.gz ${output_dir}/${outfile}_fisherz.nii.gz

# smoothing
if [[ ${smoothing_yn} -eq 1 ]] ; then
    smoothing_sigma=`echo " $smoothing_fwhm / 2.355 " | bc -l`
    ${FSLDIR}/bin/fslmaths ${output_dir}/${outfile}_fisherz.nii.gz -s $smoothing_sigma ${output_dir}/${outfile}_fisherz_fwhm-${smoothing_fwhm}.nii.gz
fi
#======================================================================
done
#======================================================================
popd
