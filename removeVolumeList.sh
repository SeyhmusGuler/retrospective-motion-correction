if [[ $# -eq 0 ]] ; then
    echo "Usage : $0 -in <metric_file> -thr <threshold value> -skip <n_nonsteady_volumes>"
    exit 1
fi
n_skip=0
while [[ $1 != '' ]] ; do
    if [[ $1 == '-in' ]] 
    then
        fd_file=$2
        shift; shift
    elif [[ $1 == '-thr' ]]
    then
        thr=$2
        shift; shift
    elif [[ $1 == '-skip' ]]
    then
        n_skip=$2
        shift; shift
    fi
done

output_dir=`realpath $(dirname $fd_file)`
filename=`basename $fd_file .txt`
n_input_data_volumes=`cat $fd_file | wc -l`
outlier_idx=''
n=0
while read line ; do
    if [[ $n > 0 ]] ; then
        aa=`echo $line | awk {' printf("%.4f",$1)'}` 
        if [[ $aa > $thr ]] ; then
            outlier_idx="$outlier_idx $n"
        fi
    fi
    n=`echo "$n+1" | bc`
done < $fd_file


if [[ -z "${outlier_idx// }" ]] ; then
    echo "Volumes with FD > 0.5 : NONE"
    echo "Remove volume list for DMC : NONE"
    #echo "Number of initial non-steady volumes: $n_skip"
    touch ${output_dir}/${filename}_high_motion_volume_list.txt
    touch ${output_dir}/${filename}_remove_volume_list.txt
else
    #remove_volume_idx=`echo ${outlier_idx} | sed -e 's/\s\+/\n/g' | while read in; do echo $(($in-$n_skip)); echo $(($in-1-$n_skip)); echo $(($in+1-$n_skip)); echo $(($in+2-$n_skip)); done | sort -n | uniq | while read in; do if [ $in -ge 0 ] && [ $in -lt $(($n_input_data_volumes-$n_skip)) ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`
    remove_volume_idx=`echo ${outlier_idx} | sed -e 's/\s\+/\n/g' | while read in; do echo $in; echo $(($in-1)); echo $(($in+1)); echo $(($in+2)); done | sort -n | uniq | while read in; do if [ $in -ge 0 ] && [ $in -lt $n_input_data_volumes ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`
    echo ${outlier_idx} > ${output_dir}/${filename}_high_motion_volume_list.txt
    echo $remove_volume_idx > ${output_dir}/${filename}_remove_volume_list.txt
    echo "Volumes with FD > 0.5 : $outlier_idx"
    echo "Remove volume list for DMC : $remove_volume_idx"
fi

