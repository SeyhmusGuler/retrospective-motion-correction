#! /bin/sh
# Create a custom square wave regressor (Explanatory Variable, EV) for FSL
# 
# Written by Seyhmus Guler 1/17/2017
# Reviewed by 

# Notes: 
#   1. The output file could be used in the FEAT as an input EV square-wave to be later convolved with an HRF
#   2. Example: Assume we want to create a regressor for left finger tap, which starts immediately as the experiment starts, is on for 24 seconds and off for 48 seconds. The experiment contains 4 blocks of left finger tap and stops after 4 minutes (whichever comes first):
#       $createSquareWaveRegressor4Fsl.sh 48 24 48 240 4 left.txt
#   If we delete two initial volumes and the TR was 3 seconds, Regressor needs to be shifted (phase = 48+2*3 = 54)
#   $createSquareWaveRegressor4Fsl.sh 48 24 54 240 4 left.txt 

if [ $# != 6 ] ; then
    echo "Usage: $0 <off_duration> <on_duration> <phase> <stop_after> <num_of_blocks> <output_file_name>"
    exit 1
else
    filename=$6
    if [ -s ${filename} ] ; then
        echo "The file ${filename} is not empty, exiting..."
        exit 1
    fi

    echo "Creating a square wave  with following parameters..."
    echo "Off: $1, on: $2, phase: $3, stopafter: $4 seconds."
    echo "The intensity of each block is fixed (for now)."
    
    period=`echo $1 + $2 | bc -l`
    if [ $3 -ge 0 ] ; then
        phase=`echo $3 % ${period} | bc`
    else
        phase=$3
    fi
    remainder=`echo $4 % $period | bc`
    echo "period: ${period} , number of lines: ${nLines} , remainder: ${remainder}"

    duration=`echo "$period - $phase" | bc`
    echo "duration: $duration"
    if [ $duration -le $2 ] ; then
        onset=0
    else
        onset=`echo "$duration - $2" | bc -l`
    fi
    duration=`echo "$duration - $onset" | bc -l`
    intensity=1

    echo -e "${onset}\t${duration}\t${intensity}" >> ${filename}; 
    onset=`echo "$onset + $duration + $1" | bc -l`
    duration=$2
    nblocks=1
    while [ $onset -lt $4 ] && [ $nblocks -lt $5 ] ; do
        if [ `echo "$onset + $duration" | bc -l` -gt $4 ] ; then
            duration=`echo "$4 - $onset" | bc -l`
        fi
        echo -e "${onset}\t${duration}\t${intensity}" >> ${filename}
        onset=`echo "${onset} + ${period}" | bc` 
        nblocks=`echo "$nblocks + 1 " | bc` 
    done
fi
