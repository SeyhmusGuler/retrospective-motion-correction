#! /bin/sh
# Cleans up the HTML log of FSL-FEAT to report only the important steps in the preprocessing

filename=`basename $1 .html`
rp=`realpath $1`
FEATDIR=`dirname ${rp}`
FEATP=`dirname ${FEATDIR}`
FSLDIR=`echo ${FSLDIR}`
FSLP=`dirname ${FSLDIR}`

temp=`echo ${FEATDIR}/${filename}_tmp.html`

sed "s@${FSLDIR}@\$\{FSLDIR\}@g" $1 > $temp

#Delete sdtout for Melodic
sed -i '/Melodic Version/,/finished/d' $temp

#Delete sdtout for Film prewhitening
sed -i '/film_gls/,/smoothest/{//!d}' $temp

sed -i "s@${FEATDIR}@\$\{FD\}@g" $temp
sed -i "s@${FEATP}@{FP}@g" $temp
sed -i "s@${FSLP}@\$\{FSLP\}@g" $temp
#sed -i "s@<\/pre><br>.*<br><pre>@@g" $temp
sed -i '/<\/pre><hr>.*<br><pre>/d' $temp
sed -i '/^\s*$/d' $temp

sed -i '/<HTML><HEAD>/,/Feat main script/d' $temp
sed -i '/mainfeatreg/,/mkdir/{//!d}' $temp
sed -i '/HTML/d' $temp
sed -i '/fsl_sub/d' $temp
sed -i '/Total original volumes/d' $temp
sed -i "/Deleting.*volume/d" $temp
sed -i '/did not find file/d' $temp

#Delete the lines that start with a number
sed -i -e '/^[0-9]/d' $temp
