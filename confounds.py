
# coding: utf-8

# In[3]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nipype.interfaces.afni as afni
import nibabel as nb
import matplotlib.pyplot as plt
import nilearn as nil
import numpy as np
import nipype.interfaces.io as nio
import os
import nipype.algorithms.confounds as nac
from nilearn.masking import compute_epi_mask
from nilearn.image import concat_imgs
from nilearn import image as nli
from nilearn.image import index_img
import tempfile
from __future__ import print_function

import nibabel as nb
import os.path
import numpy as np
import scipy
from scipy import signal
import sys

import warnings as _warnings
import os as _os
from nipype.utils.filemanip import load_json, save_json, split_filename, fname_presuffix
from os.path import join as opj
from os.path import abspath
from IPython.display import Image
from nipype.interfaces.base import (
    traits, TraitedSpec, isdefined, CommandLineInputSpec, CommandLine, BaseInterfaceInputSpec, 
    File, InputMultiPath, OutputMultiPath, SimpleInterface, BaseInterface)

