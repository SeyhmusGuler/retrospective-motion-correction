
# [0.01,0.1] Bandpass filtering of fMRI based on scipy
# author : burak erem

# usage: bandpass_nii.py <input> <TR> <output>
# usage (not implemented): bandpass_nii.py <input> <TR> <output>

import nibabel as nb
import numpy as np
import scipy
from scipy import signal

import sys

filepath = str(sys.argv[1])
samplingperiod = float(sys.argv[2])
outfilepath = str(sys.argv[3])

passHz = np.array([0.01,0.1])
stopHz = np.array([0.008,0.11])

gpass = 1
gstop = 40

img = nb.load(filepath)

samplingfreq = 1.0/samplingperiod
wp = passHz/(samplingfreq/2)
ws = stopHz/(samplingfreq/2)

sos = scipy.signal.iirdesign(wp, ws, gpass, gstop, output="sos")

filtered_data = scipy.signal.sosfiltfilt(sos, img.get_data(), axis=3, padtype="even", padlen=int(0.5*img.shape[3]))

new_img = nb.Nifti1Image(filtered_data, img.affine, img.header)
nb.save(new_img, outfilepath)
