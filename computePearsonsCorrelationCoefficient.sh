#! /bin/sh
# Calculate Pearson's correlation coefficient between averaged time series of a seed ROI 
# (given with a mask or a coordinate) and fMRI data
# 
# Author: Seyhmus Guler
#
#

#======================================================================
# check the inputs
if [ $# -lt 2 ] || [ $# -gt 5 ] ; then 
    echo "Usage: $0 <fmri_data> <seed_mask>"
    echo "Usage: $0 <fmri_data> <seed_mask> <brain_mask>"
    echo "Usage: $0 <fmri_data> <seed_coordinate>"
    echo "Usage: $0 <fmri_data> <seed_coordinate> <brain_mask>"
    exit 1
fi

DATA=$1
BRAINMASK=

#======================================================================
# extract average time series of the seed ROI

if [ $# -eq 2 ] ; then
    
    SEEDMASK=$2
    ${FSLDIR}/bin/fslmeants -i ${DATA} -o meanTimeSeriesOfSeedROI.txt -m $SEEDMASK
fi

if [ $# -eq 4 ] ; then

    SEEDCOORDINATE=$2 $3 $4
    ${FSLDIR}/bin/fslmeants -i ${DATA} -o meanTimeSeriesOfSeedROI.txt -c $SEEDCOORDINATE
fi

if [ $# -eq 3 ] ; then
    
    BRAINMASK="-m $3"
    SEEDMASK=$2
    ${FSLDIR}/bin/fslmeants -i ${DATA} -o meanTimeSeriesOfSeedROI.txt -m $SEEDMASK
fi

if [ $# -eq 5 ] ; then

    BRAINMASK="-m $5"
    SEEDCOORDINATE="$2 $3 $4"
    echo "seed coordinate: $SEEDCOORDINATE"
    ${FSLDIR}/bin/fslmeants -i ${DATA} -o meanTimeSeriesOfSeedROI.txt -c $SEEDCOORDINATE
fi
#----------------------------------------------------------------------


#======================================================================
# calculate correlation 

$FSLDIR/bin/fsl_glm -i $DATA -d meanTimeSeriesOfSeedROI.txt -o PearCorrCoef $BRAINMASK --des_norm --dat_norm --demean 
seedName=`basename $2 .gz`
seedName=`basename $seedName .nii`
mv meanTimeSeriesOfSeedROI.txt ${seedName}_mts.txt
mv PearCorrCoef.nii.gz ${seedName}_corr.nii.gz
#----------------------------------------------------------------------
