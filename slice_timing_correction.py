
# coding: utf-8

# In[8]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nibabel as nb
import matplotlib.pyplot as plt
import nilearn as nil
import nipype.algorithms.confounds as nac
from nilearn.masking import compute_epi_mask
from nilearn.image import concat_imgs
import nipype.interfaces.afni as afni
from nipype.utils.filemanip import fname_presuffix
import shutil
import numpy as np

import os
import json
from pathlib2 import Path
from os.path import join as opj
from os.path import abspath
from IPython.display import Image
from nipype.interfaces.base import (
    traits, TraitedSpec, BaseInterfaceInputSpec, File, InputMultiPath, OutputMultiPath,
SimpleInterface)

DEFAULT_MEMORY_MIN_GB = 0.01


# In[3]:


class CopyXFormInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='the file we get the data from')
    hdr_file = File(exists=True, mandatory=True, desc='the file we get the header from')


class CopyXFormOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='written file path')


class CopyXForm(SimpleInterface):
    """
    Copy the x-form matrices from `hdr_file` to `out_file`.
    """
    input_spec = CopyXFormInputSpec
    output_spec = CopyXFormOutputSpec

    def _run_interface(self, runtime):
        out_name = fname_presuffix(self.inputs.in_file,
                                   suffix='_xform',
                                   newpath=runtime.cwd)
        # Copy and replace header
        shutil.copy(self.inputs.in_file, out_name)
        _copyxform(self.inputs.hdr_file, out_name,
                   message='CopyXForm (niworkflows v%s)' % __version__)
        self._results['out_file'] = out_name
        return runtime
    
def _copyxform(ref_image, out_image, message=None):
    # Read in reference and output
    resampled = nb.load(out_image)
    orig = nb.load(ref_image)
    
    # Copy xform infos
    qform, qform_code = orig.header.get_qform(coded=True)
    sform, sform_code = orig.header.get_sform(coded=True)
    header = resampled.header.copy()
    header.set_qform(qform, int(qform_code))
    header.set_sform(sform, int(sform_code))
    header['descrip'] = 'xform matrices modified by %s.' % (message or '(unknown)')

    newimg = resampled.__class__(resampled.get_data(), orig.affine, header)
    newimg.to_filename(out_image)



# In[4]:


slice_timing_correction = pe.Workflow(name='slice_timing_correction')

inputnode = pe.Node(util.IdentityInterface(fields=['bold_file', 'skip_vols',
                                                   'slice_timing_file', 'TR']),
                   name='inputnode')
inputnode.inputs.bold_file='/common/motion/seyhmus/code/ali.nii.gz'
inputnode.inputs.slice_timing_file='/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/sliceTimingGeneral.txt'
inputnode.inputs.TR=3

outputnode = pe.Node(util.IdentityInterface(fields=['stc_file']),
                    name='outputnode')
slicet = pe.Node(fsl.SliceTimer(), 
                name='slice_timer')

slice_timing_correction.connect([
    (inputnode, slicet, [('bold_file', 'in_file'),
                        ('slice_timing_file', 'custom_timings'),
                        ('TR', 'time_repetition')]),
    (slicet, outputnode, [('slice_time_corrected_file', 'stc_file')]),
])


# In[5]:


slice_timing_correction.write_graph(graph2use='flat')


# In[24]:


Image(filename='/common/motion/seyhmus/code/graph.png')


# In[25]:


slice_timing_correction.run()

