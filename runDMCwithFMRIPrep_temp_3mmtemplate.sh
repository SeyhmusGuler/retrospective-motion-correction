#! /bin/sh
# Dynamic missing data completion and seed connectivity analysis of rsfMRI, with preprocessing
# performed by fmriprep-docker
#
# author: seyhmus guler e-mail: seyhmus.guler@childrens.harvard.edu
# date: 10/02/2018
#
#

#======================================================================
# inputs and data completion
#----------------------------------------------------------------------

if [ $# -lt 2 ] ; then
    echo "Usage: $0 <bids_data> <output_directory>"
    exit 1
fi

# default settings (change)
code_dir=/fileserver/motion/seyhmus/code
seed_rois=/fileserver/motion/seyhmus/data/dosenbash_rois/Dosenbach_160_rois_3mm_uthr.nii.gz
pcc_roi=/fileserver/motion/seyhmus/data/dosenbash_rois/Dosenbach_PCC_roi_idx115.nii.gz

# inputs
data_dir=$1
output_dir=`realpath $2`
output_dir2=/fileserver/motion/seyhmus/data/rsFMRIwithMotion/bbids_out

participant_label=
while read line; do
    subject_id=`basename $line`
    participant_label="$participant_label ${subject_id#sub-}"
    while read linee; do
        func=$linee
        func_filename=$(basename $(basename $func .gz) .nii)
        temp_out_dir=${output_dir2}/${subject_id}/func/${func_filename}
        func_parent_dir=`dirname $func`
        json_filename=${func_parentdir}/${func_filename}.json

        n_delete_volumes=0
        n_input_data_volumes=`fslval $func dim4`
        n_input_data_volumes=`echo " $n_input_data_volumes - $n_delete_volumes " | bc`
        outlier_idx=`cat ${temp_out_dir}/motion_outliers/log.txt | grep "Found spikes at" | sed -n -e 's/^.*at//p' | sed 's/^[ \t]*//'`

        if [[ -z "${outlier_idx// }" ]] ; then
            echo "Volumes with FD > 0.5 : NONE"
            echo "Remove volume list for DMC : NONE"
        else
            remove_volume_idx=`echo ${outlier_idx} | sed -e 's/\s\+/\n/g' | while read in; do echo $in; echo $(($in-1)); echo $(($in+1)); echo $(($in+2)); done | sort -n | uniq | while read in; do if [ $in -ge 0 ] && [ $in -lt $n_input_data_volumes ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`
            echo "Volumes with FD > 0.5 : $outlier_idx"
            echo "Remove volume list for DMC : $remove_volume_idx"
        fi

        motion_parameters=${temp_out_dir}/motion_outliers/motion_parameters_all_24.par

        motion_corrected=`find ${temp_out_dir}/motion_outliers/ -name "fmri_mcf.nii.gz"`
        func=${temp_out_dir}/${func_filename}_mcf.nii.gz

        if [[ ! -z $remove_volume_idx ]] ; then
            datatype=`fslval $func data_type`
            func_filename=$(basename $(basename $func .gz) .nii)
            outfile=${temp_out_dir}/${func_filename}_dmc.nii.gz
            func_dmc=${outfile}
           
            # copy dmc files to bids folder
            dmc_file_name=$(basename $(basename $func_filename _bold_mcf) _rec-ori)
        fi

    done < ${output_dir2}/${subject_id}/func_list.txt
done < ${output_dir2}/subject_list.txt

#======================================================================



#======================================================================
# preprocessing using fmri-prep docker
#----------------------------------------------------------------------
# fmriprep-docker $data_dir /local/data/fmriprep_motion participant --participant_label $participant_label --nthreads 32 --omp-nthreads 8 --template-resampling-grid MNI152NLin2009cAsym --use-syn-sdc --use-aroma --ignore-aroma-denoising-errors --force-bbr --write-graph

#docker run --rm -it -v /local/freesurfer/license.txt:/opt/freesurfer/license.txt:ro -v /fileserver/motion/seyhmus/data/rsFMRIwithMotion/isbi_out:/data:ro -v /local/data/fmriprep_motion2:/out -v /local/fsl/data/standard/MNI152_T1_3mm_brain.nii.gz:/imports/MNI152_T1_3mm_brain.nii.gz:ro poldracklab/fmriprep:1.1.3 /data /out participant --participant_label 01 --nthreads 32 --omp-nthreads 8 --use-syn-sdc --use-aroma --ignore-aroma-denoising-errors --force-bbr --write-graph --template-resampling-grid /imports/MNI152_T1_3mm_brain.nii.gz
#======================================================================



#======================================================================
# nuisance signal regression
#----------------------------------------------------------------------

# confound columns
funcset=`find /local/data/fmriprep_motion2/fmriprep -name "*rec-ori*bold_space-MNI152NLin2009cAsym_preproc.nii.gz"`
echo "FUNC SET: $funcset"
find /local/data/fmriprep_motion2/fmriprep -name "*rec-ori*bold_space-MNI152NLin2009cAsym_preproc.nii.gz" | while read line; do
func=$line
tr_in_seconds=`fslval $func pixdim4`
echo "TR : $tr_in_seconds"
func_basename=$(basename $func .nii.gz)
func_bbname=`echo $func_basename | sed "s/rec-.*//"`rec-ori_bold
remove_volume_indices_file=`find $output_dir2 -wholename "*${func_bbname}/remove_volume_list*"`
echo "remove volume file: $remove_volume_indices_file"
aroma_func=$(dirname $func)/$(basename $func _preproc.nii.gz)_variant-smoothAROMAnonaggr_preproc_3mm.nii.gz
echo "Aroma func: $aroma_func"
brain_mask=$(dirname $func)/$(basename $func _preproc.nii.gz)_brainmask.nii.gz
aroma_basename=$(basename $aroma_func .nii.gz)
func_dir=`dirname $func`
conf_file=${func_dir}/$(basename $func _space-MNI152NLin2009cAsym_preproc.nii.gz)_confounds.tsv
cat $conf_file | awk {'print $1 "\t" $2 "\t" $26 "\t" $27 "\t" $28 "\t" $29 "\t" $30 "\t" $31'} > ${output_dir}/temp_confound_file.txt
conf_file=${output_dir}/temp_confound_file.txt
sed -i '1d' ${conf_file}

# regression
#fsl_glm -i $func -d $conf_file --out_res=${output_dir}/${func_basename}_residual.nii.gz --out_data=${output_dir}/${func_basename}_data.nii.gz --demean --des_norm --dat_norm
func=${output_dir}/${func_basename}_residual.nii.gz
echo "func: $func"
#======================================================================



#======================================================================
# missing data completion
#----------------------------------------------------------------------

remove_volume_idx=`cat $remove_volume_indices_file`
if [[ ! -z $remove_volume_indices_file ]] ; then
    datatype=`fslval $func data_type`
    func_basename=$(basename $func .nii.gz)
    outfile=${output_dir}/${func_basename}_dmc.nii.gz
#    /fileserver/motion/code/fMRIMissingDataCompletion/fMRIMissingDataCompletion${datatype} $func ${outfile} $remove_volume_idx
    dmc_func=${outfile}
    echo "DMC func: $dmc_func"
fi
#======================================================================



#======================================================================
# temporal filtering 
#----------------------------------------------------------------------
func_basename=$(basename $func .nii.gz)
#python ${code_dir}/bandpass_nii.py ${func} $tr_in_seconds ${output_dir}/${func_basename}_tempfilt.nii.gz
func=${output_dir}/${func_basename}_tempfilt.nii.gz
echo "func: $func"

dmc_basename=$(basename $dmc_func .nii.gz)
#python ${code_dir}/bandpass_nii.py ${dmc_func} $tr_in_seconds ${output_dir}/${dmc_basename}_tempfilt.nii.gz
dmc_func=${output_dir}/${dmc_basename}_tempfilt.nii.gz
echo "DMC func: $dmc_func"
#======================================================================



#======================================================================
# scrubbing
#----------------------------------------------------------------------

if [ ! -z $remove_volume_indices_file ] ; then
    func_basename=$(basename $func .nii.gz)
    cp $remove_volume_indices_file ${output_dir}/temp_remove_volumes_list.txt
    ${code_dir}/scrubBasedOnRemoveVolumeList.sh -in $func -rm ${output_dir}/temp_remove_volumes_list.txt
    scrubbed_func=${output_dir}/${func_basename}_scrubbed.nii.gz
    echo "Scrubbing func: $scrubbed_func"

    dmc_basename=$(basename $dmc_func .nii.gz)
    ${code_dir}/scrubBasedOnRemoveVolumeList.sh -in $dmc_func -rm ${output_dir}/temp_remove_volumes_list.txt
    dmc_func=${output_dir}/${dmc_basename}_scrubbed.nii.gz
    echo "DMC scrubbing func: $dmc_func"
fi

#======================================================================



#======================================================================
# roi-based functional connectivity
#----------------------------------------------------------------------

func_basename=$(basename $func .nii.gz)
# compute mean time series of each roi in the seed mask
${FSLDIR}/bin/fslmeants -i $func -o ${func_basename}_mts.txt --label=/fileserver/motion/seyhmus/data/dosenbash_rois/Dosenbach_160_rois_3mm_uthr.nii.gz

${code_dir}/computePearsonsCorrelationCoefficient.sh $func $pcc_roi
pcc_roi_basename=$(basename $(basename $pcc_roi .gz) .nii)
mv ${pcc_roi_basename}_corr.nii.gz ${output_dir}/${func_basename}_${pcc_roi_basename}.nii.gz
mv ${pcc_roi_basename}_mts.txt ${output_dir}/${func_basename}_${pcc_roi_basename}_mts.txt

# mask correlation maps
if [[ ! -z $brain_mask ]] ; then
    ${FSLDIR}/bin/fslmaths ${output_dir}/${func_basename}_${pcc_roi_basename}.nii.gz -mas ${brain_mask} ${output_dir}/${func_basename}_${pcc_roi_basename}_mask.nii.gz
fi

# fisher z transform
${code_dir}/applyFisherTransformUsingFslTools.sh ${output_dir}/${func_basename}_${pcc_roi_basename}_mask.nii.gz ${output_dir}/${func_basename}_${pcc_roi_basename}_mask_fisherz.nii.gz


# same for dmc
dmc_basename=$(basename $dmc_func .nii.gz)
# compute mean time series of each roi in the seed mask
${FSLDIR}/bin/fslmeants -i $dmc_func -o ${dmc_basename}_mts.txt --label=/fileserver/motion/seyhmus/data/dosenbash_rois/Dosenbach_160_rois_3mm_uthr.nii.gz

${code_dir}/computePearsonsCorrelationCoefficient.sh $dmc_func $pcc_roi
pcc_roi_basename=$(basename $(basename $pcc_roi .gz) .nii)
mv ${pcc_roi_basename}_corr.nii.gz ${output_dir}/${dmc_basename}_${pcc_roi_basename}.nii.gz
mv ${pcc_roi_basename}_mts.txt ${output_dir}/${dmc_basename}_${pcc_roi_basename}_mts.txt

# mask correlation maps
if [[ ! -z $brain_mask ]] ; then
    ${FSLDIR}/bin/fslmaths ${output_dir}/${dmc_basename}_${pcc_roi_basename}.nii.gz -mas ${brain_mask} ${output_dir}/${dmc_basename}_${pcc_roi_basename}_mask.nii.gz
fi

# fisher z transform
${code_dir}/applyFisherTransformUsingFslTools.sh ${output_dir}/${dmc_basename}_${pcc_roi_basename}_mask.nii.gz ${output_dir}/${dmc_basename}_${pcc_roi_basename}_mask_fisherz.nii.gz


# do the same for the scrubbing
if [ ! -z $scrubbed_func ] ; then
    func=$scrubbed_func
    func_basename=$(basename $func .nii.gz)
    # compute mean time series of each roi in the seed mask
    ${FSLDIR}/bin/fslmeants -i $func -o ${func_basename}_mts.txt --label=/fileserver/motion/seyhmus/data/dosenbash_rois/Dosenbach_160_rois_3mm_uthr.nii.gz

    ${code_dir}/computePearsonsCorrelationCoefficient.sh $func $pcc_roi
    pcc_roi_basename=$(basename $(basename $pcc_roi .gz) .nii)
    mv ${pcc_roi_basename}_corr.nii.gz ${output_dir}/${func_basename}_${pcc_roi_basename}.nii.gz
    mv ${pcc_roi_basename}_mts.txt ${output_dir}/${func_basename}_${pcc_roi_basename}_mts.txt

    # mask correlation maps
    if [[ ! -z $brain_mask ]] ; then
        ${FSLDIR}/bin/fslmaths ${output_dir}/${func_basename}_${pcc_roi_basename}.nii.gz -mas ${brain_mask} ${output_dir}/${func_basename}_${pcc_roi_basename}_mask.nii.gz
    fi

    # fisher z transform
    ${code_dir}/applyFisherTransformUsingFslTools.sh ${output_dir}/${func_basename}_${pcc_roi_basename}_mask.nii.gz ${output_dir}/${func_basename}_${pcc_roi_basename}_mask_fisherz.nii.gz
fi

#======================================================================
done
#======================================================================
