#
# Example of integrating your preprocessing module (as a command line executable) into existing pipeline at any point 
# coding: utf-8

# In[1]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nibabel as nib
import matplotlib.pyplot as plt
from nipype.interfaces.base import (
    traits,
    TraitedSpec,
    CommandLineInputSpec,
    CommandLine,
    File,
    Directory,
    isdefined,
    SimpleInterface
)

from os.path import join as opj
from os.path import abspath
from IPython.display import Image
import tempfile as tempfile
import os

template4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0.nii.gz'
template_probability_map4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumProbabilityMask.nii.gz'
template_extraction_mask4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumRegistrationMask.nii.gz'


# In[17]:


brain_extract = pe.Node(ants.BrainExtraction(), name='brain_extract')
brain_extract.inputs.anatomical_image = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/anat/bestt1w_reoriented2std.nii.gz'
brain_extract.inputs.brain_probability_mask =template_probability_map4bet
brain_extract.inputs.brain_template = template4bet
brain_extract.inputs.extraction_registration_mask = template_extraction_mask4bet
brain_extract.inputs.out_prefix='highres'
brain_extract.inputs.num_threads=12


# In[74]:


tmpdir = tempfile.mkdtemp(prefix="/common/motion/seyhmus/data/georepsetup_")
motion_outliers = pe.Node(fsl.MotionOutliers(), name = 'motion_outliers')
motion_outliers.inputs.dummy=0
motion_outliers.inputs.in_file='/common/motion/seyhmus/code/ali.nii.gz'
motion_outliers.inputs.out_file=opj(tmpdir,'motion_confounders')
motion_outliers.inputs.metric='fd'
motion_outliers.inputs.threshold=0.5
motion_outliers.inputs.out_metric_values=opj(tmpdir,'fd.txt')
motion_outliers.inputs.out_metric_plot=opj(tmpdir,'fd.png')
motion_outliers.inputs.args='-v --nocleanup -t '+ tmpdir

obare=pe.Node(util.IdentityInterface(fields=['out_file', 'metric_image',
                                                        'metric']),
              name='outputnode')

wf=pe.Workflow(name='deneme')
wf.connect([
    (motion_outliers, obare, [('out_file','out_file'),
                              ('out_metric_plot','metric_image'),
                              ('out_metric_values','metric')
                             ]),
])
              
              


# In[76]:


wf.write_graph(graph2use='flat')


# In[77]:


Image(filename='/common/motion/seyhmus/code/graph.png')


# In[83]:


import nipype.algorithms.confounds as nac


# In[2]:


tempfile.mkdtemp()

