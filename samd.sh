# /bin/sh
# Run FSL-FEAT on multiple directories with the same design file.

# Written by : Seyhmus Guler
# Last edit  : 12/28/2016 by Seyhmus

# NOTES:
#   1. The results are saved at target data directories.
#   2. The desing file should be prepared beforehand, e.g. using FEAT
#      GUI and the changeFeatDesignFile.sh script
#   3. Each line in the data directory contains paths to (in order): fmri_data, optional: structural_brain, and outliers

#=====================================================================
# Check the syntax
#---------------------------------------------------------------------
# hebe
if [ ! $# -eq 2 ] ; then
    echo "Usage: $0 <input_data_directories_file> <design_file>"
    exit 1
else
    design=`basename $2 .fsf`
    rp=`realpath $2`
    parentDirectory=`dirname $rp`
    if [ ! -s $1 ] ; then
        echo "The file $1 doesn't exist or is empty. Nothing to do, exiting.."
        exit 2
    else
        echo "Running feat  on all directories listed in ${1}, with template design file ${2}"
    fi
fi

# total number of data files
nRuns=`wc $1 | awk {'print $1'}`
echo "Total number of fmri datasets: ${nRuns}"
echo =====================================================================
runIndex=1
#=====================================================================


#=====================================================================
# Add all data files to the design file and run feat
#---------------------------------------------------------------------

cp $2 ${design}_temp.fsf 

# Toggle progress watcher (browser) off
sed -i "s@set fmri(featwatcher_yn).*@set fmri(featwatcher_yn) 0@g" ${design}_temp.fsf

# Set the total number of first level analyses
sed -i "s@set fmri(multiple).*@set fmri(multiple) ${nRuns}@g" ${design}_temp.fsf

touch emptyOutlierFile
while read line ; do
    func=`echo ${line} | awk {'print $1'}`
    struct=`echo ${line} | awk {'print $2'}`
    outliers=`echo ${line} | awk {'print $3'}`
    
    if [ -z "$outliers" ] ; then
        outliers="$(pwd)/emptyOutlierFile"
    fi

    dfn=`basename ${func} .gz`
    dfn=`basename ${dfn} .nii`
    dfnP=`dirname ${func}`

    sfn=`basename ${struct} .gz`
    sfn=`basename ${sfn} .nii`
    sfnP=`dirname ${struct}`

    # add all directories to the feat design file
    if [ ${runIndex} -eq 1 ] ; then
        sed -i "s@set feat_files(${runIndex}).*@set feat_files(${runIndex}) \"${dfnP}\/${dfn}\"@g" ${design}_temp.fsf
        
        if [ ! -z "$struct" ] ; then
            sed -i "s@set highres_files(${runIndex}).*@set highres_files(${runIndex}) \"${sfnP}\/${sfn}\"@g" ${design}_temp.fsf
        fi
        sed -i "s@set confoundev_files(${runIndex}).*@set confoundev_files(${runIndex}) \"${outliers}\"@g" ${design}_temp.fsf
    else
        sed -i "/set feat_files(`echo "${runIndex} - 1" | bc`).*/a\set feat_files(${runIndex}) \"${dfnP}\/${dfn}\"" ${design}_temp.fsf
        if [ ! -z "$struct" ] ; then 
            sed -i "/set highres_files(`echo "${runIndex} - 1" | bc`).*/a\set highres_files(${runIndex}) \"${sfnP}\/${sfn}\"" ${design}_temp.fsf
        fi
        sed -i "/set confoundev_files(`echo "${runIndex} - 1" | bc`).*/a\set confoundev_files(${runIndex}) \"${outliers}\"" ${design}_temp.fsf
    fi
    
    # stdout data directories
    echo "Functional($runIndex): $func" 
    echo "Structural($runIndex): $struct"
    echo "Outliers($runIndex): $outliers"
    runIndex=`echo "${runIndex} + 1" | bc`
    echo =====================================================================
done < $1

# run feat
feat ${design}_temp.fsf
#${design}_temp.fsf

rm emptyOutlierFile
#=====================================================================
