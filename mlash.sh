#! /bin/sh
# MELODIC Limitedly Automated Schell based on the report log files
#
# Author: Seyhmus Guler
#
# Notes: 
#   1. The stages and their order are based on the log files obtained from running MELODIC on different datasets.
#   2. Certain parts are edited to create the connections between stages, e.g. to transfer variables between commands or to read in variables from the design file 



#======================================================================
#Initialization
#----------------------------------------------------------------------
currdir=$(pwd)

cp $1 design.fsf
#FSLDIR=`echo ${FSLDIR}` # FSL directory
FD=`cat design.fsf | grep "fmri(outputdir)" | awk {'print $3'} | tr -d '"'`
if [ -z $FD ] ; then 
    FD=`cat design.fsf | grep "feat_files(1)" | awk {'print $3'} | tr -d '"'`
fi
FD=`realpath $FD`
FD=${FD}.ica
DATAPTR=`cat design.fsf | grep "feat_files(1)" | awk {'print $3'} | tr -d '"'`

if [ -d "$FD" ] ; then echo "The output directory $FD exists, exiting..." ; exit 1 ; fi

mkdir -p $FD
mv design.fsf ${FD}/design.fsf
cd ${FD}

#check!
mkdir .files; 
cp ${FSLDIR}/doc/fsl.css .files
cp -r ${FSLDIR}/doc/images .files/images

#copy data and set datatype to float
echo "${FSLDIR}/bin/fslmaths ${DATAPTR} prefiltered_func_data -odt float
DATAPTR=prefiltered_func_data"

${FSLDIR}/bin/fslmaths ${DATAPTR} prefiltered_func_data -odt float
DATAPTR=prefiltered_func_data

#delete volumes!
nVol=`cat design.fsf | grep "fmri(npts)" | awk {'print $3'}`
nDelete=`cat design.fsf | grep "fmri(ndelete)" | awk {'print $3'}`
nVol=`echo " $nVol - $nDelete " | bc`
echo "${FSLDIR}/bin/fslroi $DATAPTR prefiltered_func_data $nDelete $nVol
DATAPTR=prefiltered_func_data"
${FSLDIR}/bin/fslroi $DATAPTR prefiltered_func_data $nDelete $nVol
DATAPTR=prefiltered_func_data

#create example functional image (default: center image)
exFunIdx=`echo "$nVol / 2 " | bc`
echo "${FSLDIR}/bin/fslroi $DATAPTR example_func $exFunIdx 1"
${FSLDIR}/bin/fslroi $DATAPTR example_func $exFunIdx 1
refVol=example_func # reference volume (for registration and motion corr)

# TR
echo "trSec=`cat design.fsf | grep "fmri(tr)" | awk {'print $3'}`"
trSec=`cat design.fsf | grep "fmri(tr)" | awk {'print $3'}`

#======================================================================



#======================================================================
# registration
#----------------------------------------------------------------------

flirtOpts=
version=`cat design.fsf | grep "fmri(version)" | awk {'print $3'}`
reghighres_yn=`cat design.fsf | grep "fmri(reghighres_yn)" | awk {'print $3'}`
regstandard_yn=`cat design.fsf | grep "fmri(regstandard_yn)" | awk {'print $3'}`

if [ $reghighres_yn -eq 1 ] ; then
    highresf=`cat design.fsf | grep "highres_files(1)" | awk {'print $3'} | tr -d '"'`
    hrdof=`cat design.fsf | grep "fmri(reghighres_dof)" | awk {'print $3'}`
    hrsearch=`cat design.fsf | grep "fmri(reghighres_search)" | awk {'print $3'}` 
    flirtOpts="$flirtOpts -h $highresf -w $hrdof -x $hrsearch"
fi

if [ $regstandard_yn -eq 1 ] ; then
    standardf=`cat design.fsf | grep "fmri(regstandard)" | awk {'print$3'} | tr -d '"'`
    stdof=`cat design.fsf | grep "fmri(regstandard_dof)" | awk {'print $3'}`
    stsearch=`cat design.fsf | grep "fmri(regstandard_search)" | awk {'print $3'}` 
    flirtOpts="$flirtOpts -s $standardf -y $stdof -z $stsearch"
fi

echo "${FSLDIR}/bin/mainfeatreg -F $version -d ${FD} -i ${FD}/example_func.nii.gz $flirtOpts"
${FSLDIR}/bin/mainfeatreg -F $version -d ${FD} -i ${FD}/example_func.nii.gz $flirtOpts

#======================================================================



#======================================================================
# motion correction
#----------------------------------------------------------------------

moco_yn=`cat design.fsf | grep "fmri(mc)" | awk {'print $3'}`
if [ $moco_yn -eq 1 ] ; then

    echo "${FSLDIR}/bin/mcflirt -in $DATAPTR -out prefiltered_func_data_mcf -mats -plots -reffile $refVol -rmsrel -rmsabs -spline_final"
    ${FSLDIR}/bin/mcflirt -in $DATAPTR -out prefiltered_func_data_mcf -mats -plots -reffile $refVol -rmsrel -rmsabs -spline_final
    DATAPTR=prefiltered_func_data_mcf

    /bin/mkdir -p mc
    /bin/mv -f prefiltered_func_data_mcf.mat prefiltered_func_data_mcf.par prefiltered_func_data_mcf_abs.rms prefiltered_func_data_mcf_abs_mean.rms prefiltered_func_data_mcf_rel.rms prefiltered_func_data_mcf_rel_mean.rms mc

    # visualize
    ${FSLDIR}/bin/fsl_tsplot -i mc/prefiltered_func_data_mcf.par -t 'MCFLIRT estimated rotations (radians)' -u 1 --start=1 --finish=3 -a x,y,z -w 960 -h 216 -o mc/rot.png 
    ${FSLDIR}/bin/fsl_tsplot -i mc/prefiltered_func_data_mcf.par -t 'MCFLIRT estimated translations (mm)' -u 1 --start=4 --finish=6 -a x,y,z -w 960 -h 216 -o mc/trans.png 
    ${FSLDIR}/bin/fsl_tsplot -i mc/prefiltered_func_data_mcf_abs.rms,mc/prefiltered_func_data_mcf_rel.rms -t 'MCFLIRT estimated mean displacement (mm)' -u 1 -w 960 -h 216 -a absolute,relative -o mc/disp.png 
fi

#======================================================================



#======================================================================
# Slice timing correction
#----------------------------------------------------------------------

stIdx=`cat design.fsf | grep "fmri(st)" | awk {'print $3'}` # if 0, no slice timing correction
if [ $stIdx -gt 0 ] ; then
    case $stIdx in 
    
        2)
            stOpt='--down'
            ;;
        3)
            stfile=`cat design.fsf | grep "fmri(st_file)" | awk {'print $3'} | tr -d '"'`
            stOpt="--ocustom=$stfile"
            ;;
        4)
            stfile=`cat design.fsf | grep "fmri(st_file)" | awk {'print $3'} | tr -d '"'`
            stOpt="--tcustom=$stfile"
            ;;
        5)
            stOpt='--odd'
            ;;
    esac
    
    echo "${FSLDIR}/bin/slicetimer -i $DATAPTR --out=prefiltered_func_data_st -r ${trSec} $stOpt"
    ${FSLDIR}/bin/slicetimer -i $DATAPTR --out=prefiltered_func_data_st -r ${trSec} $stOpt
    DATAPTR=prefiltered_func_data_st
fi

#======================================================================



#======================================================================
# thresholding and masking
#----------------------------------------------------------------------

funcdata_unmasked=$DATAPTR

# brain mask
altMask=`cat design.fsf | grep "fmri(alternative_mask)" | awk {'print $3'} | tr -d '"'` # alternative mask
if [ ! -z $altMask ] ; then 
    ${FSLDIR}/bin/fslmaths $DATAPTR -mas $altMask prefiltered_func_data_altmasked
    DATAPTR=prefiltered_func_data_altmasked
else # mask based on BET brain extraction
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -Tmean mean_func"
    echo "${FSLDIR}/bin/bet2 mean_func mask -f 0.3 -n -m"
    echo "${FSLDIR}/bin/immv mask_mask mask"
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -mas mask prefiltered_func_data_bet"
    
    ${FSLDIR}/bin/fslmaths $DATAPTR -Tmean mean_func
    ${FSLDIR}/bin/bet2 mean_func mask -f 0.3 -n -m
    ${FSLDIR}/bin/immv mask_mask mask
    ${FSLDIR}/bin/fslmaths $DATAPTR -mas mask prefiltered_func_data_bet
    DATAPTR=prefiltered_func_data_bet
fi

#intensity thresholding
perc=`${FSLDIR}/bin/fslstats prefiltered_func_data_bet -p 2 -p 98`
perc2=`echo $perc | awk {'print $1'}`
perc98=`echo $perc | awk {'print $2'}`
thrPerc=`echo " $perc98 / 10 " | bc -l`

brainthr=`cat design.fsf | grep "fmri(brain_thresh)" | awk {'print $3'}`
if [ $brainthr -gt 0 ] ; then # intensity % based thresholding
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -thr ${thrPerc} -Tmin -bin mask -odt char"
    echo "medInt=${FSLDIR}/bin/fslstats $funcdata_unmasked -k mask -p 50"
    echo "medInt=`${FSLDIR}/bin/fslstats $funcdata_unmasked -k mask -p 50`"
    echo "${FSLDIR}/bin/fslmaths mask -dilF mask"
    echo "${FSLDIR}/bin/fslmaths $funcdata_unmasked -mas mask prefiltered_func_data_thresh"
    echo "DATAPTR=prefiltered_func_data_thresh"
    
    ${FSLDIR}/bin/fslmaths $DATAPTR -thr ${thrPerc} -Tmin -bin mask -odt char
    medInt=`${FSLDIR}/bin/fslstats $funcdata_unmasked -k mask -p 50`
    ${FSLDIR}/bin/fslmaths mask -dilF mask
    ${FSLDIR}/bin/fslmaths $funcdata_unmasked -mas mask prefiltered_func_data_thresh
    DATAPTR=prefiltered_func_data_thresh
else
    echo "${FSLDIR}/bin/fslmaths example_func -mul 0 -add 1 -odt char"
    echo "medInt=`${FSLDIR}/bin/fslstats $DATAPTR -p 90`"
    ${FSLDIR}/bin/fslmaths example_func -mul 0 -add 1 -odt char
    medInt=`${FSLDIR}/bin/fslstats $DATAPTR -p 90`
fi

#======================================================================



#======================================================================
# spatial smoothing
#----------------------------------------------------------------------

echo "fwhm=`cat design.fsf | grep "fmri(smooth)" | awk {'print $3'}`"
fwhm=`cat design.fsf | grep "fmri(smooth)" | awk {'print $3'}`
smooThr=0.01

if [ 1 -eq "$(echo "${fwhm} > ${smooThr}" | bc)" ] ; then
    smoothSigma=`echo " $fwhm / 2.355 " | bc -l`
    susanInt=`echo " $medInt * 3 / 4 - $perc2 * 3 / 4 " | bc -l` 
    echo "smoothSigma= $smoothSigma and susanInt=$susanInt"
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -Tmean mean_func"
    echo "${FSLDIR}/bin/susan $DATAPTR $susanInt $smoothSigma 3 1 1 mean_func $susanInt prefiltered_func_data_smooth"
    echo "${FSLDIR}/bin/fslmaths prefiltered_func_data_smooth -mas mask prefiltered_func_data_smooth"
    echo "DATAPTR=prefiltered_func_data_smooth"
    
    ${FSLDIR}/bin/fslmaths $DATAPTR -Tmean mean_func
    ${FSLDIR}/bin/susan $DATAPTR $susanInt $smoothSigma 3 1 1 mean_func $susanInt prefiltered_func_data_smooth
    ${FSLDIR}/bin/fslmaths prefiltered_func_data_smooth -mas mask prefiltered_func_data_smooth
    DATAPTR=prefiltered_func_data_smooth
fi

#======================================================================



#======================================================================
# intensity normalization
#----------------------------------------------------------------------

normmean=10000
norm_yn=`cat design.fsf | grep "fmri(norm_yn)" | awk {'print $3'}`
if [ $norm_yn -eq 1 ] ; then
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -inm $normmean prefiltered_func_data_intnorm"
    ${FSLDIR}/bin/fslmaths $DATAPTR -inm $normmean prefiltered_func_data_intnorm
else
    scaling=`echo " $normmean / $medInt " | bc -l` 
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -mul $scaling prefiltered_func_data_intnorm"
    ${FSLDIR}/bin/fslmaths $DATAPTR -mul $scaling prefiltered_func_data_intnorm
fi
DATAPTR=prefiltered_func_data_intnorm

#======================================================================



#======================================================================
# perfusion subtraction
#----------------------------------------------------------------------
# perfusion subtraction code goes here
#======================================================================



#======================================================================
# temporal filtering
#----------------------------------------------------------------------

hpfilter_yn=`cat design.fsf | grep "fmri(temphp_yn)" | awk {'print $3'}`
lpfilter_yn=`cat design.fsf | grep "fmri(templp_yn)" | awk {'print $3'}`


if [ $hpfilter_yn -eq 1 ] || [ $lpfilter_yn -eq 1 ] ; then 
    restoreMeanCmd=
    hpSigmaVol=-1
    if [ $hpfilter_yn -eq 1 ] ; then
        hpSigmaSec=`cat design.fsf | grep "fmri(paradigm_hp)" | awk {'print $3'}`
        hpSigmaSec=`echo " $hpSigmaSec / 2 " | bc -l`
        hpSigmaVol=`echo " $hpSigmaSec / ${trSec} " | bc -l`
        ${FSLDIR}/bin/fslmaths $DATAPTR -Tmean tempMean
        restoreMeanCmd="-add tempMean"
    fi
    lpSigmaVol=-1
    if [ $lpfilter_yn -eq 1 ] ; then
        lpSigmaSec=2.8
        lpSigmaVol=`echo " ${lpSigmaSec} / ${trSec} " | bc -l`
    fi

    # temporal filtering
    echo "${FSLDIR}/bin/fslmaths $DATAPTR -bptf $hpSigmaVol $lpSigmaVol $restoreMeanCmd prefiltered_func_data_tempfilt"
    ${FSLDIR}/bin/fslmaths $DATAPTR -bptf $hpSigmaVol $lpSigmaVol $restoreMeanCmd prefiltered_func_data_tempfilt
    
    if [ $hpfilter_yn -eq 1 ] ; then 
        echo "${FSLDIR}/bin/imrm tempMean"
        ${FSLDIR}/bin/imrm tempMean
    fi
    DATAPTR=prefiltered_func_data_tempfilt
fi

#======================================================================

echo "${FSLDIR}/bin/fslmaths $DATAPTR filtered_func_data"
${FSLDIR}/bin/fslmaths $DATAPTR filtered_func_data


#======================================================================
# melodic ica
#----------------------------------------------------------------------
melodic_yn=`cat design.fsf | grep "fmri(melodic_yn)" | awk {'print $3'}`
inmelodic_yn=`cat design.fsf | grep "fmri(inmelodic)" | awk {'print $3'}`
if [ $melodic_yn -eq 1 ] || [ $inmelodic_yn -eq 1 ] ; then
    echo "melodic -i filtered_func_data -o filtered_func_data.ica -v --nobet --bgthreshold=3 --tr=${trSec} -d 0 --mmthresh=0.5 --report --guireport=../../report.html"
    melodic -i filtered_func_data -o filtered_func_data.ica -v --nobet --bgthreshold=3 --tr=${trSec} -d 0 --mmthresh=0.5 --report --guireport=../../report.html --Ostats
fi
    
echo "${FSLDIR}/bin/fslmaths filtered_func_data -Tmean mean_func"
echo "/bin/rm -rf prefiltered_func_data*"
${FSLDIR}/bin/fslmaths filtered_func_data -Tmean mean_func
#/bin/rm -rf prefiltered_func_data*

#======================================================================
cd $currdir
