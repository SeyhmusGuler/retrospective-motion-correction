# 
# Example for fMRIPrep anatomical preprocessing pipeline only. 
# coding: utf-8

# In[11]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.io as nio
import nipype.pipeline.engine as pe
import nibabel as nib
import matplotlib.pyplot as plt
from nipype.interfaces.base import (
    traits,
    TraitedSpec,
    CommandLineInputSpec,
    CommandLine,
    File,
    Directory,
    isdefined,
    SimpleInterface,
)

from os.path import join as opj
from os.path import abspath
from IPython.display import Image
import tempfile as tempfile
import os

template4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0.nii.gz'
template_probability_map4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumProbabilityMask.nii.gz'
template_extraction_mask4bet='/local/antsbin/templates/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumRegistrationMask.nii.gz'
mni_reference_image='/local/fsl/data/standard/MNI152_T1_2mm_brain.nii.gz'
output_dir='/fileserver/motion/seyhmus/data/nfb_motor/sham/derivatives/sub-03/anat_prep_out_delete'


# In[12]:


anat_preproc = pe.Workflow(name='anat_preprocessing')

inputnode = pe.Node(util.IdentityInterface(fields=['t1w', 't2w','ref_image' 'subjects_dir', 'subject_id']),
                   name='inputnode')
inputnode.inputs.ref_image = mni_reference_image
inputnode.inputs.t1w = '/fileserver/motion/seyhmus/data/nfb_motor/sham/derivatives/sub-03/5_MEMPRAGE_nonMoco.nii.gz'
outputnode = pe.Node(util.IdentityInterface(fields=['t1_preproc', 't1_brain','t1_mask', 't1_seg','t1_tpms',
                                                   't1_2_mni_warped_image',
                                                    't1_2_mni_forward_transforms',
                                                    't1_2_mni_composite_transform',
                                                    'mni_2_t1_inverse_composite_transform',
                                                    'mni_2_t1_inverse_warped_image',
                                                    'mni_2_t1_reverse_transforms',
                                                   'subjects_dir', 'subject_id']),
                    name='outputnode')

t1_reorient = pe.Node(fsl.Reorient2Std(),
                     name='t1_reorient2std')

brain_extract = pe.Node(ants.BrainExtraction(), name='brain_extract')
brain_extract.inputs.brain_probability_mask =template_probability_map4bet
brain_extract.inputs.brain_template = template4bet
brain_extract.inputs.extraction_registration_mask = template_extraction_mask4bet
brain_extract.inputs.out_prefix='highres'
brain_extract.inputs.num_threads=24

# Registration (good) - computes registration between subject's structural and MNI template.
antsreg = pe.Node(ants.Registration(args='--float',
                            collapse_output_transforms=True,
                            fixed_image=mni_reference_image,
                            initial_moving_transform_com=True,
                            num_threads=24,
                            output_inverse_warped_image=True,
                            output_warped_image=True,
                            sigma_units=['vox']*3,
                            transforms=['Rigid', 'Affine', 'SyN'],
                            terminal_output='file',
                            winsorize_lower_quantile=0.005,
                            winsorize_upper_quantile=0.995,
                            convergence_threshold=[1e-06],
                            convergence_window_size=[10],
                            metric=['MI', 'MI', 'CC'],
                            metric_weight=[1.0]*3,
                            number_of_iterations=[[1000, 500, 250, 100],
                                                  [1000, 500, 250, 100],
                                                  [100, 70, 50, 20]],
                            radius_or_number_of_bins=[32, 32, 4],
                            sampling_percentage=[0.25, 0.25, 1],
                            sampling_strategy=['Regular',
                                               'Regular',
                                               'None'],
                            shrink_factors=[[8, 4, 2, 1]]*3,
                            smoothing_sigmas=[[3, 2, 1, 0]]*3,
                            transform_parameters=[(0.1,),
                                                  (0.1,),
                                                  (0.1, 3.0, 0.0)],
                            use_histogram_matching=True,
                            write_composite_transform=True),
               name='antsreg')

datasink = pe.Node(nio.DataSink(),
             name = 'datasink')
datasink.inputs.base_directory=output_dir
        
anat_preproc.connect([
    (inputnode, t1_reorient, [('t1w','in_file')]),
    (inputnode, antsreg, [('ref_image','fixed_image')]),
    (t1_reorient, brain_extract, [('out_file','anatomical_image')]),
    (brain_extract, antsreg, [('BrainExtractionBrain','moving_image')]),
    (brain_extract, datasink, [('BrainExtractionBrain','bet.@t1_brain')]),
    (antsreg, outputnode, [('forward_transforms','t1_2_mni_forward_transforms')]),
    (antsreg, outputnode, [('composite_transform', 't1_2_mni_composite_transform')]),
    (antsreg, outputnode, [('inverse_warped_image','mni_2_t1_inverse_warped_image')]),
    (antsreg, outputnode, [('inverse_composite_transform','mni_2_t1_inverse_composite_transform')]),
    (antsreg, outputnode, [('warped_image','t1_2_mni_warped_image')]),
    (antsreg, outputnode, [('reverse_transforms','mni_2_t1_reverse_transforms')]),
    (antsreg, datasink, [('composite_transform','antsreg.@forward_xfm')]),
    (antsreg, datasink, [('inverse_composite_transform','antsreg.@inverse_xfm')]),
    (antsreg, datasink, [('warped_image','antsreg.@t1_2_mni_warped_image')]),
    (antsreg, datasink, [('inverse_warped_image','antsreg.@mni_2_t1_inverse_warped_image')])
])


# In[13]:


anat_preproc.write_graph(graph2use='colored')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')
#ants.Registration.output_spec()


# In[14]:


anat_preproc.run()

