# Author: Louis Soussand <lsoussan@bidmc.harvard.edu>

print("#### Connectome.py ####")

import numpy as np
from nilearn import datasets
from nilearn import input_data
from nilearn import image
import os
import pandas as pd
import time
import multiprocessing
import glob
import pickle
import argparse
from termcolor import cprint
import gzip
import io
import gc

pickle_dir='/data2/pickle_cellar/scratch'

#-------------------------------------------------------------------------------
#|                                                                             |
#|                                  Functions                                  |
#|                                                                             |
#-------------------------------------------------------------------------------
### Internal functions:

# save zipped and pickled file
def save_pickle(obj, filename):
    # disable garbage collector (hack for faster reading/writing)
    gc.disable()
    f = gzip.open(filename, 'wb')
    pickle.dump(obj, f,protocol=-1)
    f.close()
    gc.enable()

# load zipped and pickled file
def load_pickle(filename):
    # disable garbage collector (hack for faster reading/writing)
    gc.disable()
    f = gzip.open(filename,'rb')
    loaded_object = pickle.load(f)
    f.close()
    return loaded_object

# Simple connectome builder by subject including the pickle
def simple_connectome_builder(args):
    func_filename = args
    out_name=os.path.basename(func_filename)
    pickle_file=os.path.join(pickle_dir, out_name+'.pkl')
    if os.path.exists(pickle_file):
        cprint('Loading Pickled Timecourse: '+pickle_file)
        brain_time_series=load_pickle(pickle_file)
        func_file=brain_masker.inverse_transform(brain_time_series.astype(dtype=np.float32))
    else:
        cprint('Loading '+func_filename)
        func_file=image.load_img(func_filename)
        brain_time_series = brain_masker.transform(func_file).astype(dtype=np.float16)
        cprint('Saving Pickled Timecourse: '+pickle_file)
        save_pickle(brain_time_series,pickle_file)
    seed_time_series = seed_masker.transform(func_file)
    list_seed_based_correlations_fisher_z=[]
    for i in range(0,seed_time_series.shape[1]):
        indv_Fz=np.arctanh(np.dot(brain_time_series.T, seed_time_series.T[i]) / seed_time_series.shape[0])
        list_seed_based_correlations_fisher_z.append(indv_Fz)
        if create_individual_maps:
            brain_masker.inverse_transform(indv_Fz).to_filename(
                os.path.join(output_dir, 'Individual_Fz_maps', roinames[i]+'_subj_'+out_name+'_indv_Fz.nii.gz')
            )
    cprint("Individual AvgR_Fz has been calculated.")
    return list_seed_based_correlations_fisher_z
    #cprint(func_filename+' finished.')

def rois_corr(args):
    func_filename= args
    pickle_file=os.path.join(pickle_dir, os.path.basename(func_filename+'.pkl'))
    if os.path.exists(pickle_file):
        #cprint('Loading Pickled Timecourse: '+pickle_file)
        brain_time_series=load_pickle(pickle_file)
        func_file=brain_masker.inverse_transform(brain_time_series.astype(dtype=np.float32))
    else:
        cprint('Loading '+func_filename)
        func_file=image.load_img(func_filename)
        brain_time_series = brain_masker.transform(func_file).astype(dtype=np.float16)
        #cprint('Saving Pickled Timecourse: '+pickle_file)
        save_pickle(brain_time_series,pickle_file)
    timecourse=seed_masker.transform(func_file)
    df=pd.DataFrame(timecourse)
    #cprint('The correlation matrix has been calculated for '+ func_filename)
    return np.arctanh(df.corr())

cprint('Initiating process', 'blue')
start=time.time()

#-------------------------------------------------------------------------------
#|                                                                             |
#|                                  Arguments                                  |
#|                                                                             |
#-------------------------------------------------------------------------------
# Arguments for the executable script
parser = argparse.ArgumentParser(description='Connectome builder. Based on the \
original connectome.sh by Andreas Horn. This script runs connectivity of an ROI \
to every other voxel in the brain based on standardized connectomes.')
parser.add_argument('-i', '--input', help='Niftii input')
parser.add_argument('-o','--output', help='Output directory', default='./')
parser.add_argument('-m', '--mask', help='Mask')
parser.add_argument('-r','--roi', help='ROI')
parser.add_argument('-fc','--functional-connectivity', help='Functional connectivity flag from connectome.sh.', default='/data')
parser.add_argument('-w','--workers', help='Number of workers.')
parser.add_argument('-c', '--command', default='seed', help='Define what to do \
with the ROI(s). Defaults to seed which will run connectivity from that seed to \
the rest of the brain.')
parser.add_argument('-v', '--indvmaps', help='Create individual Fz maps')

# Parsing the arguments
args = parser.parse_args()

if args.input or args.functional_connectivity:
    if args.input:
        dataset=glob.glob(args.input+'/*nii.gz')
        input_display=args.input

        if args.functional_connectivity:
            pickle_dir=args.functional_connectivity
    else:
        ds=glob.glob(args.functional_connectivity+'/*pkl')
        input_display=args.functional_connectivity
        dataset=[]
        for i in range(0, len(ds)):
            dataset.append(ds[i].split('.pkl')[0])
        pickle_dir=args.functional_connectivity
else:
    cprint("Please provide either a functional connectome or an input dataset.", 'red')

if args.mask:
    brain_masker = input_data.NiftiMasker(args.mask, standardize=True).fit()
else:
    mask_img = datasets.load_mni152_brain_mask()
    brain_masker = input_data.NiftiMasker(mask_img, standardize=True).fit()

if args.roi:
    if (args.roi).lower().endswith(('.nii', '.nii.gz')):
        roi=[args.roi]
        seed_length=1
    else:
        roi=pd.read_csv(args.roi, header=None)[0]
        seed_length=roi.shape[0]

    seed_masker = input_data.NiftiMapsMasker(roi,mask_img,standardize=True).fit()
    roinames=[]
    for i_roi in roi:
        roinames.append(os.path.splitext(os.path.splitext(os.path.basename(i_roi))[0])[0])
else:
    cprint('WARNING: You must provide an ROI.', 'red')
    cprint('Process finished', 'blue')
    quit()

if args.output:
    output_dir=args.output
else:
    output_dir=os.getcwd()

if args.indvmaps:
    create_individual_maps=True
    ind_maps_dir = os.path.join(output_dir, 'Invidual_Fz_maps')
    if not os.path.exists(ind_maps_dir):
        os.mkdir(ind_maps_dir)
else:
    create_individual_maps=False

if args.command=='seed':
    func_map=simple_connectome_builder
elif args.command=='matrix':
    func_map=rois_corr
else:
    cprint('WARNING: You must provide an acceptable command: either "matrix" or "seed".', 'red')
    cprint('Process finished', 'blue')
    quit()

#-------------------------------------------------------------------------------
#|                                                                             |
#|                                Multiprocessing                              |
#|                                                                             |
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    print("\n")
    print("Input: "+input_display)
    print("\n")
    print("Seeds: "+ str(seed_length))
    print("\n")
    print("Mode: "+ args.command)
    print("\n")
    print("Output: "+output_dir)

    # Multiprocessing Process:
    n_subjects=len(dataset)
    n_rois=len(roi)
    pool = multiprocessing.Pool()
    results=pool.map(func_map, dataset)
    pool.close()
    pool.join()
    result_array=np.asarray(results)

    #-------------------------------------------------------------------------------
    #|                                                                             |
    #|                                    Outputs                                  |
    #|                                                                             |
    #-------------------------------------------------------------------------------
    # Output Control:

    if args.command=='seed':
        for i in range(0,n_rois):
            print("Working on "+ str(i))
            df_Z=pd.DataFrame(result_array[:,i,:])
            print("Creating the maps")
            Zmap=df_Z.mean(axis=0).fillna(0)
            Rmap=np.tanh(Zmap)
            Tmap=Zmap.divide(df_Z.std(axis=0)).fillna(0)

            print("Writing the maps")
            imgR=brain_masker.inverse_transform(Rmap)
            imgR.to_filename(os.path.join(output_dir, roinames[i]+'_func_seed_AvgR.nii.gz'))

            imgZ=brain_masker.inverse_transform(Zmap)
            imgZ.to_filename(os.path.join(output_dir, roinames[i]+'_func_seed_AvgR_Fz.nii.gz'))

            imgT=brain_masker.inverse_transform(Tmap)
            imgT.to_filename(os.path.join(output_dir, roinames[i]+'_func_seed_T.nii.gz'))

    if args.command=='matrix':
        AvgR_mat = pd.DataFrame()
        AvgR_Fz_mean_mat=pd.DataFrame()
        AvgR_Fz_std_mat=pd.DataFrame()
        cprint('Printing correlation matrix:')
        for j in range(0,n_rois):
            colmat=pd.DataFrame()
            for i in range(0, n_subjects):
                colmat[i]=results[i][j]
            AvgR_Fz_mean_mat[j]=colmat.mean(axis=1)
            AvgR_Fz_std_mat=colmat.std(axis=1)
        print(AvgR_Fz_mean_mat)
        AvgR_Fz_mean_mat.to_csv(
            os.path.join(output_dir, "/matrix_corrMx_AvgR_Fz.csv", index=False)
        )
        np.tanh(AvgR_Fz_mean_mat).to_csv(
            os.path.join(output_dir, "/matrix_corrMx_AvgR.csv", index=False)
        )
        AvgR_Fz_mean_mat.divide(AvgR_Fz_std_mat).fillna(0).to_csv(
            os.path.join(output_dir, "/matrix_corrMx_T.csv", index=False)
        )
    end=time.time()
    cprint(end-start, 'red')
    cprint('Process finished', 'blue')
quit()
