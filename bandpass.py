#
# Example pipeline for bandpass filtering of fMRI data
# coding: utf-8

# In[1]:


from nipype.interfaces.base import BaseInterface, BaseInterfaceInputSpec, traits, File, TraitedSpec
from nipype.utils.filemanip import split_filename
import nipype.interfaces.io as nio
import nipype.pipeline.engine as pe
import nipype.interfaces.utility as util
from IPython.display import Image

import nibabel as nb
import os.path
import numpy as np
import scipy
from scipy import signal
import sys


# In[2]:


class bandpassInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists = True, desc = 'data to be bandpassed', mandatory=True)
    TR = traits.Float(desc='repetition time', mandatory = True)
    lpThr = traits.Float(desc='low pass cut-off frequency', mandatory=True)
    hpThr = traits.Float(desc='high pass cut-off frequency', mandatory=True)
    
class bandpassOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc="bandpassed data")
    
class bandpassFMRI(BaseInterface):
    input_spec = bandpassInputSpec
    output_spec = bandpassOutputSpec
    
    def _run_interface(self, runtime):
        fname = self.inputs.in_file
        samplingperiod = self.inputs.TR
        lp_cutoff = self.inputs.lpThr
        hp_cutoff = self.inputs.hpThr
        
        passHz = np.array([hp_cutoff, lp_cutoff])
        stopHz = np.array([hp_cutoff*0.8, lp_cutoff*1.1])
        
        gpass = 1
        gstop = 40
        
        img = nb.load(fname)
        
        samplingfreq = 1.0/samplingperiod
        wp = passHz/(samplingfreq/2)
        ws = stopHz/(samplingfreq/2)
        
        sos = scipy.signal.iirdesign(wp, ws, gpass, gstop, output="sos")
        filtered_data = scipy.signal.sosfiltfilt(sos, img.get_data(), axis=3, padtype="even", padlen=int(0.5*img.shape[3]))
        new_img = nb.Nifti1Image(filtered_data, img.affine, img.header)
        _, base, _ = split_filename(fname)
        nb.save(new_img, base + '_bandpassed.nii.gz')
        return runtime
    
    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)
        outputs["out_file"] = os.path.abspath(base + '_bandpassed.nii.gz')
        return outputs

        


# In[4]:


func = '/fileserver/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/func/nomo/sca/original/preprocessing.feat/nui/filtered_func_data_nui_res.nii.gz'
TR = 3
lpThr = 0.1
hpThr = 0.01
subject_id = 'sub-01'

inputnode=pe.Node(util.IdentityInterface(fields=['bold_file', 'subject_id']),
              name='inputnode')
inputnode.inputs.bold_file = func
inputnode.inputs.subject_id = 'sub-01'

datasink = pe.Node(nio.DataSink(), 
                   name = 'datasink')
datasink.inputs.base_directory = '/local/data/hobare'


bandpass = pe.Node(bandpassFMRI(),
                 name = 'bandpass')
bandpass.inputs.lpThr = lpThr
bandpass.inputs.hpThr = hpThr
bandpass.inputs.TR = TR


# In[9]:


bandpass_wf = pe.Workflow(name='bandpass_wf')
bandpass_wf.connect([
    (inputnode, datasink, [('subject_id','container')]),
    (inputnode, bandpass, [('bold_file','in_file')]),
    (bandpass, datasink, [('out_file','out.@bandpass')]),
    (inputnode,datasink,[('bold_file','out.@original')]),
])


# In[10]:


bandpass_wf.write_graph(graph2use='flat')
Image(filename='/fileserver/motion/seyhmus/code/graph.png')


# In[11]:


bandpass_wf.run()

