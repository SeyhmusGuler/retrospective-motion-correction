#! /bin/sh
# scrubs data based on a logical vector of which volumes to remove
#
# author : seyhmus guler
# date : 3/10/2017


if [ $# -ne 4 ] ; then echo "Usage : $0 -in <fmri4D_data> -rm <remove_volumes_list>" ; exit 1 ; fi

random_text=2138213d98321

fmri4D_data=$2
remove_list=$4
datap=`dirname $fmri4D_data`
filename=`basename $fmri4D_data .gz`
filename=`basename $filename .nii`

trSec=`fslinfo $fmri4D_data | grep pixdim4 | awk {'print $2'}`
fslsplit $fmri4D_data tempvolumes_${randomtext}_


cat $remove_list | sed -e 's/\s\+/\n/g' | while read idx ; do 
    rm -f tempvolumes_${randomtext}_000${idx}.nii.gz
    rm -f tempvolumes_${randomtext}_00${idx}.nii.gz
    rm -f tempvolumes_${randomtext}_0${idx}.nii.gz
    rm -f tempvolumes_${randomtext}_${idx}.nii.gz
done

fslmerge -tr ${datap}/${filename}_scrubbed tempvolumes_${randomtext}_* $trSec

rm -f tempvolumes_${randomtext}_*

