#
# Adding DMC to existing fMRI preprocessing pipeline
# coding: utf-8

# In[28]:


import nipype.interfaces.fsl as fsl
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info
from nipype.interfaces.base import (
    traits, TraitedSpec, isdefined, CommandLineInputSpec, CommandLine, BaseInterfaceInputSpec, File, InputMultiPath, OutputMultiPath,
SimpleInterface)
from nipype.utils.filemanip import load_json, save_json, split_filename, fname_presuffix
import os
import os.path
import nipype.interfaces.io as nio

import nipype.pipeline.engine as pe
import nipype.interfaces.utility as util
from IPython.display import Image

import nibabel as nb
import numpy as np
import scipy
from scipy import signal
import sys


# In[56]:





# In[61]:


dmc_wf = pe.Workflow(name='dmc_wf')
inputnode = pe.Node(util.IdentityInterface(fields=['in_file','remove_list']),
                   name='inputnode')
inputnode.inputs.in_file = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/func/nomo/sub-01_ses-01_task-rest_acq-nomo_rec-ori_bold_mcf.nii.gz'
inputnode.inputs.remove_list = '/common/motion/seyhmus/data/rsFMRIwithMotion/bids-analysis/derivatives/sub-01/ses-01/func/nomo/sub-01_ses-01_task-rest_acq-nomo_rec-ori_bold_mcf.nii.gz
fslval = pe.Node(fslValCommand(keyword='data_type'),
                 name='fsl_val')

dmc = pe.Node(dynamicMissingdataCompletion(),
             name='dmc')

datasink = pe.Node(nio.DataSink(),
             name = 'datasink')
datasink.inputs.base_directory='/common/motion/seyhmus/code/obare2'

dmc_wf.connect([
    (inputnode, dmc, [('in_file','in_file')]),
    (inputnode, dmc, [('remove_list', 'remove_volumes')]),
    (dmc, datasink, [('out_file','dmc')]),
])


# In[62]:


dmc_wf.write_graph(graph2use='flat')
Image(filename='/common/motion/seyhmus/code/graph.png')


# In[64]:


dmc_wf.run()

