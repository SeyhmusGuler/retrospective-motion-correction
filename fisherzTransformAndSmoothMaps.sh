#! /bin/sh
# Applies Fisher's Z transform and smoothing to a list of correlation maps

if [ $# -ne 1 ] ; then
    echo "Usage: $0 <correlation_map_list>"
    echo ""
    echo "Note: List is a text file with each line corresponding to a different correlation map"
    exit 1 
fi

while read line; do
    data_dir=`dirname $line`
    filename=$(basename $(basename $line .gz) .nii)
    
    pushd $data_dir
    
    # fisher z transform
    /common/motion/seyhmus/code/applyFisherTransformUsingFslTools.sh $line ${data_dir}/${filename}_fisherz.nii.gz

    # spatial smoothing with FWHM=6
    smoothing_sigma=`echo " 6 / 2.355 " | bc -l`
    ${FSLDIR}/bin/fslmaths ${data_dir}/${filename}_fisherz.nii.gz -s $smoothing_sigma ${data_dir}/${filename}_fisherz_fwhm-6.nii.gz

    popd

done < $1


