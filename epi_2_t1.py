
# coding: utf-8

# In[2]:


get_ipython().magic(u'matplotlib inline')
import nipype.interfaces.fsl as fsl
from nipype.interfaces.fsl.base import FSLCommand, FSLCommandInputSpec, Info
import nipype.interfaces.ants as ants
import nipype.interfaces.utility as util
import nipype.interfaces.freesurfer as fs
import nipype.interfaces.io as io
import nipype.pipeline.engine as pe
import nipype.interfaces.afni as afni
import nibabel as nb
import matplotlib.pyplot as plt
import nilearn as nil
import numpy as np
import nipype.interfaces.io as nio
import os
import nipype.algorithms.confounds as nac
from nilearn.masking import compute_epi_mask
from nilearn.image import concat_imgs
from nilearn import image as nli
from nilearn.image import index_img
import tempfile
from __future__ import print_function

import nibabel as nb
import os.path
import numpy as np
import scipy
from scipy import signal
import sys

import warnings as _warnings
import os as _os
from nipype.utils.filemanip import load_json, save_json, split_filename, fname_presuffix
from os.path import join as opj
from os.path import abspath
from IPython.display import Image
from nipype.interfaces.base import (
    traits, TraitedSpec, isdefined, CommandLineInputSpec, CommandLine, BaseInterfaceInputSpec, 
    File, InputMultiPath, OutputMultiPath, SimpleInterface, BaseInterface)


# In[3]:


epi_2_t1 = pe.Workflow(name='skull_strip_bold')
epi_2_t1.base_dir = '/fileserver/motion/seyhmus/code/epi_2_t1'
inputnode = pe.Node(util.IdentityInterface(fields=['ref_bold_brain',
                                                  't1_brain','t1_seg',
                                                 'subjects_dir','subject_id',
                                                 't1_2_fsnative_reverse_transform']),
                   name='inputnode')
bbregister = pe.Node(fs.BBRegister(),
                    name='bbregister')
bbregister.inputs.args = '--init-fsl'
bbregister.inputs.contrast_type = 't1'
bbregister.inputs.out_lta_file = True

bbregister2 = pe.Node(fsl.)


outputnode = pe.Node(util.IdentityInterface(fields=['itk_bold_to_t1',
                                                  'itk_t1_to_bold',
                                                  'fallback']),
                    name='outputnode')
epi_2_t1.connect([
    (inputnode, bbregister, [('subjects_dir','subjects_dir')]),
    (inputnode, bbregister, [('t1_2_fsnative_reverse_transform',
                              't1_2_fsnative_reverse_transform')]),
    (inputnode, bbregister, [('subject_id','subject_id')]),
    (inputnode, bbregister, [('ref_bold_brain','source_file')]),
])


# In[4]:


fs.BBRegister.help()


# In[11]:


ants.Registration.help()

