if [[ $# -eq 0 ]] ; then
    echo "Usage : $0 -in <remove_list_file> -skip <n_nonsteady_volumes>"
    exit 1
fi
n_skip=0
while [[ $1 != '' ]] ; do
    if [[ $1 == '-in' ]] 
    then
        rm_file=$2
        shift; shift
    elif [[ $1 == '-skip' ]]
    then
        n_skip=$2
        shift; shift
    fi
done

output_dir=`realpath $(dirname $rm_file)`
filename=`basename $rm_file .txt`
updated_remove_list=`awk -v aaa=$n_skip '{for (i = 1; i <= NF; i++) {print $i-aaa}}' $rm_file | while read in; do if [ $in -ge 0 ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`

echo $updated_remove_list > ${output_dir}/${filename}_skip.txt

