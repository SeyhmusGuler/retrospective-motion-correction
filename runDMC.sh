#! /bin/sh
# Dynamic missing data completion and seed connectivity analysis of rsfMRI 
#
# author: seyhmus guler
# date: 7/6/2017
#
#


#======================================================================
# inputs 
#----------------------------------------------------------------------

if [ $# -lt 2 ] ; then
    echo "Usage: $0 <fmri_data> <anat_data> <output_directory>"
    exit 1
fi

func=$1
anat=$2
mkdir -p $3
output_dir=`realpath $3`

func_filename=$(basename $(basename $func .gz) .nii)
func_parentdir=`dirname $func`

mkdir -p ${output_dir}/original_data
mkdir -p ${output_dir}/temporary_files
pushd ${output_dir}/temporary_files

${FSLDIR}/bin/fslmaths $func ${output_dir}/original_data/${func_filename}.nii.gz
func=${output_dir}/original_data/${func_filename}.nii.gz

#======================================================================



#======================================================================
# motion correction (alignment) and motion outliers
#----------------------------------------------------------------------

mkdir -p ${output_dir}/motion_outliers

# delete dummy scans?
n_delete_volumes=0
n_input_data_volumes=`fslval $func dim4`
n_input_data_volumes=`echo " $n_input_data_volumes - $n_delete_volumes " | bc`

# motion outliers (FD > 0.5)
${FSLDIR}/bin/fsl_motion_outliers -i $func -o ${output_dir}/motion_outliers/${func_filename}_motion_confounders --fd --thresh=0.5 -v --nocleanup -t ${output_dir}/motion_outliers --dummy=${n_delete_volumes} -p ${output_dir}/motion_outliers/fd.png -s ${output_dir}/motion_outliers/fd.txt | tee ${output_dir}/motion_outliers/log.txt


outlier_idx=`cat ${output_dir}/motion_outliers/log.txt | grep "Found spikes at" | sed -n -e 's/^.*at//p' | sed 's/^[ \t]*//'`

if [[ -z "${outlier_idx// }" ]] ; then
    echo "Volumes with FD > 0.5 : NONE"
    echo "Remove volume list for DMC : NONE"
    touch ${output_dir}/motion_outliers/high_motion_volume_list.txt
    touch ${output_dir}/motion_outliers/remove_volume_list.txt
else
    remove_volume_idx=`echo ${outlier_idx} | sed -e 's/\s\+/\n/g' | while read in; do echo $in; echo $(($in-1)); echo $(($in+1)); echo $(($in+2)); done | sort -n | uniq | while read in; do if [ $in -ge 0 ] && [ $in -lt $n_input_data_volumes ] ; then echo $in ; fi ; done | sed ':a;N;$!ba;s/\n/ /g'`
    echo ${outlier_idx} > ${output_dir}/motion_outliers/high_motion_volume_list.txt
    echo $remove_volume_idx > ${output_dir}/remove_volume_list.txt
    echo "Volumes with FD > 0.5 : $outlier_idx"
    echo "Remove volume list for DMC : $remove_volume_idx"
fi

# motion parameters
original_six_parameters=`find ${output_dir}/motion_outliers/ -name "fmri_mcf.par"`
cp $original_six_parameters ${output_dir}/motion_outliers/motion_parameters_original_6.par
${FSLDIR}/bin/mp_diffpow.sh ${original_six_parameters} ${output_dir}/motion_outliers/motion_parameters_extended_18
paste -d ' ' ${original_six_parameters} ${output_dir}/motion_outliers/motion_parameters_extended_18.dat > ${output_dir}/motion_outliers/motion_parameters_all_24.par
motion_parameters=${output_dir}/motion_outliers/motion_parameters_all_24.par

# motion corrected fmri data
motion_corrected=`find ${output_dir}/motion_outliers/ -name "fmri_mcf.nii.gz"`
$FSLDIR/bin/fslmaths $motion_corrected ${output_dir}/${func_filename}_mcf.nii.gz
func=${output_dir}/${func_filename}_mcf.nii.gz

rm -r ${output_dir}/motion_outliers/*_mc
#======================================================================



#======================================================================
# dmc (dynamic missing data completion) 
#----------------------------------------------------------------------

if [[ ! -z $remove_volume_idx ]] ; then
    datatype=`fslval $func data_type`
    func_filename=$(basename $(basename $func .gz) .nii)
    outfile=${output_dir}/${func_filename}_dmc.nii.gz
    /common/motion/code/fMRIMissingDataCompletion/fMRIMissingDataCompletion${datatype} $func ${outfile} $remove_volume_idx
    func_dmc=${outfile}
fi

#======================================================================



#======================================================================
# seed connectivity analysis
#----------------------------------------------------------------------

# original
/common/motion/seyhmus/code/seedBasedConnectivityAnalysisUsingFslTools.sh \
    -in $func \
    -out ${output_dir}/sca/original \
    -seed_mask /common/motion/seyhmus/data/rsFMRIwithMotion/rois_111_115_111and115.nii.gz \
    -anat $anat \
    -smoothing 6 \
    -design_file /common/motion/code/fMRIPipelines/template_design_file.fsf \
    -motion_parameters $motion_parameters \
    -wm_mask /common/motion/seyhmus/data/fsl_standard_data/tissuepriors/3mm/avg152T1_white_bin.nii.gz \
    -csf_mask /common/motion/seyhmus/data/fsl_standard_data/tissuepriors/3mm/avg152T1_csf_bin.nii.gz \
    -detrend_clq \
    -prefix sca_orig \
    -reorient2std \
    -use_our_preprocessing

if [[ ! -z $remove_volume_idx ]] ; then

    # dmc
    /common/motion/seyhmus/code/seedBasedConnectivityAnalysisUsingFslTools.sh \
        -scrub_list $output_dir/remove_volume_list.txt \
        -in $func_dmc \
        -out ${output_dir}/sca/dmc \
        -seed_mask /common/motion/seyhmus/data/rsFMRIwithMotion/rois_111_115_111and115.nii.gz \
        -anat $anat \
        -smoothing 6 \
        -design_file /common/motion/code/fMRIPipelines/template_design_file.fsf \
        -motion_parameters $motion_parameters \
        -wm_mask /common/motion/seyhmus/data/fsl_standard_data/tissuepriors/3mm/avg152T1_white_bin.nii.gz \
        -csf_mask /common/motion/seyhmus/data/fsl_standard_data/tissuepriors/3mm/avg152T1_csf_bin.nii.gz \
        -detrend_clq \
        -prefix sca_dmc \
        -reorient2std \
        -use_our_preprocessing


    # scrubbing
    /common/motion/seyhmus/code/seedBasedConnectivityAnalysisUsingFslTools.sh \
        -scrub_list ${output_dir}/remove_volume_list.txt \
        -in $func \
        -out ${output_dir}/sca/scrubbing \
        -seed_mask /common/motion/seyhmus/data/rsFMRIwithMotion/rois_111_115_111and115.nii.gz \
        -anat $anat \
        -smoothing 6 \
        -design_file /common/motion/code/fMRIPipelines/template_design_file.fsf \
        -motion_parameters $motion_parameters \
        -wm_mask /common/motion/seyhmus/data/fsl_standard_data/tissuepriors/3mm/avg152T1_white_bin.nii.gz \
        -csf_mask /common/motion/seyhmus/data/fsl_standard_data/tissuepriors/3mm/avg152T1_csf_bin.nii.gz \
        -detrend_clq \
        -prefix sca_scrub \
        -reorient2std \
        -use_our_preprocessing
fi

#======================================================================
popd
