#! /bin/sh
# converst sliceTiming.txt to the FSL-FEAT compliant style

# Written by Seyhmus Guler 
# Reviewed by 

#======================================================================
# syntax
#----------------------------------------------------------------------

if [ ! $# -eq 3 ] ; then 
    echo "Usage : $0 <input> <output> <TR_in_seconds>" ; exit 1 ; fi

if [ -s $2 ] ; then echo "File $2 is not empty" ; exit 2 ; fi

stfile=$1

sts=`cat ${stfile} | awk -F "FD " {'print $2'}`
sts=`echo ${sts} | awk -F " #" {'print $1'}`
echo ${sts} | awk -F "\\" {'for(i=1;i<=NF;i++){print $i}'} > $2_temp

while read line ; do
    a=`echo "${line} / $3 / 1000 - 0.5 " | bc -l | awk '{printf "%f", $0}'`
    echo $a >> $2
done < $2_temp
rm $2_temp
